import {RoomMemory} from "./room/room-memory";
import {Profiler} from "./profiler";

export class MaintenanceLink {

    public transfer(room: Room) {
        if (Game.time % 3 !== 0) {
            //let's save some cpu
            return;
        }
        // console.log("1 "+roomName);
        // if (!room.memory.unloadingLinks) {
        //     console.log("No !room.memory.unloadingLink in " + room.name +  "("+room.memory.unloadingLinks+")");
        // }
        if (!room.memory.receivingLink || room.memory.receivingLink.length === 0 || !room.memory.unloadingLinks) {
            return;
        }
        // console.log("2 "+roomName);
        let receiver = <StructureLink> Game.getObjectById(room.memory.receivingLink);
        if (!receiver) {
            console.log("Room " + room.name + " has a configured receiving link which doesn't exist! id=" + room.memory.receivingLink);
            return;
        }

        for (let link of (<RoomMemory> room.memory).unloadingLinks) {
            // console.log("3 " + roomName);
            // console.log("receiver=" + JSON.stringify(receiver));
            let sender = <StructureLink> Game.getObjectById(link.id);
            if (!sender) {
                console.log("Room " + room.name + " has a configured sending link which doesn't exist! id=" + link.id);
                continue;
            }
            if (sender.structureType !== STRUCTURE_LINK) {
                console.log("Room " + room.name + " has a configured sending link which isn't a link! id=" + link.id);
                continue;
            }
            // console.log("sender=" + JSON.stringify(sender));

            if (
                sender.energy >= sender.energyCapacity * 0.5
                // && sender.energy > sender.energyCapacity * 0.9
                && receiver.energy <= receiver.energyCapacity * 0.7
            ) {
                // console.log("4");
                sender.transferEnergy(receiver);
            }
        }
    }
}
Profiler.registerClass(MaintenanceLink, MaintenanceLink.name);
