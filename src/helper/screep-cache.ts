import {Profiler} from "../profiler";
import {GrafanaCollector} from "../statistics/grafana-collector";

export class ScreepCache {
    // private static roomRoleIsDone = new Map<string, string[]>();

    public static clearCache() {
        if (!(<any> Memory).cache) {
            (<any> Memory).cache = {};
        }
        (<any> Memory).cache.roomRoleIsDone = {};
        if (!(<any> Memory).cache.idIsUnloaded || Game.time % 500 === 0) {
            (<any> Memory).cache.idIsUnloaded = [];
        }

        if (!(<any> Memory).cache.roomRoleIsDone || Game.time % 10 === 0) {
            (<any> Memory).cache.roomRoleIsDone = [];
        }

        if (Game.time % GrafanaCollector.tickWhereToClear === 0) {
            (<any> Memory).cache.spawningCounter = {};
        }
    }

    public static isIdUnloaded(id: string): boolean {
        return (_.indexOf((<any> Memory).cache.idIsUnloaded, id) !== -1);
    }

    public static setIdUnloaded(id: string) {
        return (<any> Memory).cache.idIsUnloaded.push(id);
    }

    public static removeIdUnloaded(id: string) {
        _.remove((<any> Memory).cache.idIsUnloaded, (entry) => {
            return entry === id;
        });
    }

    public static isRoomDone(roomName: string, role: string): boolean {
        let tmp = (<any> Memory).cache.roomRoleIsDone[roomName];
        if (!tmp) {
            return false;
        } else {
            return (tmp.indexOf(role) >= 0);
        }
    }

    public static setRoomDone(roomName: string, role: string) {
        // console.log("setRoomDone for " + roomName + " role=" + role);
        if (!(<any> Memory).cache.roomRoleIsDone[roomName]) {
            (<any> Memory).cache.roomRoleIsDone[roomName] = [];
        }
        let tmp = (<any> Memory).cache.roomRoleIsDone[roomName];
        if (!tmp) {
            //will never happen, because we created if above, just to silence the compiler
            return;
        }
        if (tmp.indexOf(role) < 0) {
            (<any> Memory).cache.roomRoleIsDone[roomName].push(role);
        }
    }

    public static addCreepSpawningTotal(roomName: string, type: string, current: number, required: number) {
        if (!(<any> Memory).cache.spawningCounter) {
            (<any> Memory).cache.spawningCounter = {};
        }
        if (!(<any> Memory).cache.spawningCounter.creepSpawning) {
            (<any> Memory).cache.spawningCounter.creepSpawning = {};
        }
        if (!(<any> Memory).cache.spawningCounter.creepSpawning[roomName]) {
            (<any> Memory).cache.spawningCounter.creepSpawning[roomName] = {};
        }
        if (!(<any> Memory).cache.spawningCounter.creepSpawning[roomName][type]) {
            (<any> Memory).cache.spawningCounter.creepSpawning[roomName][type] = {};
        }
        let tmp = (<any> Memory).cache.spawningCounter.creepSpawning[roomName][type];
        tmp.current = current;
        tmp.required = required;
    }

    public static getCreepSpawning(): any {
        return (<any> Memory).cache.spawningCounter;
    }

}
Profiler.registerClass(ScreepCache, ScreepCache.name);
