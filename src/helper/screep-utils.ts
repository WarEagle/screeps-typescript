import {Profiler} from "../profiler";
export class ScreepUtils {
    // static getMaxEnergyAllowed(room: Room): number {
    //     return Math.min(1600, room.energyCapacityAvailable);
    // }
    //
    static countArray(creeps: Creep[], param2: (c: Creep) => boolean): number {
        let cnt: number = 0;
        for (let key in creeps) {
            let creep = creeps[key];
            if (param2((creep))) {
                cnt++;
            }
        }
        return cnt;
    }

    static count(creeps: {[p: string]: Creep}, param2: (c: Creep) => boolean): number {
        // return _.size(_.filter(creeps, filter));
        let cnt: number = 0;
        for (let key in creeps) {
            let creep = Game.creeps[key];
            if (param2((creep))) {
                cnt++;
            }
        }
        return cnt;
    }

    static roomsWithController(): Room[] {
        return _.filter(Game.rooms, (room) => room.controller && room.controller.my && room.controller.level >= 1);
    }

}
Profiler.registerClass(ScreepUtils, ScreepUtils.name);
