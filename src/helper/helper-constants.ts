export class RoomConstants {

    public roomConstants: any = [];

    constructor() {
        this.roomConstants["W76N29"] = {
            // displayName: "MainBase   ",

            attacker: [
                // {targetRoomName: "W78N29", cnt: 1},
            ],
            remoteBuilder: [
                //{targetRoomName: "W78N29", cnt: 2},//additional builder for upgrade
                // {targetRoomName: "W77N27", cnt: 3},// additional builder for upgrade
                {targetRoomName: "W77N27", cnt: 3},// additional builder for upgrade
            ],
            remoteHarvester: [
            ],
            defender: [
                //{targetRoomName: "W77N22", cnt: 2},// preparing room for conquer
                // {targetRoomName: "W76N23", cnt: 1},// preparing room for conquer
            ],
        };

        this.roomConstants["W78N29"] = {
            // displayName: "Exp 1 NEW ",
            remoteBuilder: [
                {targetRoomName: "W77N27", cnt: 3},// additional builder for upgrade
            ],
            defender: [
                {targetRoomName: "W77N29", cnt: 1},// just for vision
                {targetRoomName: "W75N23", cnt: 1},// preparing room for conquer
                // {targetRoomName: "W78N25", cnt: 1},// preparing room for conquer
                // {targetRoomName: "W77N22", cnt: 1},// preparing room for conquer
                // {targetRoomName: "W76N22", cnt: 1},// preparing room for conquer
                // {targetRoomName: "W77N21", cnt: 1},// preparing room for conquer
            ],
            remoteClaimer: [
                {targetRoomName: "W77N29"}, // just so the room isn't neutral
            ],
        };

        this.roomConstants["W77N27"] = {
            // displayName: "Exp 2      ",

            remoteBuilder: [
                // {targetRoomName: "W75N29", cnt: 1}, // first roads
                // {targetRoomName: "W77N29", cnt: 2},
                // {targetRoomName: "W78N29", cnt: 3},// additional builder for upgrade
                // {targetRoomName: "W77N27", cnt: 3},// additional builder for upgrade
                {targetRoomName: "W78N25", cnt: 1},// new room
                {targetRoomName: "W77N21", cnt: 2},// new room
                {targetRoomName: "W77N27", cnt: 3},// additional builder for upgrade
            ],
            defender: [
                // {targetRoomName: "W77N29", cnt: 1},// just for vision
                {targetRoomName: "W75N23", cnt: 1},// preparing room for conquer
            ],
        };

        this.roomConstants["W77N21"] = {
            // displayName: "South   ",

            attacker: [
            ],
            remoteBuilder: [
            ],
            remoteHarvester: [
            ],
            defender: [
            ],
        };

        this.roomConstants["W78N28"] = {
            // displayName: "West   ",

            attacker: [
            ],
            remoteBuilder: [
            ],
            remoteHarvester: [
            ],
            defender: [
            ],
        };

    }
}
