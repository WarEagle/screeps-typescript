import {RoomConstants} from "./helper-constants";
import {Profiler} from "../profiler";
import {RoomMemory} from "../room/room-memory";
import {ScreepCache} from "./screep-cache";
import {MyCreep} from "../types/my-creep";

export class HelperNavigation {

    private coordinatesToTry: {x: number, y: number}[] = [
        {x: 25, y: 25},
        //
        {x: 15, y: 15},
        {x: 35, y: 35},
        {x: 15, y: 35},
        {x: 35, y: 15},
        //
        {x: 40, y: 40},
        {x: 20, y: 40},
        {x: 40, y: 20},
        {x: 20, y: 20}
    ];

    public moveToRoom(creep: MyCreep, roomName: string): boolean {

        // correct room?
        if (creep.room.name === roomName) {
            return true;
        } else if (!roomName) {
            return false;
        } else {
            return this.performRoomChange(creep, roomName);
        }
    }

    public collectFromDesiredTarget(creep: MyCreep): boolean {
        if (!creep.memory.desiredTarget) {
            return false;
        }
        let desiredTarget = Game.getObjectById(creep.memory.desiredTarget);
        if (!desiredTarget) {
            ScreepCache.removeIdUnloaded(creep.memory.desiredTarget);
            delete creep.memory.desiredTarget;
            return false;
        }

        let rc;
        if (desiredTarget instanceof Resource) {
            rc = creep.pickup(desiredTarget);
        } else if (desiredTarget instanceof Structure) {
            rc = creep.withdraw(desiredTarget, RESOURCE_ENERGY);
        }

        // console.log("rc="+rc);
        if (rc === ERR_NOT_IN_RANGE) {
            creep.moveTo(<RoomPosition> desiredTarget);
            return true;
        } else if (rc === OK) {
            ScreepCache.removeIdUnloaded(creep.memory.desiredTarget);
            delete creep.memory.desiredTarget;
            return true;
        }
        return false;
    }

    //noinspection JSUnusedGlobalSymbols
    public collectEnergyFromGround(creep: MyCreep): boolean {
        if (creep.memory.desiredTarget) {
            return false;
        }

        let droppedEnergy: any = creep.pos.findClosestByRange(FIND_DROPPED_ENERGY, {
            filter: (x: any) => (
                x.room === creep.room
                && x.energy >= 400
            )
        });
        if (droppedEnergy) {
            //console.log(JSON.stringify(droppedEnergy));
            let tmp = creep.pickup(droppedEnergy);
            if (tmp === ERR_NOT_IN_RANGE) {
                creep.moveTo(droppedEnergy);
            }
            //console.log(tmp);
            return true;
        }
        return false;
    }

    public canTakeEnergyFromReceiverLink(creep: MyCreep): boolean {
        let rc: boolean = false;
        let link = new RoomConstants().roomConstants[creep.room.name];
        if (link && link.energySourceLink) {
            let loadingLink: any = Game.getObjectById(link.energySourceLink);
            if (loadingLink && loadingLink.energy > 0) {
                // console.log(creep.withdraw(loadingLink, RESOURCE_ENERGY));
                if (creep.withdraw(loadingLink, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(loadingLink);
                    rc = true;
                }
            } else {
                let source: any = creep.pos.findClosestByRange(FIND_DROPPED_ENERGY, {
                        filter: (x: any) => (x.room === creep.room)
                    })
                    ;
                if (source) {
                    if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
                        creep.moveTo(source);
                        return true;
                    }
                }
            }
        }
        return rc;
    }

    public unloadingToSpawnOrExtension(creep: MyCreep): boolean {
        let rc: boolean = false;

        let structure: any = undefined;
        if (creep.memory.unloadingTarget) {
            structure = Game.getObjectById(creep.memory.unloadingTarget);
        } else {
            structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
                    filter: (s: any) => (
                        (
                            s.structureType === STRUCTURE_SPAWN
                            || s.structureType === STRUCTURE_EXTENSION
                        )
                        && s.energy < s.energyCapacity
                    ),
                },
                {
                    maxOps: 500,
                    maxRooms: 1
                }
            );
            if (structure) {
                creep.memory.unloadingTarget = structure.id;
            } else {
                delete creep.memory.unloadingTarget;
            }
        }
        if (structure) {
            if (this.moveCloseTo(creep, structure.pos)) {
                let unloadRc = creep.transfer(structure, RESOURCE_ENERGY);
                // console.log(creep.name+" unloadRc="+unloadRc);
                if (unloadRc === ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                    //creep.transfer(structure, RESOURCE_ENERGY);
                    // console.log(creep.name+" rc="+rc);
                    // try to unload after move, it could be done in the same tick
                    // unloadRc = creep.transfer(structure, RESOURCE_ENERGY);
                }
                if (unloadRc === ERR_NO_PATH || unloadRc === ERR_FULL || unloadRc === ERR_INVALID_TARGET) {
                    delete creep.memory.unloadingTarget;
                }
                if (unloadRc === OK) {
                    delete creep.memory.unloadingTarget;
                }
            }
            rc = true;
        }
        return rc;
    }

    public unloadingEnergyToStorage(creep: MyCreep): boolean {
        let structure: any = creep.room.storage;

        if (!structure) {
            return false;
        }

        if (creep.transfer(structure, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            creep.moveTo(structure);
            // try to unload after move, it could be done in the same tick
            // creep.transfer(structure, RESOURCE_ENERGY);
            return true;
        }
        return false;
    }

    public unloadingEnergyToTerminalWithLimit(creep: MyCreep, limit: number): boolean {
        let structure: any = creep.room.terminal;

        if (!structure || structure.store.energy >= limit) {
            return false;
        }

        if (this.moveCloseTo(creep, structure)) {
            if (creep.transfer(structure, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
                // try to unload after move, it could be done in the same tick
                // creep.transfer(structure, RESOURCE_ENERGY);
            }
            return true;
        } else {
            return true;
        }
        // return false;
    }

    public unloadingEnergyToTower(creep: MyCreep, percentage: number): boolean {
        let structure: any = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
                filter: (s: any) => (
                    (s.structureType === STRUCTURE_TOWER
                    && s.energy <= s.energyCapacity * percentage
                    && s.room === creep.room)
                    // && s.energy < s.energyCapacity
                ),
            },
        );
        if (!structure) {
            return false;
        }
        if (creep.transfer(structure, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            creep.moveTo(structure);
            // try to unload after move, it could be done in the same tick
            // creep.transfer(structure, RESOURCE_ENERGY);
            return true;
        }
        return false;
    }

    public unloadingEnergyToLab(creep: MyCreep) {
        let structure: any = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
                filter: (s: any) => (
                    (s.structureType === STRUCTURE_LAB
                    && s.energy <= s.energyCapacity * 0.75
                    && s.room === creep.room)
                ),
            },
        );
        if (creep.transfer(structure, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            creep.moveTo(structure);
            // try to unload after move, it could be done in the same tick
            // creep.transfer(structure, RESOURCE_ENERGY);
            return true;
        }
        return false;
    }

    public unloadingMineralsToStorage(creep: MyCreep): boolean {
        let structure: any = creep.room.storage;
        if (!structure) {
            return false;
        }

        for (let resource in RESOURCES_ALL) {
            // console.log(JSON.stringify(creep.carry));
            if (creep.carry[RESOURCES_ALL[resource]] !== undefined && creep.carry[RESOURCES_ALL[resource]] > 0) {
                // console.log(creep.carry[RESOURCES_ALL[resource]]);
                if (creep.transfer(structure, RESOURCES_ALL[resource]) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                    return true;
                }
                break;
            }
        }
        return false;
    }

    public unloadingMineralsToTerminal(creep: MyCreep): boolean {
        let structure: any = creep.room.terminal;
        if (!structure) {
            return false;
        }
        if (_.sum(structure.store) >= 250000) {
            //terminal too full
            // console.log("completely full");
            return false;
        }

        //TODO also check, if more than X of the current mineral is stored

        // console.log(JSON.stringify(creep.carry));
        // for (let resource in creep.carry) {
        //     console.log(resource);
        // }

        for (let resource in creep.carry) {
            // console.log(JSON.stringify(creep.carry));
            if (creep.carry[creep.carry[resource]] !== undefined && creep.carry[creep.carry[resource]] > 0) {
                // console.log(creep.carry[RESOURCES_ALL[resource]]);
                if (creep.transfer(structure, creep.carry[resource]) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                    return true;
                }
                break;
            }
        }
        return false;
    }

    public transferFromContainer(creep: MyCreep): boolean {
        if (creep.memory.desiredTarget) {
            return false;
        }

        let structure = <Structure|undefined> creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s: any) => (
                    s.structureType === STRUCTURE_CONTAINER
                    && s.store[RESOURCE_ENERGY] >= creep.carryCapacity * 0.9
                    && s.room === creep.room
                    && !ScreepCache.isIdUnloaded(s.id)
                ),
            },
        );

        if (structure) {
            ScreepCache.setIdUnloaded(structure.id);
            creep.memory.desiredTarget = structure.id;

            if (creep.withdraw(structure, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        }
        return false;
    }

    public transferToUnloadingLink(creep: MyCreep, range?: number): boolean {
        let roomUnloadingLink: string = "";
        let memory = (<RoomMemory> creep.room.memory);
        if (!memory.receivingLink || !memory.unloadingLinks || memory.unloadingLinks.length === 0) {
            return false;
        }

        if (creep.memory.roomUnloadingLink) {
            roomUnloadingLink = creep.memory.roomUnloadingLink;
        } else {
            for (let i in memory.unloadingLinks) {
                let memoryUnloadingLinks = memory.unloadingLinks[i];
                if (memoryUnloadingLinks.targetRoomName === creep.memory.targetRoomName) {
                    roomUnloadingLink = memoryUnloadingLinks.id;
                    creep.memory.roomUnloadingLink = memoryUnloadingLinks.id;
                    break;
                }
            }
        }
        let unloadingLink: any = Game.getObjectById(roomUnloadingLink);
        // console.log(roomUnloadingLink);
        if (unloadingLink && unloadingLink.energy < unloadingLink.energyCapacity && (!range || unloadingLink.pos.inRangeTo(creep.pos, range))) {
            if (creep.transfer(unloadingLink, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(unloadingLink);
                return true;
            }
        } else {
            delete creep.memory.roomUnloadingLink;
        }
        return false;
    }

    public transferFromLink(creep: MyCreep): boolean {
        if (!(<RoomMemory> creep.room.memory).receivingLink) {
            // console.log("1 "+creep.name)
            return false;
        }

        if (creep.memory.desiredTarget) {
            return false;
        }

        let structure: any = Game.getObjectById(creep.room.memory.receivingLink);
        if (!structure) {
            // console.log("2 "+creep.name)
            return false;
        }
        if (structure.energy <= structure.energyCapacity * 0.5) {
            // if (structure.energy <= structure.energyCapacity * 0.7) {
            // console.log("\t3 "+creep.name)
            // console.log("\t3 "+structure.energy)
            // console.log("\t3 "+structure.energyCapacity * 0.9)
            return false;
        }

        if (creep.withdraw(structure, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            creep.moveTo(structure);
            return true;
        }
        // console.log("5 "+creep.name)
        return false;
    }

    public takeEnergyFromMyStorage(creep: MyCreep) {
        //take from storage
        let structure: any = creep.room.storage;
        if (structure && structure.store.energy >= creep.carryCapacity) {
            // console.log(1)
            if (structure.transfer(creep.getOriginalCreep(), RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        } else {
            return false;
        }
    }

    public takeEnergyFromMyTerminal(creep: MyCreep) {
        //take from storage
        let structure: any = creep.room.terminal;
        if (structure && structure.store[RESOURCE_ENERGY] >= creep.carryCapacity) {
            let rc = structure.transfer(creep.getOriginalCreep(), RESOURCE_ENERGY);
            if (rc === OK) {
                return true;
            } else if (rc === ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
                return true;
            }
            // console.log(creep.name + " " + rc);
        } else {
            return false;
        }
    }

    public takeEnergyFromMyContainer(creep: MyCreep) {
        let structure: any = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (s: any) => (s.structureType === STRUCTURE_CONTAINER
                && s.store[RESOURCE_ENERGY] >= creep.carryCapacity
                && s.room === creep.room
            ),
        });
        if (structure) {
            if (structure.transfer(creep.getOriginalCreep(), RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        } else {
            return false;
        }
    }

    public takeEnergyFromEnemyBuilding(creep: MyCreep) {
        for (let mineral in RESOURCES_ALL) {
            // console.log(mineral);
            let structure: any = creep.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES, {
                    filter: (s: any) =>
                        (
                            (
                                s.structureType === STRUCTURE_EXTENSION
                                || s.structureType === STRUCTURE_LAB
                                || s.structureType === STRUCTURE_SPAWN
                            )
                            && s.energy >= 20
                            && s.room === creep.room
                            && s.owner !== creep.owner
                        ),
                },
            );
            if (structure) {
                if (creep.withdraw(structure, RESOURCES_ALL[mineral]) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                } else {
                    if (!structure.owner.my) {
                        creep.say("♥ Thanks ♥", true);
                    }
                }
                // console.log(true);
                return true;
            }
        }
        // console.log(false);
        return false;
    }

    public takeMineralsFromStorage(creep: MyCreep) {
        //take from storage
        for (let mineral in RESOURCES_ALL) {
            // console.log(mineral);
            let structure: any = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (s: any) => (s.structureType === STRUCTURE_STORAGE
                        && s.store[RESOURCES_ALL[mineral]] >= 100
                        && s.room === creep.room
                    ),
                },
            );
            if (structure) {
                if (structure.transfer(creep.getOriginalCreep(), RESOURCES_ALL[mineral]) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                }
                // console.log(true);
                return true;
            }
        }
        // console.log(false);
        return false;
    }

    moveCloseTo(creep: MyCreep, pos: RoomPosition, distance?: number): boolean {
        if (distance && creep.pos.getRangeTo(pos) > distance) {
            creep.moveTo(pos);
            return false;
        } else if (creep.pos.getRangeTo(pos) > 2) {
            creep.moveTo(pos);
            return false;
        }
        return true;
    }

    public kite(distance: number, creep: MyCreep, targetCreep: Creep) {
        if (distance <= 5) {
            let targetDirection = creep.pos.getDirectionTo(targetCreep);

            //TODO improve this with some random
            let kiteDirection;
            switch (targetDirection) {
                case TOP:
                    kiteDirection = BOTTOM;
                    break;
                case TOP_RIGHT:
                    kiteDirection = BOTTOM_LEFT;
                    break;
                case RIGHT:
                    kiteDirection = LEFT;
                    break;
                case BOTTOM_RIGHT:
                    kiteDirection = TOP_LEFT;
                    break;
                case BOTTOM:
                    kiteDirection = TOP;
                    break;
                case BOTTOM_LEFT:
                    kiteDirection = TOP_RIGHT;
                    break;
                case LEFT:
                    kiteDirection = RIGHT;
                    break;
                case TOP_LEFT:
                    kiteDirection = BOTTOM_RIGHT;
                    break;
                default:
                    kiteDirection = BOTTOM;
            }
            if (creep.pos.x <= 2) {
                kiteDirection = TOP;
            } else if (creep.pos.x >= 48) {
                kiteDirection = BOTTOM;
            }
            creep.move(kiteDirection);
            creep.rangedAttack(targetCreep);
        }
    }

    private performRoomChange(creep: MyCreep, roomName: string): boolean {
        if (this.roomChangeInProgress(creep, roomName)) {
            return false;
        }

        let moveToOpts = <MoveToOpts> {reusePath: 50, maxOps: 5000, maxRooms: 10};
        // let direction = this.findDirectionOfExit(creep, roomName);
        // console.log(creep.name + " " +creep.room.name + " "+ roomName);
        // console.log(JSON.stringify(direction));
        // let exit = creep.room.findExitTo(roomName);
        // let posExit = creep.pos.findClosestByRange(exit);
        // creep.moveTo(posExit, moveToOpts);

        for (let coord of this.coordinatesToTry) {
            let pos: RoomPosition = new RoomPosition(coord.x, coord.y, roomName);
            let rc = creep.moveTo(pos, moveToOpts);
            if (rc !== ERR_NO_PATH) {
                return false;
            }
        }
        return false;
    }

    private roomChangeInProgress(creep: MyCreep, targetRoomName: string): boolean {
        // if (1 < 2) {
        //     return false;
        // }
        // if (creep.name !== "RLO W77N27 #2") {
        //     return false;
        // }
        // console.log("roomChangeInProgress from " + creep.room.name + " to " + targetRoomName + " for " + creep.name);

        let showMissingFlagMessage: boolean = false;

        if (creep.memory.path) {
            // console.log("has path " + creep.name);
            let path: PathStep[] = creep.memory.path;

            let pathStep = path[path.length - 1];
            if (creep.pos.x === pathStep.x && creep.pos.y === pathStep.y) {
                //goal reached
                // console.log(JSON.stringify(path[path.length - 1]));
                // console.log(JSON.stringify(creep.pos));
                delete creep.memory.path;
                return false;
            } else {
                let rc = creep.moveByPath(path);
                if (rc === -5) {
                    delete creep.memory.path;
                }
                if (creep.pos.x === pathStep.x && creep.pos.y === pathStep.y) {
                    delete creep.memory.path;
                }
                // let rc = creep.moveByPath(path.path);
                // console.log("rc=" + rc);
                return true;
            }
        }
        // console.log("has NO path " + creep.name);


        // if (1 < 2) {
        //     return false;
        // }
        let flagNameFrom = "WP " + creep.room.name + " -> " + targetRoomName;
        let flagNameTo = "WP " + targetRoomName + " -> " + creep.room.name;
        let flagFrom = Game.flags[flagNameFrom];
        if (!flagFrom) {
            if (showMissingFlagMessage) {
                console.log("No flag '" + flagNameFrom + "' found.");
            }
            return false;
        }
        let flagTo = Game.flags[flagNameTo];
        if (!flagTo) {
            if (showMissingFlagMessage) {
                console.log("No flag '" + flagNameTo + "' found.");
            }
            return false;
        }

        let path: PathStep[];

        // let path: {
        //     path: RoomPosition[];
        //     ops: number;
        // };
        //
        if (flagFrom.memory.path) {
            path = flagFrom.memory.path;
        } else {
            // path = PathFinder.search(flagFrom.pos, {pos: flagTo.pos, range: 0}, {
            //     maxRooms: 1,
            // });
            path = flagFrom.room.findPath(flagFrom.pos, flagTo.pos);
            flagFrom.memory.path = path;
        }

        // console.log("Moving to " + flagFrom.name);
        if (creep.pos.getRangeTo(flagFrom) > 0) {
            creep.moveTo(flagFrom);
        } else {
            creep.memory.path = path;
        }

        return true;
    }
}

Profiler.registerClass(HelperNavigation, HelperNavigation.name);
