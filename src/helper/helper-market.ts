import {Profiler} from "../profiler";
export class HelperMarket {

    public static findBest(resource: string, buy: boolean) {
        let direction = buy === true ? "sell" : "buy";
        let market = Game.market;
        let costOfEnergy = 0.1;
        let amount = 10000;
        let myRoom = "W77N27";

        let orders = market.getAllOrders((entry) => {
            return entry.resourceType === resource
                && entry.type === direction
                && entry.remainingAmount > 0;
        });

        console.log(JSON.stringify(orders));

        let listOfOptions: {orderId: string, roomName: string, totalPrice: number}[] = [];

        _.forEach(orders, (order) => {
            let transferCosts = Game.market.calcTransactionCost(1000, myRoom, order.roomName!);
            let totalCosts = transferCosts * costOfEnergy;
            //console.log(JSON.stringify(order.roomName + " + " + (order.price * amount) + " + " + totalCosts));
            listOfOptions.push({
                roomName: order.roomName!,
                totalPrice: (order.price * amount + totalCosts),
                orderId: order.id
            });
        });
        _.sortBy(listOfOptions, (e1) => {
            return e1.totalPrice;
        });

        // console.log(JSON.stringify(listOfOptions));
        if (buy === true) {
            console.log("best buy: " + JSON.stringify(_.first(listOfOptions)));
        }
        if (buy === false) {
            // console.log("best buy: " + JSON.stringify(_.first(listOfOptions)));
            console.log("best sell: " + JSON.stringify(_.last(listOfOptions)));
        }

    }
}
Profiler.registerClass(HelperMarket, HelperMarket.name);
