import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";
export class HelperCalculation {

    //noinspection JSUnusedGlobalSymbols
    public static costsOfCreep(creep: MyCreep) {
        let sum = 0;
        for (let part of creep.body) {
            sum += BODYPART_COST[part.type];
        }
        return sum;
    }

    public static costsForBody(body: string[]) {
        let sum = 0;
        for (let part of body) {
            sum += BODYPART_COST[part];
        }
        return sum;
    }
}
Profiler.registerClass(HelperCalculation, HelperCalculation.name);
