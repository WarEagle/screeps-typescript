import {RoomMemory, MiningContainer, MiningOperation} from "../room/room-memory";
import {BaseDesigner} from "../room/base-designer";
import {Profiler} from "../profiler";

export class RemoteMining {
    private baseDesigner = new BaseDesigner();

    public fillMissingVariables(): void {
        for (let roomName in Game.rooms) {
            let room = Game.rooms[roomName];
            for (let sourceRoomName in (<RoomMemory>room.memory).miningOperations) {
                if (!(<RoomMemory>room.memory).miningOperations[sourceRoomName]) {
                    continue;
                }
                for (let targetRoomName in (<RoomMemory>room.memory).miningOperations[sourceRoomName]) {
                    let mem = (<RoomMemory>room.memory).miningOperations[sourceRoomName][targetRoomName];
                    // console.log(JSON.stringify(mem));
                    let sourceRoom = Game.rooms[sourceRoomName];
                    let targetRoom = Game.rooms[targetRoomName];
                    if (!sourceRoom || !targetRoom) {
                        continue;
                    }
                    this.buildRoadsToSource(mem, sourceRoom, targetRoom);
                    this.markContainer(mem, sourceRoom, targetRoom);
                    this.addDistance(mem, sourceRoom);
                    this.buildRoadsBetweenSources(mem, targetRoom);
                    this.buildRoadsToController(mem, sourceRoom, targetRoom);
                    this.addCntSources(mem, targetRoom);
                }
            }
        }
    }

    public newMining(sourceRoomName: string, targetRoomName: string): void {
        this.addRoomToMemory(sourceRoomName, targetRoomName);
    }

    public addRoomToMemory(sourceRoomName: string, targetRoomName: string): void {
        let sourceRoom = Game.rooms[sourceRoomName];

        if (!sourceRoom) {
            console.log("Source room " + sourceRoomName + " not found.");
            return;
        }

        if (!(<RoomMemory>(sourceRoom.memory)).miningOperations) {
            (<RoomMemory>(sourceRoom.memory)).miningOperations = {};
        }
        let mem = (<RoomMemory>(sourceRoom.memory)).miningOperations;
        if (!mem[sourceRoom.name]) {
            mem[sourceRoom.name] = {};
        }
        if (!mem[sourceRoom.name][targetRoomName]) {
            mem[sourceRoom.name][targetRoomName] = {
                hasControllerRoads: false,
                hasSourceRoads: false,
                hasExternalSourceRoads: false,
                hasContainer: false,
                hasCntSource: false,
                isSourceKeeper: false,
                containers: [],
            };
        }
    }

    public buildRoadsToSource(mem: any, sourceRoom: Room, targetRoom: Room): void {
        if (mem.hasExternalSourceRoads === true) {
            return;
        }
        let sourcePos = sourceRoom.find(FIND_MY_SPAWNS)[0];
        this.baseDesigner.buildRoad((<StructureSpawn>sourcePos).pos, (<any>targetRoom.controller).pos);

        let sources = targetRoom.find(FIND_SOURCES);
        for (let source in sources) {
            this.baseDesigner.buildRoad((<StructureSpawn>sourcePos).pos, (<Source>sources[source]).pos);
        }
        //let mem = (<RoomMemory>room.memory).miningOperations[sourceRoomName][targetRoomName];
        //console.log(JSON.stringify(mem));
        // sourceRoom = sourceRoom;
        // targetRoom = targetRoom;
        mem.hasExternalSourceRoads = true;
    }

    public addDistance(mem: MiningOperation, sourceRoom: Room): void {
        // if (1 < 2){
        //     return;
        // }
        if (!mem || mem.hasContainer === false) {
            return;
        }
        let source = <StructureSpawn>sourceRoom.find(FIND_MY_SPAWNS)[0];

        for (let container of mem.containers) {
            if (container.distance && container.distance !== 0) {
                return;
            }
            let containerStructure = <StructureContainer>Game.getObjectById(container.containerId!);
            if (!containerStructure) {
                console.log("Problem finding containerstructure " + container.containerId + " in " + sourceRoom.name);
                return;
            }
            let result = PathFinder.search(source.pos, containerStructure.pos);
            container.distance = result.path.length;
            // console.log(result.path.length);
        }

    }

    public markContainer(mem: any, sourceRoom: Room, targetRoom: Room): void {

        if (!mem || mem.hasContainer === true) {
            return;
        }

        let sourcePos = <StructureSpawn>sourceRoom.find(FIND_MY_SPAWNS)[0];
        let sources = targetRoom.find(FIND_SOURCES);
        for (let source in sources) {
            let src = (<Source>sources[source]);
            let targetPos = src.pos;
            //route is backwards, so we can take the first step of it
            let route = targetPos.findPathTo(sourcePos, {
                ignoreCreeps: true,
                ignoreRoads: false,
            });
            if (route && route.length > 0) {
                let pos = route[0];
                let container: MiningContainer = {
                    x: pos.x,
                    y: pos.y,
                    sourceId: src.id,
                    isBuilt: false,
                    distance: 0,
                };
                if (!mem.containers) {
                    mem.containers = [];
                }
                mem.containers.push(container);
            }
        }

        mem.hasContainer = true;
    }

    public buildRoadsToController(mem: any, sourceRoom: Room, targetRoom: Room): void {
        if (mem.hasControllerRoads === true) {
            return;
        }

        let sourcePos = sourceRoom.find(FIND_MY_SPAWNS)[0];
        this.baseDesigner.buildRoad((<StructureSpawn>sourcePos).pos, (<any>targetRoom.controller).pos);
        mem.hasControllerRoads = true;
    }

    public buildRoadsBetweenSources(mem: any, targetRoom: Room): void {
        if (mem.hasSourceRoads === true) {
            return;
        }

        let sources = targetRoom.find(FIND_SOURCES);
        if (_.size(sources) === 2) {
            this.baseDesigner.buildRoad((<Source>sources[0]).pos, (<Source>sources[1]).pos);
        }
        mem.hasSourceRoads = true;
    }

    private addCntSources(mem: any, targetRoom: Room) {
        if (mem.hasCntSource === true) {
            return;
        }

        mem.cntSources = _.size(targetRoom.find(FIND_SOURCES));

        mem.hasCntSource = true;
    }
}

Profiler.registerClass(RemoteMining, RemoteMining.name);

global.newMining = function (source: string, target: string) {
    let rm = new RemoteMining();
    rm.newMining(source, target);
};
