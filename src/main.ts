import {MainLogic} from "./main-logic";
import {ProfilerStorage} from "./statistics/profiler-archiver";
import {ScreepCache} from "./helper/screep-cache";
import {GrafanaCollector} from "./statistics/grafana-collector";
import {HelperMarket} from "./helper/helper-market";

// Any modules that you use that modify the game's prototypes should be require'd
// before you require the profiler.
//noinspection TsLint
const profiler = require("screeps-profiler");

// This line monkey patches the global prototypes.
profiler.enable();

global.tkt = function () {
    let pos1: RoomPosition = <RoomPosition>Game.rooms["W76N28"].getPositionAt(25, 25);
    let pos2: RoomPosition = <RoomPosition>Game.rooms["W78N28"].getPositionAt(25, 25);

    let result = PathFinder.search(pos1, pos2);

    console.log(JSON.stringify(result));
    console.log(result.path.length);
};

global.bestPrice = function (resource: string, buy: boolean) {
    HelperMarket.findBest(resource, buy);
};

global.tryToSpawn = function (body: string[], memory: any): string {
    let spawns = _.filter(Game.structures, (structure: Structure) => {
        return structure.structureType === STRUCTURE_SPAWN && structure.isActive();
    });

    let message: string = "NOK";
    spawns.forEach((spawn: StructureSpawn) => {
        if (message !== "NOK") {
            return;
        }
        if (!spawn.spawning || spawn.spawning.remainingTime <= 0) {
            let rc = spawn.createCreep(body, undefined, memory);
            if (_.isString(rc)) {
                message = "OK " + spawn.name + " " + spawn.room;
            }
        }
    });
    return message;
};

global.t1 = function () {
    Game.profiler.reset();
};

global.t2 = function () {
    if (Game.time % 3 === 0) {
        Game.profiler.reset();
    }
    if (Game.time % 3 === 1) {
        Game.profiler.profile(1);
        // Game.profiler.profile(1, "RoleRemoteBuilder.run");
    }
};

global.removeConstructions = function (roomName: string) {
    let room = Game.rooms[roomName];
    let sites: any = room.find(FIND_MY_CONSTRUCTION_SITES);
    for (let s of sites) {
        s.remove();
    }
    return "done";
};

/**
 * Returns html for a button that will execute the given command when pressed in the console.
 * @param id (from global.getId(), value to be used for the id property of the html tags)
 * @param type (resource type, pass undefined most of the time. special parameter for storageContents())
 * @param text (text value of button)
 * @param command (command to be executed when button is pressed)
 * @param browserFunction {boolean} (true if command is a browser command, false if its a game console command)
 * @returns {string}
 * Author: Helam
 */

global.makeButton = function (id: any, type: any, text: any, command: any, browserFunction = false) {
    let outstr = ``;
    let handler = ``;
    if (browserFunction) {
        outstr += `<script>let bf${id}${type} = ${command}</script>`;
        handler = `bf${id}${type}()`;
    } else {
        handler = `customCommand${id}${type}(\`${command}\`)`;
    }
    outstr += `<script>let customCommand${id}${type} = function(command) { $('body').injector().get('Connection').sendConsoleCommand(command) }</script>`;
    outstr += `<input type="button" value="${text}" style="background-color:#555;color:white;" onclick="${handler}"/>`;
    return outstr;
};

PathFinder.use(true);

//noinspection JSUnusedGlobalSymbols
/**
 * Screeps system expects this "loop" method in main.js to run the
 * application. If we have this line, we can be sure that the globals are
 * bootstrapped properly and the game loop is executed.
 * http://support.screeps.com/hc/en-us/articles/204825672-New-main-loop-architecture
 *
 * @export
 */
export function loop() {
    profiler.wrap(function () {
        // Main.js logic should go here.
        GrafanaCollector.reset();
        ScreepCache.clearCache();

        let mainLogic = new MainLogic();
        mainLogic.run();

        GrafanaCollector.addDetailedCpu("ProfilerStorage", () => {
            ProfilerStorage.run();
        });
        GrafanaCollector.addDetailedCpu("GrafanaCollector", () => {
            GrafanaCollector.run();
        });
    });
}
