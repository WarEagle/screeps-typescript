import {StatisticsCollector} from "./statistics-collector";
import {HelperCalculation} from "../helper/helper-calculation";
import {Profiler} from "../profiler";
import {BaseRole} from "../roles/common/role-base";

export class SpawnerWrapper {
    static createCreep(spawner: StructureSpawn, body: string[], creepRole: BaseRole, variables: any, namePrefix?: string): number | string {
        // console.log("body=" + JSON.stringify(body));
        // console.log("spawner.room.energyAvailable=" + spawner.room.energyAvailable);

        if (spawner.spawning && spawner.spawning.remainingTime > 0) {
            return ERR_BUSY;
        }

        let costs = HelperCalculation.costsForBody(body);
        // console.log("costs=" + costs);
        if (costs <= spawner.room.energyAvailable) {
            StatisticsCollector.addSpawnerCall();
            variables.spawnedIn = spawner.room.name;
            let name = this.generateCreepName(creepRole, spawner, namePrefix);
            return spawner.createCreep(body, name, variables);
        } else {
            return ERR_NOT_ENOUGH_ENERGY;
        }
    }

    private static generateCreepName(creepRole: BaseRole, spawner: StructureSpawn, namePrefix?: string) {
        if (!(<any> Memory).creepNameNumber) {
            (<any> Memory).creepNameNumber = 0;
        }

        let name = undefined;
        let cnt = 0;
        let found: boolean = true;
        do {
            name = creepRole.roleShortName() + " " + spawner.pos.roomName + " #" + cnt++;
            if (namePrefix) {
                name += " " + namePrefix;
            }
            found = !!Game.creeps[name];
        } while (found);
        // console.log("possible creep name=" + name);
        return name;
    }
}
Profiler.registerClass(SpawnerWrapper, SpawnerWrapper.name);
