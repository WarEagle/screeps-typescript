import {Profiler} from "../profiler";

export class CpuMeasurerer {

    private startCpu: number;
    private measureEnabled = true;

    constructor(private name: string) {
        if (this.measureEnabled) {
            this.start();
        } else {
            this.name = name;
            this.startCpu = 0;
        }
    }

    public start() {
        if (this.measureEnabled) {
            this.startCpu = Game.cpu.getUsed();
        }
    }

    //noinspection JSUnusedGlobalSymbols
    public stop() {
        if (this.measureEnabled) {
            if (Game.rooms["sim"]) {
                // used cpu doesn't work for simulation, so don't spam the log
                return;
            }

            let cpu: number = Game.cpu.getUsed() - this.startCpu;
            if (cpu <= 1) {
                // don't report tiny things
                return;
            }

            console.log(this.name + " used " + Math.ceil(cpu));
        }
    }
}
Profiler.registerClass(CpuMeasurerer, CpuMeasurerer.name);
