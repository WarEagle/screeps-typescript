import {Profiler} from "../profiler";

export class StatisticsCollector {

    private static cntSpawnerCalls: number = 0;
    private static startMeasurement: number = 0;

    public static reset() {
        this.cntSpawnerCalls = 0;
        this.startMeasurement = Game.time;
    }

    public static addSpawnerCall() {
        this.cntSpawnerCalls++;
        // console.log("addSpawnerCall called");
    }

    //noinspection JSUnusedGlobalSymbols
    public static printReport() {
        // let rc = "";
        // rc += "SpawnerController Calls: " + this.cntSpawnerCalls ;
        // rc += "\n";
        // rc += "Cpu used: " + Math.ceil(Game.cpu.getUsed());
        // console.log(rc);
    }
}
Profiler.registerClass(StatisticsCollector, StatisticsCollector.name);
