import {RoomInfo} from "../room/room-info";
import {Profiler} from "../profiler";
import {ScreepUtils} from "../helper/screep-utils";

export class WorldInfoCollector {
    public static TAG_ROOMINFO: string = "roominfo";
    public static TAG_SCOUTED: string = "scouted";
    public static TAG_UNKNOWN: string = "unknown";

    public collectInfos() {
        if (Game.time % 200 === 0) {
            this.assignRooms();
        }
    }

    public assignRooms() {
        console.log("assignRooms called at " + Game.time);
        let map = Game.map;

        let roomAssignments: any = {};
        let roomInfo: {
            [roomName: string]: RoomInfo
        };
        roomInfo = {};

        let roomsToScout: string[] = [];
        for (let room of ScreepUtils.roomsWithController()) {
            this.getInformationOfNeighborRooms(map, room, roomAssignments, roomsToScout, roomInfo);
        }
        // console.log("Rooms to scout: " + JSON.stringify(roomsToScout));
        // console.log("Room info: " + JSON.stringify(roomInfo));
        this.storeInMemory(roomInfo, roomsToScout);
    }

    private storeInMemory(roomInfo: {[p: string]: RoomInfo}, roomsToScout: string[]) {
        if (!Memory[WorldInfoCollector.TAG_ROOMINFO]) {
            Memory[WorldInfoCollector.TAG_ROOMINFO] = {};
        }

        if (!Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED]) {
            Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED] = {};
        }

        if (!Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_UNKNOWN]) {
            Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_UNKNOWN] = {};
        }

        for (let i in roomInfo) {
            Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED][i] = roomInfo[i];
        }

        Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_UNKNOWN] = [];

        for (let i in roomsToScout) {
            // console.log(Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED][roomsToScout[i]]);
            // console.log(roomsToScout[i]);
            if (!Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED][roomsToScout[i]]) {
                Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_UNKNOWN].push(
                    {
                        name: roomsToScout[i],
                    }
                );
            }
        }
    }

    private getInformationOfNeighborRooms(map: GameMap, room: Room, roomAssignments: any, roomsToScout: string[], roomInfo: {[p: string]: RoomInfo}): void {
        let exists: any = map.describeExits(room.name);
        // console.log(JSON.stringify(exists));
        roomAssignments[room.name] = {};
        this.runRoomScan(room.name, roomInfo, roomAssignments, roomsToScout);
        this.checkDirection(roomAssignments, exists, "1", roomsToScout, roomInfo);
        this.checkDirection(roomAssignments, exists, "3", roomsToScout, roomInfo);
        this.checkDirection(roomAssignments, exists, "5", roomsToScout, roomInfo);
        this.checkDirection(roomAssignments, exists, "7", roomsToScout, roomInfo);
    }

    private checkDirection(roomAssignments: any, exists: any, direction: string, roomsToScout: string[], roomInfo: any) {
        let roomName = exists[direction];
        if (!roomName) {
            return;
        }
        this.runRoomScan(roomName, roomInfo, roomAssignments, roomsToScout);
    }

    private runRoomScan(roomName: string, roomInfo: any, roomAssignments: any, roomsToScout: string[]) {
        // console.log(Game.rooms[roomName]);
        if (Game.rooms[roomName]) {
            this.scanRoom(roomName, roomInfo);
        } else {
            if (!roomAssignments[roomName] && Game.map.isRoomAvailable(roomName)) {
                roomsToScout.push(roomName);
            }
        }
    }

    private scanRoom(roomName: string, roomInfo: any) {
        let info = new RoomInfo();
        let room = Game.rooms[roomName];
        let sources = room.find(FIND_SOURCES);
        info.cntSources = sources ? sources.length : 0;

        if (!room.controller) {
            info.isEmpty = false;
            info.isEnemy = true;
            info.isKeeper = false;
        } else if (!room.controller.owner) {
            info.isEmpty = true;
            info.isEnemy = false;
            info.isKeeper = false;
        } else if (!room.controller.my) {
            info.isEmpty = false;
            info.isEnemy = true;
            info.isKeeper = false;
        }
        info.isRoomAvailable = Game.map.isRoomAvailable(roomName);

        info.cntWorkableFieldsController = this.getWorkableFieldsController(room.controller);
        info.cntWorkableFieldsSource = this.getWorkableFieldsSources(sources);
        info.timeScanned = Game.time;

        roomInfo[roomName] = info;

    }

    private getWorkableFieldsController(controller: StructureController|any): number {
        if (!controller || !(controller instanceof StructureController)) {
            return 0;
        }
        return this.getWorkableFields(controller.pos.x, controller.pos.y, controller.room);
    }

    private getWorkableFieldsSources(sources: any[]): number {
        // console.log(JSON.stringify(sources))
        if (!sources || sources.length === 0) {
            return 0;
        }
        let cnt = 0;
        for (let source of sources) {
            cnt += this.getWorkableFields(source.pos.x, source.pos.y, source.room);
        }
        return cnt;
    }

    private getWorkableFields(x: number, y: number, room: Room): number {
        let tmp = 0;
        let lookAtResult = <LookAtResultWithPos[]> room.lookAtArea(y - 1, x - 1, y + 1, x + 1, true);

        for (let result of lookAtResult) {
            tmp += this.isWorkable(result, x, y) ? 1 : 0;
        }

        return tmp;
    }

    private isWorkable(lookResult: LookAtResultWithPos, x: number, y: number): boolean {
        if (x === lookResult.x && y === lookResult.y) {
            return false;
        }
        return lookResult.terrain === "plain" || lookResult.terrain === "swamp";
    }

    public static getCntWorkableFieldsSource(roomName: string): number {
        let rc = -1;
        let room = Game.rooms[roomName];
        if (room
            && Memory[WorldInfoCollector.TAG_ROOMINFO]
            && Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED]
            && Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED][roomName]
        ) {
            rc = Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED][roomName].cntWorkableFieldsSource;
        }
        return rc;
    }
}
Profiler.registerClass(WorldInfoCollector, WorldInfoCollector.name);
