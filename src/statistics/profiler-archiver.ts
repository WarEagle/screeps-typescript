// import {Profiler} from "../profiler";

export class ProfilerStorage {

    public static run(): void {
        //Inactive
        if (1 < 2) {
            return;
        }

        if (Game.time % 3 === 0) {
            return;
        }

        let currentMemory = Memory.profiler;
        currentMemory.currentTimestamp = Game.time;

        if (!(<any> Memory).archivedProfiler) {
            (<any> Memory).archivedProfiler = {};
            (<any> Memory).archivedProfiler.calls = [];
            (<any> Memory).archivedProfiler.time = [];
        }
        let objTime: any = {time: Game.time};
        let objCalls: any = {time: Game.time};
        for (let i in Memory.profiler.map) {
            //noinspection JSUnfilteredForInLoop
            objCalls[i] = Memory.profiler.map[i].calls;
            //noinspection JSUnfilteredForInLoop
            objTime[i] = Memory.profiler.map[i].time;
        }
        (<any> Memory).archivedProfiler.calls.push(objCalls);
        (<any> Memory).archivedProfiler.time.push(objTime);

        Game.profiler.reset();
        Game.profiler.background();
    }
}
