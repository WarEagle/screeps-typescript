import {GlobalRoomsController} from "../room/global-rooms-controller";
import {ScreepCache} from "../helper/screep-cache";
import {ScreepUtils} from "../helper/screep-utils";

export class GrafanaCollector {
    // TODO sync with screeps-updater
    public static tickWhereToClear = 5;
    private static globalRoomsController: GlobalRoomsController = new GlobalRoomsController();

    static reset() {
        // if (Game.time % GrafanaCollector.tickWhereToClear === 0) {
        //     //don't run every time
        //     return;
        // }
        let mem = (<any> Memory).stats;
        if (!mem) {
            mem = {};
        }
        mem.room = {};
        mem.detailedCpu = {};
        mem.roomSummary = {};
    }

    static run() {

        if (Game.time % GrafanaCollector.tickWhereToClear === 0) {
            //don't run every time
            return;
        }

        let mem = (<any> Memory).stats;
        if (!mem) {
            mem = {};
        }

        mem.cpu = Game.cpu;
        mem.gcl = Game.gcl;
        mem.memory = {
            used: RawMemory.get().length
        };
        mem.tick = Game.time;

        mem.market = {
            credits: Game.market.credits,
            num_orders: Game.market.orders ? Object.keys(Game.market.orders).length : 0,
        };

        mem.rooms = Game.rooms;

        let roomsWithController = ScreepUtils.roomsWithController();
        for (let roomCnt in roomsWithController) {
            let roomName = roomsWithController[roomCnt].name;

            if (!Game.rooms[roomName].controller) {
                continue;
            }

            if ((<any> Game).rooms[roomName].controller.level <= 0) {
                continue;
            }

            this.fillRoomStatistics(mem, roomName);
        }

        this.fillCurrentCreepStatistics(mem);
        this.fillSpawningCreepStatistics(mem);

        //must be last line!
        mem.cpu.used = Game.cpu.getUsed();
    }

    private static fillRoomSummary(mem: any, roomName: string) {
        if (Game.rooms[roomName].storage) {
            mem.roomSummary[roomName].storageEnergy = (<any> Game.rooms[roomName]).storage.store.energy;
        }

        if (Game.rooms[roomName].terminal) {
            mem.roomSummary[roomName].terminalEnergy = (<any> Game.rooms[roomName]).terminal.store.energy;
        } else {
            mem.roomSummary[roomName].terminalEnergy = 0;
        }

        mem.roomSummary[roomName].energyAvailable = Game.rooms[roomName].energyAvailable;
        mem.roomSummary[roomName].energyCapacityAvailable = Game.rooms[roomName].energyCapacityAvailable;
        mem.roomSummary[roomName].maxEnergyAllowed = this.globalRoomsController.getController(Game.rooms[roomName], true).getMaxEnergyAllowed();

        let energy: number = 0;
        let energyCapacity: number = 0;
        _.forEach(Game.rooms[roomName].find(FIND_SOURCES), function (entry: any) {
            energy += entry.energy;
            energyCapacity += entry.energyCapacity;
        });
        mem.roomSummary[roomName].sourceEnergyCapacity = energyCapacity;
        mem.roomSummary[roomName].sourceEnergy = energy;

        let towerEnergy: number = 0;
        let containerEnergy: number = 0;
        let linkEnergy: number = 0;
        let isSpawning: number = 0;
        let isNotSpawning: number = 0;
        let missingHitPoints: number = 0;
        let missingHitPointsRoads: number = 0;
        _.forEach(Game.rooms[roomName].find(FIND_STRUCTURES), function (entry: Structure) {
            if (entry.structureType === STRUCTURE_TOWER) {
                towerEnergy += (<StructureTower> entry).energy;
            }
            if (entry.structureType === STRUCTURE_CONTAINER) {
                containerEnergy += (<StructureContainer> entry).store.energy;
            }
            if (entry.structureType === STRUCTURE_LINK) {
                linkEnergy += (<StructureLink>  entry).energy;
            }
            if (entry.structureType === STRUCTURE_SPAWN) {
                if ((<StructureSpawn> entry).spawning) {
                    isSpawning++;
                } else {
                    isNotSpawning++;
                }
            }
            if ((entry.structureType !== STRUCTURE_WALL) && (entry.structureType !== STRUCTURE_RAMPART)) {
                missingHitPoints += (entry.hitsMax - entry.hits);
            }
            if (entry.structureType === STRUCTURE_ROAD) {
                missingHitPointsRoads += (entry.hitsMax - entry.hits);
            }
        });

        mem.roomSummary[roomName].towerEnergy = towerEnergy;
        mem.roomSummary[roomName].containerEnergy = containerEnergy;
        mem.roomSummary[roomName].linkEnergy = linkEnergy;
        mem.roomSummary[roomName].isSpawning = isSpawning;
        mem.roomSummary[roomName].isNotSpawning = isNotSpawning;
        mem.roomSummary[roomName].missingHitPoints = missingHitPoints;
        mem.roomSummary[roomName].missingHitPointsRoads = missingHitPointsRoads;

        mem.roomSummary[roomName].totalConstructionTodo = 0;
        _.forEach(Game.constructionSites, function (entry: ConstructionSite) {
            // console.log(JSON.stringify(entry));
            if (entry.pos.roomName === roomName) {
                // console.log(1);
                mem.roomSummary[roomName].totalConstructionTodo += (entry.progressTotal - entry.progress);
            }
        });

    }

    private static fillRoomStatistics(mem: any, roomName: string) {
        if (!mem.room) {
            mem.room = {};
        }
        if (!mem.roomSummary) {
            mem.roomSummary = {};
        }
        mem.room[roomName] = {};
        mem.roomSummary[roomName] = {};

        mem.room[roomName].controller = Game.rooms[roomName].controller;
        mem.room[roomName].storage = Game.rooms[roomName].storage;
        mem.room[roomName].terminal = Game.rooms[roomName].terminal;
        this.fillRoomSummary(mem, roomName);
    }

    private static fillSpawningCreepStatistics(mem: any) {
        mem.creepSpawning = ScreepCache.getCreepSpawning().creepSpawning;
        // console.log(JSON.stringify(ScreepCache.getCreepSpawning()));
    }

    private static fillCurrentCreepStatistics(mem: any) {
        mem.roles = {};
        let roles: any = {};
        for (let creepName in Game.creeps) {
            let creep = Game.creeps[creepName];
            if (!roles[creep.memory.role]) {
                roles[creep.memory.role] = {};
                roles[creep.memory.role].cnt = 0;
            }
            roles[creep.memory.role].cnt++;
        }
        mem.roles = roles;
    }

    public static addDetailedCpu(name: string, callback: () => void) {
        // if (name.indexOf("ldHarvester")>-1) {
        //     console.log(Game.time)
        // }
        let start = Game.cpu.getUsed();
        callback();
        let end = Game.cpu.getUsed();
        if (!(<any> Memory).stats) {
            (<any> Memory).stats = {};
        }
        if (!(<any> Memory).stats.detailedCpu) {
            (<any> Memory).stats.detailedCpu = {};
        }

        let mem = (<any> Memory).stats.detailedCpu;

        //TODO make this nicer
        name = name.replace(" ", "_");
        name = name.replace(" ", "_");
        name = name.replace(" ", "_");
        name = name.replace(" ", "_");
        name = name.replace(" ", "_");
        name = name.replace(" ", "_");
        name = name.replace(" ", "_");
        name = name.replace(" ", "_");

        if (!mem[name]) {
            mem[name] = (end - start);
        } else {
            mem[name] = (end - start) + mem[name];
        }
        // if (name.indexOf("ldHarvester")>-1) {
        //     console.log(mem[name])
        // }
    }

}
