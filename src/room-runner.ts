import {SpawnerController} from "./spawning/spawner-controller";
import {RoomControllerCommon} from "./room/room-controller-common";
import {DefenceTower} from "./roles/military/defence-tower";
import {SpawnerStatus} from "./spawning/spawner-status";
import {MaintenanceLink} from "./maintenance.links";
import {Profiler} from "./profiler";
import {RoleFactory} from "./roles/common/role-factory";
import {Laboratory} from "./room/laboratory";
import {GrafanaCollector} from "./statistics/grafana-collector";

export class RoomRunner {
    private towerRunner: DefenceTower;

    constructor() {
        this.towerRunner = new DefenceTower();
    }

    public manage(room: Room, roomController: RoomControllerCommon, statusCollection: SpawnerStatus[], spawnerController: SpawnerController, linkRunner: MaintenanceLink, roleFactory: RoleFactory) {
        GrafanaCollector.addDetailedCpu("RoomRunner Spawner " + room.name, () => {
            if (Game.time % 4 === 0) {
                //let"s save some cpu
                this.runOneSpawnInRoom(spawnerController, room, roomController, statusCollection, roleFactory);
            }
        });

        GrafanaCollector.addDetailedCpu("RoomRunner Links " + room.name, () => {
            linkRunner.transfer(room);
        });

        GrafanaCollector.addDetailedCpu("RoomRunner Tower " + room.name, () => {
            this.runTowers(room);
        });

        GrafanaCollector.addDetailedCpu("RoomRunner Labs " + room.name, () => {
            new Laboratory().runLabs(room);
        });

    }

    private runOneSpawnInRoom(spawnerController: SpawnerController, room: Room, roomController: RoomControllerCommon, statusCollection: SpawnerStatus[], roleFactory: RoleFactory) {
        let spawnsForRoom = <StructureSpawn[]>room.find(FIND_MY_SPAWNS);

        if (!spawnsForRoom || spawnsForRoom.length === 0) {
            return;
        }

        let spawner;
        if (spawnsForRoom.length === 1) {
            spawner = spawnsForRoom[0];
        } else if (spawnsForRoom.length === 2 && !spawnsForRoom[0].spawning) {
            spawner = spawnsForRoom[0];
        } else if (spawnsForRoom.length === 2) {
            spawner = spawnsForRoom[1];
        } else if (spawnsForRoom.length === 3 && !spawnsForRoom[0].spawning) {
            spawner = spawnsForRoom[0];
        } else if (spawnsForRoom.length === 3 && !spawnsForRoom[1].spawning) {
            spawner = spawnsForRoom[1];
        } else if (spawnsForRoom.length === 3) {
            spawner = spawnsForRoom[2];
        } else {
            console.log("Missing if-case");
            spawner = spawnsForRoom[0];
        }

        spawnerController.spawn(room, spawner, roomController, statusCollection, roleFactory);
    }

    private runTowers(room: Room) {
        if (this.towerRunner.defendRoom(room)) {
        } else {
            _.forEach(room.find(FIND_MY_STRUCTURES), (structure: Structure) => {
                if (structure.structureType === STRUCTURE_TOWER) {
                    // console.log(Game.structures[name].pos);
                    if (this.towerRunner.healCreeps(<StructureTower> structure)) {
                    } else {
                        this.towerRunner.repairBuildings(<StructureTower> structure);
                    }
                }
            });
        }
    }
}
Profiler.registerClass(RoomRunner, RoomRunner.name);
