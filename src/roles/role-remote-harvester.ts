import {ScreepUtils} from "../helper/screep-utils";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {BaseRole, SpawnParameter} from "./common/role-base";
import {Profiler} from "../profiler";
import {RoomMemory} from "../room/room-memory";
import {MyCreep} from "../types/my-creep";

export class RoleLdHarvester extends BaseRole {

    public icon(): string {
        return "⛐⚒";
    }

    public roleName(): string {
        return "ldHarvester";
    }

    public roleShortName(): string {
        return "RH";
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 7);

        if (creep.memory.working === true && creep.carry.energy === 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working === false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.ticksToLive < 50) {
            if (Game.time % 4 === 0) {
                creep.say("dying", false);
            }
            creep.memory.working = true;
        }

        if (creep.fatigue > 0) {
            return;
        }

        if (creep.hits < creep.hitsMax) {
            this.helperNavigation.moveToRoom(creep, creep.memory.baseRoomName);
            return;
        }

        if (creep.memory.working === true) {
            // unloading
            this.unloading(creep);
        } else {
            this.harvesting(creep);
        }
    }

    private harvesting(creep: MyCreep) {
        if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
            let source: Source|null;

            if (creep.memory.desiredTarget) {
                source = <Source|null>Game.getObjectById(creep.memory.desiredTarget);
            } else {
                source = creep.pos.findClosestByRange(FIND_SOURCES_ACTIVE, {
                    filter: (x: any) => x.energy > 0
                    && x.room === creep.room
                });
                if (source) {
                    creep.memory.desiredTarget = source.id;
                }
            }
            if (!source) {
                delete creep.memory.desiredTarget;
                return;
            }

            let rc = creep.harvest(source);
            // console.log("rc=" + rc + " " + creep.name);
            if (rc === OK) {
            } else if (rc === ERR_NOT_IN_RANGE) {
                if (creep.moveTo(source) === ERR_NO_PATH) {
                    // console.log(creep.name + " " + creep.moveTo(source) + " " + ERR_NOT_IN_RANGE);
                    let newOpts: FindPathOpts & MoveToOpts = {};
                    newOpts.reusePath = 50;
                    creep.moveToXY(25, 25, newOpts);
                }
                //console.log("    "+creep.room.name + " " + creep.name + " " + source);
            } else if (rc === ERR_NOT_ENOUGH_RESOURCES) {
                delete creep.memory.desiredTarget;
            } else {
                console.log("unexpected rc harvesting=" + rc);
                delete creep.memory.desiredTarget;
            }
        }
    }

    private unloading(creep: MyCreep) {
        if (this.helperNavigation.moveToRoom(creep, creep.memory.baseRoomName)) {
            if (this.helperNavigation.transferToUnloadingLink(creep)) {
            } else if (this.helperNavigation.unloadingToSpawnOrExtension(creep)) {
            } else if (this.helperNavigation.unloadingEnergyToTerminalWithLimit(creep, 100000)) {
            } else if (this.helperNavigation.unloadingMineralsToStorage(creep)) {
            } else {
            }
        }
    }

    public callReplacementRole(): void {
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        let hasSpawned = false;

        // //FIXME currently inactive for testserver
        // if ((<any>Memory).fast) {
        //     return;
        // }
        //
        if (1<2) {
            return;
        }

        for (let sourceRoomName in (<RoomMemory>spawnParameter.spawner.room.memory).miningOperations) {
            if (sourceRoomName !== spawnParameter.spawner.room.name) {
                continue;
            }
            for (let targetRoomName in (<RoomMemory>spawnParameter.spawner.room.memory).miningOperations[sourceRoomName]) {
                let roomName = spawnParameter.spawner.room.name;

                let currLongDistanceHarvester = ScreepUtils.countArray(spawnParameter.filteredCreeps, (c) => (
                    c.memory.role === this.roleName()
                    && c.memory.baseRoomName === roomName
                    && c.ticksToLive >= 50
                    && c.memory.targetRoomName === targetRoomName
                ));

                let mem = (<RoomMemory>(Game.rooms[roomName].memory)).miningOperations[roomName][targetRoomName];
                let cntHarvesters = mem.cntSources * 2;

                if (isNaN(cntHarvesters)) {
                    console.log("Room " + targetRoomName + " is not scouted yet. Skipping.");
                    continue;
                }

                // console.log("targetRoomName=" + targetRoomName);
                // console.log("cntHarvesters=" + cntHarvesters);
                spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currLongDistanceHarvester, cntHarvesters, targetRoomName);
                if (spawnParameter.spawnerStatus.hasSpawned) {
                    continue;
                }

                if (currLongDistanceHarvester < cntHarvesters) {
                    //spawn new one
                    hasSpawned = this.spawn(targetRoomName, spawnParameter.spawner, roomName, spawnParameter.maxEnergyAllowed);
                    spawnParameter.spawnerStatus.hasSpawned = true;
                }
            }
        }
    }

    private spawn(targetRoomName: string, spawner: StructureSpawn, baseRoomName: string, maxEnergyCapacityAvailable: number, roomUnloadingLink?: string): boolean {
        let nrComponents = Math.floor(maxEnergyCapacityAvailable / 550);
        let components = [];

        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
            components.push(WORK);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
            components.push(CARRY);
            components.push(CARRY);
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
            components.push(MOVE);
            components.push(MOVE);
        }
        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            baseRoomName: baseRoomName,
            role: this.roleName(),
            roomUnloadingLink: roomUnloadingLink,
            targetRoomName: targetRoomName,
            working: false,
        });
        return _.isString(rc);
    }
}
Profiler.registerClass(RoleLdHarvester, RoleLdHarvester.name);
