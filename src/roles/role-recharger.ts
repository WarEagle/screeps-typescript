import {BaseRole} from "./common/role-base";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";

export class RoleRecharger extends BaseRole {

    public icon(): string {
        return "🔋";
    }

    public roleName(): string {
        return "recharger";
    }

    public roleShortName(): string {
        return "RC";
    }

    public callReplacementRole(creep: MyCreep): void {
        //perform upgrade
        this.roleFactory.getRoleUpgrader().runAsSubRole(creep);
    }

    public run(creep: MyCreep) {

        if (creep.memory.working === true && creep.carry.energy === 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working === false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.memory.working === true) {
            if (this.helperNavigation.unloadingEnergyToTower(creep, 0.9)) {
            } else if (this.helperNavigation.unloadingEnergyToLab(creep)) {
            } else if (this.helperNavigation.unloadingToSpawnOrExtension(creep)) {
            } else if (this.helperNavigation.unloadingEnergyToTerminalWithLimit(creep, 150000)) {
            }
        } else {
            //take from storage
            if (this.helperNavigation.takeEnergyFromMyStorage(creep)) {
            } else if (this.helperNavigation.takeEnergyFromMyTerminal(creep)) {
            } else {
                creep.memory.working = true;
            }
        }
    }

    internalSpawnIfNeeded(): void {
        throw new SyntaxError("This method isn't implemented");
    }

}
Profiler.registerClass(RoleRecharger, RoleRecharger.name);
