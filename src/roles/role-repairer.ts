import {BaseRole, SpawnParameter} from "./common/role-base";
import {Profiler} from "../profiler";
import {ScreepCache} from "../helper/screep-cache";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {MyCreep} from "../types/my-creep";
import {ScreepUtils} from "../helper/screep-utils";

export class RoleRepairer extends BaseRole {

    public icon(): string {
        return "🔧";
    }

    public roleName(): string {
        return "repairer";
    }

    public roleShortName(): string {
        return "RP";
    }

    public callReplacementRole(creep: MyCreep): void {
        //perform upgrade
        ScreepCache.setRoomDone(creep.pos.roomName, "repairer");
        this.roleFactory.getRoleUpgrader().runAsSubRole(creep);
    }

    run(creep: MyCreep) {
        if (creep.memory.working === true && creep.carry.energy === 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working === false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        // if (!this.helperNavigation.moveToRoom(creep, creep.memory.spawnedIn)) {
        //     return;
        // }

        if (this.isSubRole && ScreepCache.isRoomDone(creep.pos.roomName, "repairer")) {
            // console.log("skipping role builder");
            this.callReplacementRole(creep);
        }

        if (creep.memory.working === true) {
            if (this.repairRemembered(creep)) {
            } else if (this.repairBuildings(creep)) {
            } else if (this.repairRoads(creep)) {
            } else if (this.repairDefence(creep)) {
            } else {
                this.callReplacementRole(creep);
            }
        } else {
            if (this.helperNavigation.takeEnergyFromMyStorage(creep)) {
            } else if (this.helperNavigation.takeEnergyFromMyContainer(creep)) {
            }
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        let currRepairer = ScreepUtils.countArray(spawnParameter.filteredCreeps, (c: Creep) => c.room.name === spawnParameter.spawner.room.name);
        let targetRepairer = spawnParameter.roomController.getTargetCntRepairer();

        if (currRepairer >= targetRepairer || spawnParameter.spawnerStatus.hasSpawned) {
            return;
        }

        if (targetRepairer === 0) {
            return;
        }

        spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currRepairer, targetRepairer);

        let components = [];

        components.push(WORK);
        components.push(WORK);
        components.push(WORK);

        components.push(CARRY);
        components.push(CARRY);
        components.push(CARRY);
        components.push(CARRY);
        components.push(CARRY);
        components.push(CARRY);
        components.push(CARRY);

        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);

        SpawnerWrapper.createCreep(spawnParameter.spawner, components, this, {
            role: this.roleName(),
            working: false,
        });
    }

    private repairRemembered(creep: MyCreep): boolean {
        let structure = Game.getObjectById(creep.memory.remembered);
        if (!structure) {
            delete creep.memory.remembered;
            return false;
        } else {
            let myStructure = <Structure> structure;
            let rc = creep.repair(myStructure);
            if (rc === ERR_NOT_IN_RANGE) {
                // if (creep.name === "RP W77N27 #0") {
                //     console.log(creep.name);
                // }
                creep.moveTo(myStructure);
            }
            if (myStructure.hits >= myStructure.hitsMax) {
                delete creep.memory.remembered;
            }
            return true;
        }
    }

    private repairBuildings(creep: MyCreep): boolean {
        let targetPercentage = this.isSubRole ? 0.40 : 0.80;
        let structure: any = creep.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (s: any) => (
                s.hits < s.hitsMax * targetPercentage
                && s.pos.roomName === creep.pos.roomName
                && s.structureType !== STRUCTURE_WALL
                && s.structureType !== STRUCTURE_RAMPART
                && s.structureType !== STRUCTURE_ROAD
            ),
        });
        if (structure) {
            creep.memory.remembered = structure.id;
            //console.log(creep.name + " target room " + structure.pos.roomName  + " creep room "+ creep.pos.roomName);
            if (creep.repair(structure) === ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        }
        return false;
    }

    private repairRoads(creep: MyCreep): boolean {
        let targetPercentage = this.isSubRole ? 0.5 : 0.80;
        let map = Game.map;
        let structure: any = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (s: Structure) => (
                s.pos.roomName === creep.pos.roomName
                && s.structureType === STRUCTURE_ROAD
                && s.hits < s.hitsMax * targetPercentage
                && map.getTerrainAt(s.pos.x, s.pos.y, s.room.name) !== "plain"
            )
        });
        if (structure) {
            // console.log("getTerrainAt="+map.getTerrainAt(structure.pos.x, structure.pos.y, structure.room.name));
            creep.memory.remembered = structure.id;
            //console.log(creep.name + " target room " + structure.pos.roomName  + " creep room "+ creep.pos.roomName);
            if (creep.repair(structure) === ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        }
        return false;
    }

    private repairDefence(creep: MyCreep): boolean {
        let targetPercentage = this.isSubRole ? 0.0001 : 0.0003;
        let structure: any = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (s: any) => (
                    (s.structureType === STRUCTURE_WALL
                    || s.structureType === STRUCTURE_RAMPART)
                    && s.hits < s.hitsMax * targetPercentage
                    && s.pos.roomName === creep.pos.roomName
                )
            }
        );
        if (structure) {
            // creep.memory.remembered = structure.id;
            //console.log(creep.name + " target room " + structure.pos.roomName  + " creep room "+ creep.pos.roomName);
            if (creep.repair(structure) === ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        }
        return false;
    }

}

Profiler.registerClass(RoleRepairer, RoleRepairer.name);
