import {BaseRole, SpawnParameter} from "./common/role-base";
import {Profiler} from "../profiler";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {MyCreep} from "../types/my-creep";
import {ScreepUtils} from "../helper/screep-utils";

export class RoleUpgrader extends BaseRole {

    public icon(): string {
        return "TODO";
    }

    public roleName(): string {
        return "upgrader";
    }

    public roleShortName(): string {
        return "UP";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {

        if (creep.memory.working === true && creep.carry.energy === 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working === false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (this.isSubRole || this.helperNavigation.moveToRoom(creep, creep.memory.spawnedIn)) {
            if (creep.memory.working === true) {
                this.upgradeController(creep);
            } else {
                this.collectEnergy(creep);
            }
        }
    }

    private collectEnergy(creep: MyCreep) {
        if (this.helperNavigation.canTakeEnergyFromReceiverLink(creep)) {
        } else if (this.helperNavigation.takeEnergyFromMyStorage(creep)) {
        } else if (this.helperNavigation.takeEnergyFromMyTerminal(creep)) {
        } else {
            this.harvestFromClosestResource(creep);
        }
    }

    private upgradeController(creep: MyCreep) {

        if (creep.room.controller && creep.room.controller.my) {
            // if (creep.upgradeController(creep.room.controller) === ERR_NOT_IN_RANGE) {
            //     creep.moveTo(creep.room.controller);
            // }

            // console.log(creep.name + " " + creep.room.name);
            if (this.helperNavigation.moveCloseTo(creep, creep.room.controller.pos)) {
                creep.upgradeController(creep.room.controller);
            }
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        let targetBuilder = 1;
        let currBuilder = ScreepUtils.countArray(spawnParameter.filteredCreeps, (c: Creep) => c.room.name === spawnParameter.spawner.room.name);

        spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currBuilder, targetBuilder);

        if (spawnParameter.spawnerStatus.hasSpawned) {
            return;
        }

        if (currBuilder < targetBuilder) {
            //spawn new one
            this.spawn(spawnParameter.spawner);
            spawnParameter.spawnerStatus.hasSpawned = true;
        }
    }

    private spawn(spawner: StructureSpawn): boolean {
        let components = [];

        components.push(WORK);
        components.push(CARRY);
        components.push(MOVE);

        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            working: false,
        });
        return _.isString(rc);
    }
}

Profiler.registerClass(RoleUpgrader, RoleUpgrader.name);
