import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {BaseRole, SpawnParameter} from "./common/role-base";
import {Profiler} from "../profiler";
import {RoomMemory} from "../room/room-memory";
import {MyCreep} from "../types/my-creep";

// JSON.stringify(Game.rooms.W77N27.memory.labOperations)
// {"5868e398a307453b646af756":{"producer":false,"amount":1000,"resource":"XGH2O","lab1":"","lab2":""}}
//
// Game.rooms.W76N29.memory.labOperations= {"586c912e57ff7a494e2874a1":{"producer":false,"amount":1000,"resource":"O","lab1":"","lab2":""}, "586c98133b2e8ffa3399e420":{"producer":false,"amount":1000,"resource":"L","lab1":"","lab2":""}, "586c3da819aabae14d96d84f":{"producer":true,"amount":1000,"resource":"LO","lab1":"586c912e57ff7a494e2874a1","lab2":"586c98133b2e8ffa3399e420"}}
//
// Game.rooms.W77N27.terminal.send(RESOURCE_OXYGEN, 20000, 'W76N29')
// Game.rooms.W76N29.terminal.send(RESOURCE_LEMERGIUM, 20000, 'W77N27')
// Game.rooms.W76N29.terminal.send(RESOURCE_LEMERGIUM_OXIDE, 20000, 'W77N27')
// Game.rooms.W78N29.terminal.send(RESOURCE_, 20000, 'W77N27')
// Game.rooms.W78N29.terminal.send(RESOURCE_UTRIUM, 20000, 'W77N27')
export class RoleLabRat extends BaseRole {

    public icon(): string {
        return "♲";
    }

    public roleName(): string {
        return "labrat";
    }

    public roleShortName(): string {
        return "LR";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        // console.log("DEBUG "+Game.time + " for creep "+creep.name);
        this.sayRoleIcon(creep, 3);

        // console.log(creep.name + " 11");

        if (creep.memory.working === true && _.sum(creep.carry) === 0) {
            creep.memory.working = false;

            // console.log("switching");
            // delete previous assignment
            delete creep.memory.unloadingTarget;
            delete creep.memory.resourceAmount;
            delete creep.memory.resourceType;
        }

        if (creep.memory.working === false && _.sum(creep.carry) >= creep.carryCapacity * 0.75) {
            creep.memory.working = true;
        }

        if (creep.ticksToLive < 30) {
            if (Game.time % 4 === 0) {
                creep.say("dying", false);
            }
            creep.memory.working = true;
            //creep.suicide();
        }

        if (creep.fatigue > 0) {
            return;
        }

        if (!creep.memory.unloadingTarget) {
            creep.memory.working = false;
        }

        if (!creep.memory.resourceAmount) {
            creep.memory.working = false;
        }

        if (!creep.memory.resourceType) {
            creep.memory.working = false;
        }

        // console.log("creep.memory.working=" + creep.memory.working);
        if (creep.memory.working) {
            // unloading
            this.unloading(creep);
        } else {
            // loading
            this.load(creep);
        }
        this.setAssignment(creep);
    }

    private load(creep: MyCreep) {
        // console.log("load creep.memory.mode=" + creep.memory.mode);
        if (creep.memory.mode !== "cleaning" && this.tryToLoadFrom(creep, creep.room.storage)) {
        } else if (creep.memory.mode !== "cleaning" && this.tryToLoadFrom(creep, creep.room.terminal)) {
        } else {
            // console.log("cleaning");
            if (creep.memory.mode === "cleaning") {
                let structure = <StructureLab> Game.getObjectById(creep.memory.unloadingTarget);
                this.tryToLoadFromLab(creep, structure);
            }
        }
    }

    private hasResources(amount: number, resource: string, storage: StructureStorage|StructureTerminal|undefined): boolean {
        if (!storage) {
            return false;
        }
        if (!storage.store[resource]) {
            return false;
        } else if (storage.store[resource] && storage.store[resource] < amount) {
            return false;
        }
        return true;
    }

    private tryToLoadFrom(creep: MyCreep, storage: StructureStorage|StructureTerminal|undefined): boolean {
        if (!storage) {
            return false;
        }
        if (!storage.store[creep.memory.resourceType]) {
            return false;
        }

        let rc = creep.withdraw(storage, <any> creep.memory.resourceType);
        // console.log("rc="+rc);
        // console.log("creep.memory.resourceType="+creep.memory.resourceType);
        if (rc === ERR_NOT_IN_RANGE) {
            creep.moveTo(storage);
            return true;
        }
        if (rc === OK) {
            creep.memory.working = true;
        }
        return false;
    }

    private tryToLoadFromLab(creep: MyCreep, storage: StructureLab): boolean {
        if (!storage) {
            return false;
        }
        let rc = creep.withdraw(storage, <any> creep.memory.resourceType);
        if (rc === ERR_NOT_IN_RANGE) {
            creep.moveTo(storage);
            return true;
        }
        if (rc === OK) {
            creep.memory.working = true;
        }
        return false;
    }

    private unloading(creep: MyCreep) {
        if (creep.memory.mode === "cleaning") {
            if (this.helperNavigation.unloadingMineralsToTerminal(creep)) {
            }else if (this.helperNavigation.unloadingMineralsToStorage(creep)) {
            }
        } else {
            let structure = <StructureLab> Game.getObjectById(creep.memory.unloadingTarget);
            // console.log("creep.carry[0] = " + creep.carry[0]);
            // console.log("creep.carry[1] = " + creep.carry[1]);
            // console.log("creep.carry[creep.memory.resourceType] = " + creep.carry[creep.memory.resourceType]);

            if (!structure) {
                // console.log("Target for " + creep.name + " was wrong!");
                delete creep.memory.unloadingTarget;
                delete creep.memory.mode;
                return;
            }

            let rc = creep.transfer(structure, creep.memory.resourceType);
            // console.log("rc2=" + rc);
            if (rc === ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
        }
    }

    private setAssignment(creep: MyCreep) {
        // no assignment?
        if (creep.memory.unloadingTarget) {
            // console.log(creep.name + " 1");
            return;
        }
        // console.log(creep.name + " 2");
        let labOperations = (<RoomMemory> creep.room.memory).labOperations;
        for (let labId in labOperations) {
            // console.log(creep.name + " 3");
            let labMemEntry = labOperations[labId];
            let structure = <StructureLab>Game.getObjectById(labId);
            // console.log(creep.name + " " + structure.mineralType)
            if (structure.mineralType === labMemEntry.resource || (!structure.mineralType || structure.mineralType == null)) {
                if (labMemEntry.producer) {
                    continue;
                }
                // console.log(creep.name + " 1")
                let diff = labMemEntry.amount - structure.mineralAmount;

                // console.log("this.hasResources(creep, diff, creep.room.terminal)=" + this.hasResources(diff, labMemEntry.resource, creep.room.terminal));
                // console.log("this.hasResources(creep, diff, creep.room.storage)=" + this.hasResources(diff, labMemEntry.resource, creep.room.storage));

                if (diff >= 100
                    && (this.hasResources(diff, labMemEntry.resource, creep.room.terminal) || this.hasResources(diff, labMemEntry.resource, creep.room.storage))
                ) {
                    creep.memory.mode = "filling";
                    creep.memory.unloadingTarget = labId;
                    creep.memory.resourceAmount = diff;
                    creep.memory.resourceType = labMemEntry.resource;
                    break;
                }
            } else {
                //wrong resource in lab, remove it!
                //TODO add unloading of producer here, too

                creep.memory.mode = "cleaning";
                creep.memory.unloadingTarget = labId;
                creep.memory.resourceAmount = structure.mineralAmount;
                creep.memory.resourceType = structure.mineralType;
                break;
            }
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        // let tmp: number = 0;

        // _.forEach(spawner.room.find(FIND_MY_STRUCTURES), function (entry: any) {
        //     // console.log(JSON.stringify(entry));
        //     if (entry.structureType === StructureLab
        //     && entry.room.name === spawner.room.name) {
        //         tmp++;
        //     }
        // });
        //
        let currTarget = 0;

        if (spawnParameter.spawner.room.memory.labOperations && _.size(spawnParameter.spawner.room.memory.labOperations) > 0) {
            currTarget = 1;
        } else {
            currTarget = 0;
        }

        if (currTarget === 0) {
            // don't report special creep
            return;
        }

        let currCountTotal = _.filter(spawnParameter.filteredCreeps, (c) => (
            c.room.name === spawnParameter.spawner.room.name
        )).length;

        spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currCountTotal, currTarget);

        if (currTarget <= currCountTotal || spawnParameter.spawnerStatus.hasSpawned) {
            return;
        }

        let components = [];
        components.push(CARRY);
        components.push(CARRY);
        components.push(CARRY);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);

        SpawnerWrapper.createCreep(spawnParameter.spawner, components, this, {
            role: this.roleName(),
            working: false,
        });
        spawnParameter.spawnerStatus.hasSpawned = true;
    };

}

Profiler.registerClass(RoleLabRat, RoleLabRat.name);
