import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {BaseRole} from "./common/role-base";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";

export class RoleRoomTransfer extends BaseRole {

    public icon(): string {
        return "TODO";
    }

    public roleName(): string {
        return "roomTransfer";
    }

    public roleShortName(): string {
        return "RT";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        // if (1 < 2) {
        //     return;
        // }

        if (creep.memory.working === true) {
            let hasLoad = false;
            for (let mineral in RESOURCES_ALL) {
                if (creep.carry[RESOURCES_ALL[mineral]] > 0) {
                    hasLoad = true;
                    break;
                }
            }
            creep.memory.working = hasLoad;
        }

        if (creep.memory.working === false) {
            let carryTotal = 0;
            for (let mineral in RESOURCES_ALL) {
                if (creep.carry[RESOURCES_ALL[mineral]]) {
                    carryTotal += creep.carry[RESOURCES_ALL[mineral]];
                }
            }
            // console.log(JSON.stringify(creep.carry));
            // console.log(carryTotal);
            if (carryTotal >= creep.carryCapacity * 0.9) {
                creep.memory.working = true;
            }
        }

        if (creep.ticksToLive < 200) {
            creep.memory.working = true;
        }

        if (creep.memory.working === true) {
            // unload

            // correct room?
            if (this.helperNavigation.moveToRoom(creep, creep.memory.baseRoomName)) {
                if (this.helperNavigation.unloadingEnergyToStorage(creep)) {
                    //
                } else if (this.helperNavigation.unloadingMineralsToStorage(creep)) {
                    //
                }
            }

        } else {
            // load

            // correct room?
            if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
                if (this.helperNavigation.takeEnergyFromMyStorage(creep)) {
                    //
                } else if (this.helperNavigation.takeMineralsFromStorage(creep)) {
                    //
                } else if (this.helperNavigation.takeEnergyFromEnemyBuilding(creep)) {
                    //
                }
            }
        }
    }

    internalSpawnIfNeeded(): void {
        //TODO uses constants!
        // let hasSpawned = false;
        //
        // if (!constants || !constants.lorryRoomtransfer) {
        //     return;
        // }
        //
        // for (let remoteRoom of constants.lorryRoomtransfer) {
        //     let current = ScreepUtils.count(Game.creeps, (c) => (
        //         c.memory.role === this.roleName()
        //         // && c.memory.baseRoomName === roomName
        //         && c.memory.targetRoomName === remoteRoom.targetRoomName
        //     ));
        //
        //     spawnerStatus.addToReport(this.roleShortName(), current, remoteRoom.cnt, remoteRoom.targetRoomName);
        //     if (spawnerStatus.hasSpawned) {
        //         continue;
        //     }
        //
        //     if (current < remoteRoom.cnt) {
        //         //spawn new one
        //         hasSpawned = this.spawnLorryRoomtransfer(remoteRoom.targetRoomName, spawner, roomName, maxEnergyCapacityAvailable);
        //         spawnerStatus.hasSpawned = true;
        //     }
        // }
    }

    protected spawnLorryRoomtransfer(targetRoomName: string, spawner: StructureSpawn, baseRoomName: string, maxEnergyCapacityAvailable: number): boolean {
        let nrComponents = Math.floor(maxEnergyCapacityAvailable / 150);
        let components = [];

        for (let i = 0; i < nrComponents * 2; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            baseRoomName: baseRoomName,
            role: this.roleName(),
            targetRoomName: targetRoomName,
            working: false,
        });
        return _.isString(rc);
    }

}
Profiler.registerClass(RoleRoomTransfer, RoleRoomTransfer.name);
