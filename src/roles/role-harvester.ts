import {BaseRole} from "./common/role-base";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";

export class RoleHarvester extends BaseRole {

    public icon(): string {
        return "⚒";
    }

    public roleName(): string {
        return "harvester";
    }

    public roleShortName(): string {
        return "HV";
    }

    public callReplacementRole(creep: MyCreep): void {
        this.roleFactory.getRoleBuilder().runAsSubRole(creep);
    }

    public run(creep: MyCreep) {
        if (!this.isSubRole) {
            this.sayRoleIcon(creep, 2);
        }

        if (creep.memory.working === true && creep.carry.energy === 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working === false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.memory.working === true) {
            // let harvesterMeasurer = new CpuMeasurer("\t" + creep.memory.role + " unloading " + creep.name);
            // harvesterMeasurer.start();
            // distribute energy
            if (this.isSubRole || this.helperNavigation.moveToRoom(creep, creep.memory.spawnedIn)) {
                if (this.helperNavigation.unloadingToSpawnOrExtension(creep)) {
                } else if (this.helperNavigation.unloadingEnergyToStorage(creep)) {
                } else if (this.helperNavigation.unloadingEnergyToTower(creep, 0.5)) {
                } else {
                    this.callReplacementRole(creep);
                }
            }
            // harvesterMeasurer.stop();
        } else {
            if (this.harvestFromClosestResource(creep)) {
            } else if (this.helperNavigation.canTakeEnergyFromReceiverLink(creep)) {
            }
        }
    }

    internalSpawnIfNeeded(): void {
        throw new SyntaxError("This method isn't implemented");
    }

}
Profiler.registerClass(RoleHarvester, RoleHarvester.name);
