import {BaseRole, SpawnParameter} from "./common/role-base";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {SpawnerStatus} from "../spawning/spawner-status";
import {Profiler} from "../profiler";
import {ScreepCache} from "../helper/screep-cache";
import {RoomMemory} from "../room/room-memory";
import {MyCreep} from "../types/my-creep";

export class RoleRemoteBuilder extends BaseRole {

    public icon(): string {
        return "⛐⛫";
    }

    public roleName(): string {
        return "remoteBuilder";
    }

    public roleShortName(): string {
        return "RB";
    }

    public callReplacementRole(creep: MyCreep): void {
        this.roleFactory.getRoleBuilder().runAsSubRole(creep);
    }

    run(creep: MyCreep) {
        this.sayRoleIcon(creep, 6);

        if (ScreepCache.isRoomDone(creep.pos.roomName, "remoteBuilder")) {
            // console.log("skipping role remoteBuilder");
            this.callReplacementRole(creep);
        }

        if (creep.fatigue > 0) {
            return;
        }

        if (creep.hits < creep.hitsMax) {
            this.helperNavigation.moveToRoom(creep, creep.memory.baseRoomName);
            return;
        }

        // correct room?
        if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
            this.callReplacementRole(creep);
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        let filteredCreeps = this.getCreepsWithOwnRole();

        for (let sourceRoomName in (<RoomMemory>spawnParameter.spawner.room.memory).miningOperations) {
            if (sourceRoomName !== spawnParameter.spawner.room.name) {
                continue;
            }
            for (let targetRoomName in (<RoomMemory>spawnParameter.spawner.room.memory).miningOperations[sourceRoomName]) {

                let currentCreeps = _.filter(filteredCreeps, (c) => (
                    c.memory.targetRoomName === targetRoomName
                )).length;

                spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currentCreeps, 1, targetRoomName);
                if (spawnParameter.spawnerStatus.hasSpawned) {
                    continue;
                }

                if (currentCreeps < 1) {
                    if (RoleRemoteBuilder.hasRemoteRoomConstructionsOrController(targetRoomName)) {
                        //spawn new one
                        this.spawnRemoteBuilder(targetRoomName, spawnParameter.spawner, spawnParameter.spawner.room.name, spawnParameter.maxEnergyAllowed);
                    } else {
                        //only spawn a small one for repairs
                        this.spawnSmallRemoteBuilder(targetRoomName, spawnParameter.spawner, spawnParameter.spawner.room.name);
                    }
                    spawnParameter.spawnerStatus.hasSpawned = true;
                }
            }
        }

    }

    public spawnIfNeededOldWay(constants: any, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, maxEnergyCapacityAvailable: number) {
        let hasSpawned = false;
        let filteredCreeps = this.getCreepsWithOwnRole();

        if (!constants || !constants.remoteBuilder) {
            return;
        }

        for (let remoteRoom of constants.remoteBuilder) {
            let currentCreeps = _.filter(filteredCreeps, (c) => (
                c.memory.targetRoomName === remoteRoom.targetRoomName
            )).length;

            spawnerStatus.addToReport(this.roleShortName(), currentCreeps, remoteRoom.cnt, remoteRoom.targetRoomName);
            if (spawnerStatus.hasSpawned) {
                continue;
            }

            if (currentCreeps < remoteRoom.cnt) {
                if (RoleRemoteBuilder.hasRemoteRoomConstructionsOrController(remoteRoom.targetRoomName)) {
                    //spawn new one
                    hasSpawned = this.spawnRemoteBuilder(remoteRoom.targetRoomName, spawner, spawner.room.name, maxEnergyCapacityAvailable);
                } else {
                    //only spawn a small one for repairs
                    hasSpawned = this.spawnSmallRemoteBuilder(remoteRoom.targetRoomName, spawner, spawner.room.name);
                }
                spawnerStatus.hasSpawned = true;
            }
        }
        return hasSpawned;
    }

    public static hasRemoteRoomConstructionsOrController(targetRoomName: string): boolean {
        // console.log("hasRemoteRoomConstructionsOrController for room " + targetRoomName);
        if (!Game.rooms[targetRoomName]) {
            // console.log("false 1");
            return false;
        }
        let room = Game.rooms[targetRoomName];
        if (room && room.controller && room.controller.my) {
            // console.log("true 1");
            return true;
        }
        if (room && room.find(FIND_MY_CONSTRUCTION_SITES).length > 1) {
            // console.log("true 2");
            return true;
        }
        // console.log("false 2");
        return false;
    }

    private spawnRemoteBuilder(targetRoomName: string, spawner: StructureSpawn, baseRoomName: string, maxEnergyCapacityAvailable: number): boolean {
        // console.log("spawnRemoteBuilder in " + baseRoomName + " for " + targetRoomName);
        let nrComponents = Math.floor(maxEnergyCapacityAvailable / 250);
        let components = [];

        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
        }
        for (let i = 0; i < nrComponents * 2; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            baseRoomName: baseRoomName,
            role: this.roleName(),
            targetRoomName: targetRoomName,
            working: false,
        });
        return _.isString(rc);
    };

    private spawnSmallRemoteBuilder(targetRoomName: string, spawner: StructureSpawn, baseRoomName: string): boolean {
        let components = [];

        components.push(WORK);
        components.push(CARRY);
        components.push(CARRY);
        components.push(MOVE);
        components.push(MOVE);

        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            baseRoomName: baseRoomName,
            role: this.roleName(),
            targetRoomName: targetRoomName,
            working: false,
        });
        return _.isString(rc);
    };
}

global.tt = function (roomName: string) {
    console.log(RoleRemoteBuilder.hasRemoteRoomConstructionsOrController(roomName));
    return "";
};

Profiler.registerClass(RoleRemoteBuilder, RoleRemoteBuilder.name);
