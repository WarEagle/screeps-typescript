import {BaseRole} from "./common/role-base";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";

export class RoleSigner extends BaseRole {

    public icon(): string {
        return "✎";
    }

    public roleName(): string {
        return "signer";
    }

    public roleShortName(): string {
        return "SG";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        if (creep.memory.working === true) {
            //move towards controller
            if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
                if (creep.room.controller) {
                    let rc = creep.signController(creep.room.controller, creep.memory.message);
                    if (rc === ERR_NOT_IN_RANGE) {
                        creep.moveTo(creep.room.controller.pos);
                    } else if (rc === OK) {
                        creep.memory.working = false;
                        creep.suicide();
                    }
                }
            }
        } else {
            // ??? what to do
            // creep.memory.working = true;
        }
    }

    internalSpawnIfNeeded(): void {
        throw new SyntaxError("This method isn't implemented");
    }

}
Profiler.registerClass(RoleSigner, RoleSigner.name);
