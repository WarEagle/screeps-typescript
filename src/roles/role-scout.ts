import {BaseRole, SpawnParameter} from "./common/role-base";
import {WorldInfoCollector} from "../statistics/world-info";
import {ScreepUtils} from "../helper/screep-utils";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";

export class RoleScout extends BaseRole {

    public icon(): string {
        return "TODO";
    }

    public roleName(): string {
        return "scout";
    }

    public roleShortName(): string {
        return "SC";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        if (creep.memory.done === true) {
            creep.suicide();
            return;
        }

        if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
            new WorldInfoCollector().assignRooms();
            creep.memory.done = true;
            creep.suicide();
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {

        if (!spawnParameter.spawner.room.controller || spawnParameter.spawner.room.controller.level < 3) {
            // don't make such things in small rooms
            return;
        }

        let hasSpawned = false;

        for (let i in Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_UNKNOWN]) {
            //noinspection JSUnfilteredForInLoop
            let target = Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_UNKNOWN][i];

            if (!i || !target) {
                continue;
            }

            // console.log("scout test " + target.name + " "  +i);

            if (spawnParameter.spawnerStatus.hasSpawned) {
                continue;
            }

            let curr = ScreepUtils.countArray(spawnParameter.filteredCreeps, (c) => (
                c.memory.targetRoomName === target.name
            ));

            // console.log(target.name + " " + curr);
            if (curr < 1) {
                //spawn new one
                console.log("spawning scout " + target.name);
                hasSpawned = this.spawn(target.name, spawnParameter.spawner);
                spawnParameter.spawnerStatus.hasSpawned = true;
                break;
            }
        }
    }

    private spawn(targetRoomName: string, spawner: StructureSpawn): boolean {
        let components = [];
        components.push(MOVE);
        // the name prevents multiple scouts for the same target
        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            targetRoomName: targetRoomName,
            working: true,
        }, targetRoomName);
        return _.isString(rc);
    }
}

Profiler.registerClass(RoleScout, RoleScout.name);
