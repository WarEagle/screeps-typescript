import {ScreepUtils} from "../helper/screep-utils";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {HelperCalculation} from "../helper/helper-calculation";
import {SpawnerStatus} from "../spawning/spawner-status";
import {BaseRole, SpawnParameter} from "./common/role-base";
import {WorldInfoCollector} from "../statistics/world-info";
import {Profiler} from "../profiler";
import {RoomMemory} from "../room/room-memory";
import {MyCreep} from "../types/my-creep";

export class RoleClaimer extends BaseRole {

    public icon(): string {
        return "TODO";
    }

    public roleName(): string {
        return "claimer";
    }

    public roleShortName(): string {
        return "CL";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        // console.log(creep.name)

        // correct room?
        if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
            if (!creep.room.controller) {
                console.log("Error in RoleClaimer!");
                return;
            }
            // console.log(creep.name + " " + (!creep.room.controller.my ) + " " + creep.room.controller.owner.username)

            if (!creep.room.controller.my && creep.room.controller.owner && creep.room.controller.owner.username) {
                console.log("Error in RoleClaimer!");
                return;
            } else {
                // console.log(2)
                this.claimOrReserve(creep);
            }
        } else {
            // console.log(creep.name + " is at " + creep.pos.roomName + " and tries to get to " + creep.memory.targetRoomName);
        }
    }

    private claimOrReserve(creep: MyCreep) {
        if (!creep.room.controller) {
            return;
        }
        if (creep.memory.claim === true) {
            let result = creep.claimController(creep.room.controller);
            //console.log(result);
            if (result === ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller.pos);
            } else if (result === ERR_GCL_NOT_ENOUGH) {
                //console.log("ERROR Your Global Control Level is not enough. " + creep.memory.targetRoomName);
                let rc2 = creep.reserveController(creep.room.controller);
                if (rc2 === ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.controller.pos);
                }
            }
        } else {
            //just reserve
            let rc2 = creep.reserveController(creep.room.controller);
            // console.log(rc2)
            if (rc2 === ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller.pos);
            }
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        if ((<any>spawnParameter.spawner.room.controller).level <= 2) {
            // we don't need any claimer for such low rooms, because their creeps will be too small to harvest the 3000 energy
            return;
        }

        for (let sourceRoomName in (<RoomMemory>spawnParameter.spawner.room.memory).miningOperations) {
            if (sourceRoomName !== spawnParameter.spawner.room.name) {
                continue;
            }

            for (let targetRoomName in (<RoomMemory>spawnParameter.spawner.room.memory).miningOperations[sourceRoomName]) {
                let currentCreeps = _.filter(spawnParameter.filteredCreeps, (c) => (
                    c.memory.targetRoomName === targetRoomName
                )).length;

                if (Game.rooms[targetRoomName]) {
                    let controller = Game.rooms[targetRoomName].controller;
                    if (controller && controller.reservation && controller.reservation.ticksToEnd > 3000) {
                        // don't spawn if there is lots of reservation left
                        return;
                    }
                    if (!controller) {
                        // no controller, so do nothing
                        return;
                    }
                }

                let targetNumber = 1;
                if (spawnParameter.spawner.room.energyCapacityAvailable < 800) {
                    // room is too small
                    return;
                }

                if (spawnParameter.spawner.room.energyCapacityAvailable < 1300) {
                    targetNumber++;
                }

                spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currentCreeps, targetNumber, targetRoomName);

                if (spawnParameter.spawnerStatus.hasSpawned) {
                    continue;
                }

                let room = Game.rooms[spawnParameter.spawner.room.name];
                if (room
                    && Memory[WorldInfoCollector.TAG_ROOMINFO]
                    && Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED]
                    && Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED][spawnParameter.spawner.room.name]
                ) {
                    let workableFields = Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED][spawnParameter.spawner.room.name].cntWorkableFieldsController;
                    targetNumber = Math.min(workableFields, targetNumber);
                }

                if (currentCreeps < targetNumber) {
                    //spawn new one
                    this.spawnClaimer(targetRoomName, spawnParameter.spawner.room.name, spawnParameter.spawner);
                    spawnParameter.spawnerStatus.hasSpawned = true;
                }
            }
        }
    }

    public spawnIfNeededOldWay(roomName: string, constants: any, spawner: StructureSpawn, spawnerStatus: SpawnerStatus): void {
        let hasSpawned = false;

        if (!constants || !constants.remoteClaimer) {
            return;
        }

        for (let remoteRoom of constants.remoteClaimer) {
            let currClaimer = ScreepUtils.count(Game.creeps, (c) => (
                c.memory.role === this.roleName()
                // && c.memory.baseRoomName === roomName
                && c.memory.targetRoomName === remoteRoom.targetRoomName
            ));

            let targetNumber = 1;
            if (spawner.room.energyCapacityAvailable < 1300) {
                targetNumber++;
            }

            spawnerStatus.addToReport(this.roleShortName(), currClaimer, targetNumber, remoteRoom.targetRoomName);

            if (spawnerStatus.hasSpawned) {
                continue;
            }

            let room = Game.rooms[roomName];
            if (room
                && Memory[WorldInfoCollector.TAG_ROOMINFO]
                && Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED]
                && Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED][roomName]
            ) {
                let workableFields = Memory[WorldInfoCollector.TAG_ROOMINFO][WorldInfoCollector.TAG_SCOUTED][roomName].cntWorkableFieldsController;
                targetNumber = Math.min(workableFields, targetNumber);
            }

            if (currClaimer < targetNumber) {
                //spawn new one
                hasSpawned = this.spawnClaimer(remoteRoom.targetRoomName, roomName, spawner);
                spawnerStatus.hasSpawned = true;
            }
        }
    }

    private spawnClaimer(targetRoomName: string, baseRoomName: string, spawner: StructureSpawn): boolean {
        let body1: string[] = [MOVE, MOVE, CLAIM, CLAIM];
        let body2: string[] = [MOVE, MOVE, CLAIM];
        let components: string[] = [];
        if (HelperCalculation.costsForBody(body1) <= spawner.room.energyCapacityAvailable) {
            components = body1;
        } else if (HelperCalculation.costsForBody(body2) <= spawner.room.energyCapacityAvailable) {
            components = body2;
        }

        if (components.length === 0) {
            return false;
        }

        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            baseRoomName: baseRoomName,
            role: this.roleName(),
            targetRoomName: targetRoomName,
            working: false,
        });
        return _.isString(rc);
    }

}
Profiler.registerClass(RoleClaimer, RoleClaimer.name);
