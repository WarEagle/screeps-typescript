import {BaseRole, SpawnParameter} from "./common/role-base";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {SpawnerStatus} from "../spawning/spawner-status";
import {Profiler} from "../profiler";
import {RoomMemory, MiningContainer} from "../room/room-memory";
import {HelperCalculation} from "../helper/helper-calculation";
import {MyCreep} from "../types/my-creep";

export class RoleRemoteFixedHarvester extends BaseRole {

    public icon(): string {
        return "⚿" + this.roleFactory.getRoleRemoteHarvester().icon();
    }

    public roleName(): string {
        return "remoteFixedHarvester";
    }

    public roleShortName(): string {
        return "RFH";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        // creep.suicide();
        this.sayRoleIcon(creep, 1);

        let targetRoomName = creep.memory.targetRoomName;

        if (this.helperNavigation.moveToRoom(creep, targetRoomName)) {
            let x = creep.memory.x;
            let y = creep.memory.y;
            //let targetPos = new RoomPosition(x, y, targetRoomName);

            if (creep.pos.x !== x || creep.pos.y !== y) {
                creep.moveToXY(x, y);
            } else {
                this.doWorkAtTarget(creep);
            }
        }
    }

    private doWorkAtTarget(creep: MyCreep) {

        //check the container statis
        if (this.maintainContainer(creep)) {
        } else {
            //collect energy
            let sourceId = creep.memory.sourceId;
            let source: any = Game.getObjectById(sourceId);
            creep.harvest(source);
        }
    }

    private maintainContainer(creep: MyCreep): boolean {
        let containerExists: boolean = false;
        let constructionExists: boolean = false;
        let idOfObject: string|undefined = undefined;

        // if ((creep.carry.energy <= 10) || !creep.memory.repair ==== true){
        //     return false;
        // }

        //TODO update room-memory at first repair
        _.forEach(creep.pos.look(), function (value: LookAtResult) {
            // console.log(key + " " + JSON.stringify(value));
            if (value.type && value.type === "structure" && value.structure && value.structure.structureType === STRUCTURE_CONTAINER) {
                containerExists = true;
                idOfObject = value.structure.id;
                // do we really need to do this every time?
                let sourceRoom = Game.rooms[creep.memory.sourceRoomName];
                let mem = (<RoomMemory> sourceRoom.memory).miningOperations[creep.memory.sourceRoomName][creep.room.name];
                if (!mem) {
                    return;
                }
                for (let containerCnt in mem.containers) {
                    let container = mem.containers[containerCnt];
                    if (container.x === creep.memory.x
                        && container.y === creep.memory.y
                        && !container.isBuilt
                    ) {
                        //TODO what when this container gets destroyed?
                        container.isBuilt = true;
                        container.containerId = value.structure.id;
                    }
                }
                // (<RoomMemory> creep.room.memory).miningOperations[creep.memory.sourceRoomName][creep.room.name].is = true;
            }
            if (value.type && value.type === "constructionSite" && value.constructionSite && value.constructionSite.structureType === STRUCTURE_CONTAINER) {
                constructionExists = true;
                idOfObject = value.constructionSite.id;
            }
        });

        // console.log(creep.name + " constructionExists=" + constructionExists);
        // console.log(creep.name + " containerExists=" + containerExists);

        if (!constructionExists && !containerExists) {
            creep.pos.createConstructionSite(STRUCTURE_CONTAINER);
            return false;
        } else if (!containerExists && constructionExists) {
            if (!idOfObject) {
                console.log("Error while maintainig controller for " + creep.name);
                return false;
            }
            if (creep.carry.energy > 30) {
                let construction = <ConstructionSite> Game.getObjectById(idOfObject);
                creep.build(construction);
            }
        } else if (containerExists) {
            if (!idOfObject) {
                console.log("Error while maintainig controller for " + creep.name);
                return false;
            }
            if (creep.carry.energy >= 40 || creep.memory.repair === true) {
                let container = <StructureContainer> Game.getObjectById(idOfObject);
                //TODO change this to a tick duration, e.g. check every 5th tick, the decay is constant, so we don't need to calc this every turn.
                //TODO update room-memory with container id so we can skip this step in future
                if (container.hits <= container.hitsMax * 0.75 && creep.carry.energy >= 40) {
                    creep.memory.repair = true;
                    creep.repair(container);
                    return true;

                } else if (container.hits >= container.hitsMax * 0.95 || creep.carry.energy <= 10) {
                    delete creep.memory.repair;
                    return false;
                } else if (creep.carry.energy <= 10) {
                    delete creep.memory.repair;
                    return false;
                }
            }
        }
        return false;

    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        // //FIXME currently inactive for normal server
        // if (1 < 2 && !(<any>Memory).fast) {
        //     return;
        // }

        let room = spawnParameter.spawner.room;
        let roomMemory = <RoomMemory> room.memory;

        let targetTotal = 1;

        let possibleRelevantCreeps = this.getCreepsWithOwnRole();

        // if ((<any> spawner.room.controller).level < 4) {
        //todo change this to the exact amount from room.energyAvailable
        //room too small, will not have the energy
        // return;
        // }

        // if (!possibleRelevantCreeps || possibleRelevantCreeps.length === 0) {
        //     console.log(1);
        //     return;
        // }
        //
        for (let src in roomMemory.miningOperations) {
            for (let trg in roomMemory.miningOperations[src]) {
                let operation = roomMemory.miningOperations[src][trg].containers;
                let isSourceKeeper = roomMemory.miningOperations[src][trg].isSourceKeeper;
                for (let container in operation) {
                    let currCountTotal = this.getExistingCreepsForMiningOperation(operation[container], possibleRelevantCreeps, src, trg);
                    spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currCountTotal, 1, trg);
                    this.spawn(src, trg, spawnParameter.spawner, spawnParameter.spawnerStatus, currCountTotal, targetTotal, operation[container], isSourceKeeper);
                }
            }
        }
    }

    private spawn(sourceRoomName: string, targetRoomName: string, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, currCountTotal: number, targetTotal: number, container: MiningContainer, isSourceKeeper: boolean) {
        if (spawnerStatus.hasSpawned) {
            return;
        }

        if (currCountTotal >= targetTotal) {
            return;
        }

        let componentsKeeper = [WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE];
        let componentsNormal = [WORK, WORK, WORK, WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE];
        let componentsSmall = [WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE];
        let componentsTiny = [WORK, WORK, CARRY, MOVE];// not really worth it, but it can build up the container
        let components: string[] = [];

        // console.log("");
        // console.log(spawner.room.energyCapacityAvailable );
        // console.log(HelperCalculation.costsForBody(componentsNormal));
        // console.log(HelperCalculation.costsForBody(componentsSmall));

        if (isSourceKeeper && spawner.room.energyCapacityAvailable >= HelperCalculation.costsForBody(componentsKeeper)) {
            components = componentsKeeper;
        } else if (spawner.room.energyCapacityAvailable >= HelperCalculation.costsForBody(componentsNormal)) {
            components = componentsNormal;
        } else if (spawner.room.energyCapacityAvailable >= HelperCalculation.costsForBody(componentsSmall)) {
            components = componentsSmall;
        } else if (spawner.room.energyCapacityAvailable >= HelperCalculation.costsForBody(componentsTiny)) {
            components = componentsTiny;
        }

        if (components.length === 0) {
            return;
        }

        SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            working: false,
            targetRoomName: targetRoomName,
            sourceRoomName: sourceRoomName,
            x: container.x,
            y: container.y,
            sourceId: container.sourceId,
        });

        spawnerStatus.hasSpawned = true;
    }

    private getExistingCreepsForMiningOperation(container: MiningContainer, possibleRelevantCreeps: any, sourceRoomName: string, targetRoomName: string): number {
        // console.log("container=" + JSON.stringify(container));
        // console.log("possibleRelevantCreeps=" + JSON.stringify(possibleRelevantCreeps));
        let cnt = _.filter(possibleRelevantCreeps, function (creep: MyCreep) {
                return (
                    creep.memory.sourceRoomName === sourceRoomName
                    && creep.memory.targetRoomName === targetRoomName
                    && creep.memory.x === container.x
                    && creep.memory.y === container.y
                );
            }
        ).length;

        return cnt;
    }

}

Profiler.registerClass(RoleRemoteFixedHarvester, RoleRemoteFixedHarvester.name);
