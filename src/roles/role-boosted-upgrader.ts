import {BaseRole, SpawnParameter} from "./common/role-base";
import {Profiler} from "../profiler";
import {SpawnerStatus} from "../spawning/spawner-status";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {ScreepUtils} from "../helper/screep-utils";
import {MyCreep} from "../types/my-creep";

export class RoleBoostedUpgrader extends BaseRole {

    public icon(): string {
        return this.roleFactory.getRoleUpgrader().icon() + "+";
    }

    public roleName(): string {
        return "boostedUpgrader";
    }

    public roleShortName(): string {
        return "UP+";
    }

    public callReplacementRole(creep: MyCreep): void {
        this.roleFactory.getRoleUpgrader().runAsSubRole(creep);
    }

    public run(creep: MyCreep) {

        if (creep.memory.isBoosted !== true) {
            return this.tryToBoost(creep);
        }

        this.callReplacementRole(creep);
    }

    private tryToBoost(creep: MyCreep) {
        let lab = <StructureLab> Game.getObjectById(creep.memory.lab);
        let rc = lab.boostCreep(creep.getOriginalCreep());
        if (rc === ERR_NOT_IN_RANGE) {
            creep.moveTo(lab);
        }
        if (rc === OK) {
            creep.memory.isBoosted = true;
        }
        return;
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        let currTarget = 0;

        if (spawnParameter.spawner.room.name === "INACTIVE") {//FIXME remove hardcoded stuff
            currTarget = 2;
        }

        if (currTarget === 0) {
            // don't report special creep
            return;
        }

        let currCountTotal = ScreepUtils.countArray(spawnParameter.filteredCreeps, (c) => (
            c.room.name === spawnParameter.spawner.room.name
        ));

        spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currCountTotal, currTarget);

        if (currTarget <= currCountTotal || spawnParameter.spawnerStatus.hasSpawned) {
            return;
        }
        this.spawn(spawnParameter.maxEnergyAllowed, spawnParameter.spawner, spawnParameter.spawnerStatus);
    };

    private spawn(maxEnergyAllowed: number, spawner: StructureSpawn, spawnerStatus: SpawnerStatus) {

        let nrComponents = Math.floor(maxEnergyAllowed / 350);
        let components = [];

        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
            components.push(WORK);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }

        SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            working: false,
            isBoosted: false,
            lab: "5868e398a307453b646af756",//FIXME remove hardcoded stuff
        });
        spawnerStatus.hasSpawned = true;
    }
}

Profiler.registerClass(RoleBoostedUpgrader, RoleBoostedUpgrader.name);
