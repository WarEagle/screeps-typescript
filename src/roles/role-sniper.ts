import {BaseRole} from "./common/role-base";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";

export class RoleSniper extends BaseRole {

    public icon(): string {
        return "TODO";
    }

    public roleName(): string {
        return "sniper";
    }

    public roleShortName(): string {
        return "SN";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
            if (creep.room.controller && !creep.room.controller.my) {
                let rc = creep.attackController(creep.room.controller);
                // console.log(rc);
                if (rc === ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.controller.pos);
                }
            } else {
                console.log("Problem with RoleSniper " + creep.name + " in " + creep.pos.roomName);
            }
        }
    }
    internalSpawnIfNeeded(): void {
        throw new SyntaxError("This method isn't implemented");
    }

}
Profiler.registerClass(RoleSniper, RoleSniper.name);
