//FIXME too weak, a power-thingi has 2M hitPoints

import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {BaseRole, SpawnParameter} from "./common/role-base";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";

// Game.spawns.Spawn4.createCreep([TOUGH, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, ATTACK, HEAL, HEAL, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY], "PH W77N30", {role:'powerHarvester', working:false, baseRoomName: 'W78N29', targetRoomName: 'W77N30'})

export class RolePowerHarvester extends BaseRole {

    public icon(): string {
        return "!⛐⚒";
    }

    public roleName(): string {
        return "powerHarvester";
    }

    public roleShortName(): string {
        return "PH";
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        if (creep.memory.working === true && creep.carry.energy === 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working === false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.ticksToLive < 150) {
            if (Game.time % 4 === 0) {
                creep.say("dying", false);
            }
            creep.memory.working = true;
        }

        if (creep.fatigue > 0) {
            return;
        }

        if (creep.memory.working === true) {
            // correct room?
            if (this.helperNavigation.moveToRoom(creep, creep.memory.baseRoomName)) {
                //TODO maybe use terminal?
                if (this.helperNavigation.unloadingMineralsToStorage(creep)) {
                }
            }
        } else {
            if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
                if (this.heal(creep, false)) {
                } else if (this.attack(creep)) {
                } else if (this.heal(creep, true)) {
                } else if (this.collect(creep)) {
                } else {
                    console.log("Creep " + creep.name + " in " + creep.pos.roomName + " has done it's job.");
                }
            }
        }
    }

    private heal(creep: MyCreep, full: boolean): boolean {
        if (creep.hits < creep.hitsMax * (full ? 1 : 0.7)) {
            creep.heal(creep);
            return true;
        }
        // console.log(1);
        return false;
    }

    private collect(creep: MyCreep): boolean {
        let power: any = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
            filter: (x: any) =>
            x.room === creep.room
            && x.power > 0
        });
        // console.log(JSON.stringify(power));
        if (power) {
            let rc = creep.harvest(power);
            if (rc === ERR_NOT_IN_RANGE) {
                creep.moveTo(power);
            }
            return true;
        }
        return false;
    }

    private attack(creep: MyCreep): boolean {
        // console.log(2);
        let source: any = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
                filter: (x: any) =>
                x.room === creep.room
                && x.power > 0
            })
            ;
        // console.log(JSON.stringify(source));
        if (creep.attack(source) === ERR_NOT_IN_RANGE) {
            //console.log(creep.name + " "+creep.moveTo(source));
            if (creep.moveTo(source) === ERR_NO_PATH) {
                creep.moveToXY(25, 25);
                return true;
            }
            //console.log("    "+creep.room.name + " " + creep.name + " " + source);
        } else {
            return true;
        }
        return false;
    }

    public callReplacementRole(creep: MyCreep): void {
        this.roleFactory.getRoleHarvester().runAsSubRole(creep);
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        //TODO currently no auto spawn
        if (1 < 2) {
            return;
        }

        spawnParameter.spawnerStatus.addToReport(this.roleShortName(), -1, -1);
        if (spawnParameter.spawnerStatus.hasSpawned) {
            return;
        }

        this.spawn("TODO", spawnParameter.spawner, spawnParameter.spawner.room.name, spawnParameter.maxEnergyAllowed);
        spawnParameter.spawnerStatus.hasSpawned = true;
    }

    private spawn(targetRoomName: string, spawner: StructureSpawn, baseRoomName: string, maxEnergyCapacityAvailable: number): boolean {
        let nrComponents = Math.floor(maxEnergyCapacityAvailable / (1320));
        let components = [];

        for (let i = 0; i < nrComponents; i++) {
            components.push(TOUGH);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
            components.push(MOVE);
            components.push(MOVE);
            components.push(MOVE);
            components.push(MOVE);
            components.push(MOVE);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(ATTACK);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(HEAL);
            components.push(HEAL);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
            components.push(CARRY);
            components.push(CARRY);
            components.push(CARRY);
            components.push(CARRY);
            components.push(CARRY);
            components.push(CARRY);
        }

        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            baseRoomName: baseRoomName,
            role: this.roleName(),
            targetRoomName: targetRoomName,
            working: false,
        });
        return _.isString(rc);
    }

}

Profiler.registerClass(RolePowerHarvester, RolePowerHarvester.name);
