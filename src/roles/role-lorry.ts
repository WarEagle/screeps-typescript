import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {BaseRole} from "./common/role-base";
import {SpawnerStatus} from "../spawning/spawner-status";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";

export class RoleLorry extends BaseRole {

    public icon(): string {
        return "⛟";
    }

    public roleName(): string {
        return "lorry";
    }

    public roleShortName(): string {
        return "LO";
    }

    public callReplacementRole(): void {
    }

    // public callReplacementRole(creep: MyCreep): void {
    //     this.roleFactory.getRoleRecharger().runAsSubRole(creep);
    // }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 3);

        if (creep.memory.working === true && creep.carry.energy === 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working === false && creep.carry.energy >= creep.carryCapacity * 0.75) {
            creep.memory.working = true;
        }

        if (creep.ticksToLive < 30) {
            if (Game.time % 4 === 0) {
                creep.say("dying", false);
            }
            creep.memory.working = true;
            creep.suicide();
        }

        if (creep.fatigue > 0) {
            return;
        }

        if (this.helperNavigation.moveToRoom(creep, creep.memory.spawnedIn)) {
            if (creep.memory.working === true) {
                //distribute energy
                let structure: any = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (s: any) => s.structureType === STRUCTURE_TOWER
                    && s.energyCapacity * 0.8 > s.energy
                    && s.room === creep.room
                });
                if (structure) {
                    if (creep.transfer(structure, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                        creep.moveTo(structure);
                    }
                } else if (this.helperNavigation.unloadingToSpawnOrExtension(creep)) {
                } else if (this.helperNavigation.unloadingEnergyToTower(creep, 0.8)) {
                } else if (this.helperNavigation.unloadingEnergyToTerminalWithLimit(creep, 100000)) {
                } else if (this.helperNavigation.unloadingEnergyToStorage(creep)) {
                } else {
                    console.log("lorry has problem unloading " + creep.name);
                }
            } else {
                if (this.helperNavigation.collectFromDesiredTarget(creep)) {
                    // console.log("0 "+creep.name);
                } else if (this.helperNavigation.collectEnergyFromGround(creep)) {
                    // console.log("1 "+creep.name);
                } else if (this.helperNavigation.transferFromContainer(creep)) {
                    // console.log("2 "+creep.name);
                } else if (this.helperNavigation.transferFromLink(creep)) { //Link is last, because this is "only optimization" the others are decaying
                    // console.log("3 " +creep.name);
                } else {
                    // console.log("4 " +creep.name);
                    // this.callReplacementRole(creep);
                }
            }
        }
    }

    public spawnIfNeededOldWay(spawner: StructureSpawn, spawnerStatus: SpawnerStatus, currLorry: number, targetLorry: number, currHarvester: number, maxEnergyCapacityAvailable: number): void {

        if (this.tooMuchEnergyOnGround(spawner.room)) {
            targetLorry++;
        }

        if (targetLorry === 0) {
            return;
        }

        spawnerStatus.addToReport(this.roleShortName(), currLorry, targetLorry);

        if (targetLorry <= currLorry || spawnerStatus.hasSpawned) {
            return;
        }

        let nrComponents;
        if (currHarvester === 0 && targetLorry > 0 && currLorry === 0) {
            // Emergency!
            nrComponents = Math.floor(spawner.room.energyAvailable / 150);
        } else {
            nrComponents = Math.floor(maxEnergyCapacityAvailable / 150);
        }
        let components = [];

        for (let i = 0; i < nrComponents * 2; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            working: false,
        });
        spawnerStatus.hasSpawned = true;
    };

    private tooMuchEnergyOnGround(room: Room): boolean {
        let tmp: number = 0;
        _.forEach(room.find(FIND_DROPPED_ENERGY), function (entry: any) {
            // console.log(JSON.stringify(entry));
            tmp += entry.energy;
        });

        // let tmp = _.size(room.find(FIND_DROPPED_ENERGY));
        // console.log(room.name + "=" + tmp);
        return tmp > 1000;
    }

    internalSpawnIfNeeded(): void {
        throw new SyntaxError("This method isn't implemented");
    }

}

Profiler.registerClass(RoleLorry, RoleLorry.name);
