import {SpawnerWrapper} from "../../statistics/spawner-wrapper";
import {BaseRole, SpawnParameter} from "../common/role-base";
import {SpawnerStatus} from "../../spawning/spawner-status";
import {Profiler} from "../../profiler";
import {RoomMemory} from "../../room/room-memory";
import {HelperCalculation} from "../../helper/helper-calculation";
import {MyCreep} from "../../types/my-creep";

export class RoleDefender extends BaseRole {

    private static cntDefenderPerRoom: number = 1;

    public icon(): string {
        return "⛨";
    }

    public roleName(): string {
        return "defender";
    }

    public roleShortName(): string {
        return "DF";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        // correct room?
        if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {

            // heal self
            if (creep.hits < creep.hitsMax && creep.getActiveBodyparts(HEAL)) {
                creep.heal(creep);
            }
            //TODO add remote healing

            if (this.attackEnemyCreeps(creep)) {
            } else if (this.attackEnemyBuildings(creep)) {
            }
        }
        // heal self
        if (creep.hits < creep.hitsMax && creep.getActiveBodyparts(HEAL)) {
            creep.heal(creep);
        }
    }

    private attackEnemyCreeps(creep: MyCreep) {
        let hasEnemyCreeps: boolean = false;

        // first try ranged
        let targets: any = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 3);
        if (targets.length > 0) {
            creep.rangedAttack(targets[0]);
            hasEnemyCreeps = true;
        }

        // then attack melee
        targets = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 1);
        if (targets.length > 0) {
            if (creep.attack(targets[0]) === ERR_NOT_IN_RANGE) {
                creep.moveTo(targets[0]);
            }
        }

        // and now move closer
        let closedTarget: any = creep.pos.findClosestByPath(FIND_HOSTILE_CREEPS);
        if (closedTarget) {
            hasEnemyCreeps = true;
            creep.moveTo(closedTarget);
        } else {
            // move to center of room
            creep.moveToXY(25, 25);
        }
        return hasEnemyCreeps;
    }

    private attackEnemyBuildings(creep: MyCreep) {
        let hasEnemyCreeps: boolean;

        // first try ranged
        let targets: any = creep.pos.findInRange(FIND_HOSTILE_STRUCTURES, 3, {filter: (entry: Structure) => (entry.structureType !== STRUCTURE_CONTROLLER)});
        if (targets.length > 0) {
            creep.rangedAttack(targets[0]);
            hasEnemyCreeps = true;
        }

        // then attack melee
        targets = creep.pos.findInRange(FIND_HOSTILE_STRUCTURES, 1, {filter: (entry: Structure) => (entry.structureType !== STRUCTURE_CONTROLLER)});
        if (targets.length > 0) {
            if (creep.attack(targets[0]) === ERR_NOT_IN_RANGE) {
                creep.moveTo(targets[0]);
            }
        }

        // and now move closer
        let closedTarget: any = creep.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES, {filter: (entry: Structure) => (entry.structureType !== STRUCTURE_CONTROLLER)});
        if (closedTarget) {
            creep.moveTo(closedTarget);
            hasEnemyCreeps = true;
        } else {
            // move to center of room
            creep.moveToXY(25, 25);
            hasEnemyCreeps = true;
        }
        return hasEnemyCreeps;
    }

    public spawnIfNeededOldWay(constants: any, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, maxEnergyAllowed: number): void {
        let hasSpawned = false;

        if (!constants || !constants.defender) {
            return;
        }

        let filteredCreeps = this.getCreepsWithOwnRole();

        for (let remoteRoom of constants.defender) {
            let currentCreeps = _.filter(filteredCreeps, (c) => (
                c.memory.targetRoomName === remoteRoom.targetRoomName
            )).length;

            spawnerStatus.addToReport(this.roleShortName(), currentCreeps, remoteRoom.cnt, remoteRoom.targetRoomName);
            if (spawnerStatus.hasSpawned) {
                continue;
            }

            if (currentCreeps < remoteRoom.cnt) {
                //spawn new one
                hasSpawned = this.spawnDefender(remoteRoom.targetRoomName, spawner.room.name, spawner, maxEnergyAllowed);
                spawnerStatus.hasSpawned = true;
            }
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        let hasSpawned = false;

        let filteredCreeps = this.getCreepsWithOwnRole();

        for (let sourceRoomName in (<RoomMemory> spawnParameter.spawner.room.memory).miningOperations) {
            if (sourceRoomName !== spawnParameter.spawner.room.name) {
                continue;
            }
            for (let targetRoomName in (<RoomMemory> spawnParameter.spawner.room.memory).miningOperations[sourceRoomName]) {

                if ((<RoomMemory> spawnParameter.spawner.room.memory).miningOperations[sourceRoomName][targetRoomName].isSourceKeeper === true) {
                    // no auto-defender in SK-rooms
                    continue;
                }

                let currentCreeps = _.filter(filteredCreeps, (c) => (
                    c.memory.targetRoomName === targetRoomName
                )).length;

                spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currentCreeps, RoleDefender.cntDefenderPerRoom, targetRoomName);
                if (spawnParameter.spawnerStatus.hasSpawned) {
                    continue;
                }

                if (currentCreeps < RoleDefender.cntDefenderPerRoom) {
                    //spawn new one
                    hasSpawned = this.spawnDefender(targetRoomName, spawnParameter.spawner.room.name, spawnParameter.spawner, spawnParameter.maxEnergyAllowed);
                    spawnParameter.spawnerStatus.hasSpawned = true;
                }
            }
        }
    }

    private spawnDefender(targetRoomName: string, baseRoomName: string, spawner: StructureSpawn, maxEnergyAllowed: number): boolean {
        let componentsBig = [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, ATTACK, ATTACK, ATTACK, ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, HEAL, HEAL, MOVE];
        let componentsNormal = [TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, MOVE, MOVE, MOVE, ATTACK, ATTACK, MOVE, MOVE, RANGED_ATTACK, RANGED_ATTACK];
        let componentsSmall = [TOUGH, TOUGH, MOVE, MOVE, ATTACK, ATTACK];

        let rc;
        if (HelperCalculation.costsForBody(componentsBig) <= maxEnergyAllowed) {
            rc = SpawnerWrapper.createCreep(spawner, componentsBig, this, {
                baseRoomName: baseRoomName,
                role: this.roleName(),
                targetRoomName: targetRoomName,
                working: false,
            });
        } else if (HelperCalculation.costsForBody(componentsNormal) <= maxEnergyAllowed) {
            rc = SpawnerWrapper.createCreep(spawner, componentsNormal, this, {
                baseRoomName: baseRoomName,
                role: this.roleName(),
                targetRoomName: targetRoomName,
                working: false,
            });
        } else {
            rc = SpawnerWrapper.createCreep(spawner, componentsSmall, this, {
                baseRoomName: baseRoomName,
                role: this.roleName(),
                targetRoomName: targetRoomName,
                working: false,
            });
        }

        return _.isString(rc);
    }
}
Profiler.registerClass(RoleDefender, RoleDefender.name);
