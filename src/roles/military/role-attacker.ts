import {SpawnerWrapper} from "../../statistics/spawner-wrapper";
import {BaseRole} from "../common/role-base";
import {Profiler} from "../../profiler";
import {MyCreep} from "../../types/my-creep";

export class RoleAttacker extends BaseRole {

    public icon(): string {
        return "❌";
    }

    public roleName(): string {
        return "attacker";
    }

    public roleShortName(): string {
        return "AT";
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        // correct room?
        //FIXME when no route to room, then scan for enemies in current room
        if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
            let filterMilitary: any = {
                filter: (target: Creep) => (this.containsBodyPart(target, ATTACK) || this.containsBodyPart(target, RANGED_ATTACK))
            };
            let filterEconomy: any = {
                filter: (target: Structure) => (
                    target.structureType !== STRUCTURE_ROAD
                )
            };
            if (this.attackEnemy(creep, filterMilitary)) {
                // console.log("attack military");
                //
            } else if (this.attackEnemy(creep, filterEconomy)) {
                // console.log("attack civil");
                //
            } else {
                // console.log("attack building");
                // attack building
                this.attackBuilding(creep, filterEconomy);
            }
        }
    }

    //FIXME this isn't working correctly, it attacks workers
    private containsBodyPart(target: Creep, partToSearchFor: string) {
        return target.body.filter((part: BodyPartDefinition) => (part.type === partToSearchFor));
    }

    public callReplacementRole(): void {
    }

    private attackEnemy(creep: MyCreep, filter: any) {
        // console.log("+1");
        // first try ranged-attack
        let targets: any = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 3, filter);
        if (targets.length > 0) {
            // console.log("+2");
            creep.rangedAttack(targets[0]);
        }

        // then attack melee-attack
        targets = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 1, filter);
        if (targets.length > 0) {
            // console.log("+3");
            if (creep.attack(targets[0]) === ERR_NOT_IN_RANGE) {
                let rc = creep.moveTo(targets[0]);
                if (rc === ERR_NO_PATH) {
                    return false;
                }
            }
        }

        // and now move closer
        let closedTarget: any = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, filter);
        creep.moveTo(closedTarget);

        return !!closedTarget;
    }

    //FIXME restrict to enemy buildings or walls
    private attackBuilding(creep: MyCreep, filter: any) {
        // console.log("1");
        // first try ranged-attack
        let targets: any = creep.pos.findInRange(FIND_STRUCTURES, 3, filter);
        if (targets.length > 0) {
            // console.log("2");
            creep.rangedAttack(targets[0]);
        }

        // then attack melee-attack
        targets = creep.pos.findInRange(FIND_STRUCTURES, 1, filter);
        // console.log("4 "+targets.length);
        if (targets.length > 0) {
            if (creep.attack(targets[0]) === ERR_NOT_IN_RANGE) {
                //console.log("3");
                let rc = creep.moveTo(targets[0]);
                if (rc === ERR_NO_PATH) {
                    return false;
                }
            }
        }

        // and now move closer
        let closedTarget: any = creep.pos.findClosestByPath(FIND_STRUCTURES, filter);
        creep.moveTo(closedTarget);
        // console.log("4");

        return !!closedTarget;
    }

    internalSpawnIfNeeded(): void {
        //TODO still uses old code!
        // let hasSpawned = false;

        // for (let remoteRoom of constants.attacker) {
        //     let currentCreeps = ScreepUtils.count(Game.creeps, (c) => (
        //         c.memory.role === this.roleName()
        //         // && c.memory.baseRoomName === roomName
        //         && c.memory.targetRoomName === remoteRoom.targetRoomName
        //     ));
        //
        //     spawnerStatus.addToReport(this.roleShortName(), currentCreeps, remoteRoom.cnt, remoteRoom.targetRoomName);
        //     if (spawnerStatus.hasSpawned) {
        //         continue;
        //     }
        //
        //     if (currentCreeps < remoteRoom.cnt) {
        //         //spawn new one
        //         hasSpawned = this.spawnAttacker(remoteRoom.targetRoomName, roomName, spawner);
        //         spawnerStatus.hasSpawned = true;
        //     }
        // }
    }

    protected spawnAttacker(targetRoomName: string, baseRoomName: string, spawner: StructureSpawn): boolean {
        let components = [];
        // https://codepen.io/findoff/details/RPmqOd
        components.push(TOUGH);
        components.push(TOUGH);
        components.push(TOUGH);
        components.push(TOUGH);
        components.push(TOUGH);
        components.push(TOUGH);
        // components.push(TOUGH);

        // components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);

        components.push(ATTACK);
        components.push(ATTACK);
        components.push(MOVE);
        components.push(RANGED_ATTACK);
        components.push(RANGED_ATTACK);
        components.push(MOVE);

        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            baseRoomName: baseRoomName,
            role: this.roleName(),
            targetRoomName: targetRoomName,
            working: false,
        });
        return _.isString(rc);
    }

}
Profiler.registerClass(RoleAttacker, RoleAttacker.name);
