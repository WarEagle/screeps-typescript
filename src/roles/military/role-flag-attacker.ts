import {BaseRole, SpawnParameter} from "../common/role-base";
import {SpawnerStatus} from "../../spawning/spawner-status";
import {Profiler} from "../../profiler";
import {MyCreep} from "../../types/my-creep";
import {SpawnerWrapper} from "../../statistics/spawner-wrapper";

export class RoleFlagAttacker extends BaseRole {

    public icon(): string {
        return "⚑❌";
    }

    public roleName(): string {
        return "flagAttacker";
    }

    public roleShortName(): string {
        return "FAT";
    }

    public callReplacementRole() {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        let flag = Game.flags[creep.memory.flagName];
        // console.log(JSON.stringify(flag));
        if (!flag) {
            console.log(creep.name + " is missing his flag!");
            return;
        }

        if (!flag.room || flag.room.name !== creep.room.name) {
            let moveToOpts = <MoveToOpts> {reusePath: 25, maxOps: 2000, maxRooms: 10};
            creep.moveTo(flag, moveToOpts);
        } else {
            if (creep.memory.target || this.enemiesInRange(creep)) {
                this.huntTarget(creep);
            } else {
                if (this.helperNavigation.moveCloseTo(creep, flag.pos)) {
                    if (this.attackFlagTarget(creep, flag.pos)) {
                    } else if (this.attackCloseBuilding(creep, flag.pos)) {
                    }
                }
            }
        }
    }

    private attackFlagTarget(creep: MyCreep, pos: RoomPosition): boolean {
        // console.log(creep.name + " " + "attackFlagTarget");
        let lookResult = pos.look();
        let tmpTarget: any;
        let foundTarget: boolean = false;
        for (let look of lookResult) {
            // console.log(creep.name + " " + JSON.stringify(look));
            if (look.structure) {
                tmpTarget = Game.getObjectById(look.structure.id);
                foundTarget = true;
            }
        }
        creep.rangedAttack(tmpTarget);
        return foundTarget;
    }

    private attackCloseBuilding(creep: MyCreep, pos: RoomPosition): boolean {
        // console.log(creep.name + " " + "attackCloseBuilding");
        let structure = pos.findClosestByRange(FIND_HOSTILE_STRUCTURES);
        if (!structure) {
            return false;
        }

        creep.rangedAttack(<Structure>structure);
        return false;
    }

    private enemiesInRange(creep: MyCreep): boolean {
        // console.log("enemiesInRange");
        let targetCreeps = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 5, this.filterWhitelistMilitary);
        // let targetCreep = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 7);
        if (!targetCreeps || targetCreeps.length <= 0) {
            return false;
        }
        creep.memory.target = targetCreeps[0].id;
        return true;
    }

    private huntTarget(creep: MyCreep) {
        // console.log("huntTarget");
        let targetObject = <Structure|Creep> Game.getObjectById(creep.memory.target);

        // console.log(JSON.stringify(targetObject));
        if (!targetObject) {
            delete creep.memory.target;
            return;
        }

        let distance = creep.pos.getRangeTo(targetObject);

        // if (creep.hits <= creep.hitsMax * 0.25 || creep.memory.heal === true) {
        //     console.log("kite");
        //     creep.memory.heal = true;
        //     this.helperNavigation.kite(distance, creep, targetObject);
        // } else {
        console.log("fullAttack");
        this.fullAttack(distance, creep, targetObject);
        // }

    }

    private fullAttack(distance: number, creep: MyCreep, targetObject: any) {
        if (distance >= 6) {
            delete creep.memory.target;
        } else if (distance <= 3) {
            creep.rangedAttack(targetObject);
        } else {
            let rc = creep.moveTo(targetObject);
            if (rc === ERR_NO_PATH) {
                delete creep.memory.target;
            }
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        // let hasSpawned = false;

        for (let flagName in Game.flags) {
            let flag = <Flag> Game.flags[flagName];
            if (flagName.substr(0, 7) === "Attack ") {
                // console.log(JSON.stringify(flag));
                this.checkSpawnForFlag(flag, spawnParameter.spawner, spawnParameter.spawnerStatus, spawnParameter.filteredCreeps);
                // } else {
                // console.log("NOK " + flag.name);
            }
        }
    }

    private checkSpawnForFlag(flag: Flag, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, filteredCreeps: Creep[]) {
        let currentCreeps = 0;
        _.forEach(filteredCreeps, (creep: Creep) => {
            if (creep.ticksToLive > 250) {
                currentCreeps += creep.memory.flagName === flag.name ? 1 : 0;
            }
        });
        let targetNumber = 1;

        //TODO spawnerStatus.addToReport(this.roleShortName(), cnt, targetNumber, flag.name);

        if (spawnerStatus.hasSpawned) {
            return;
        }

        if (currentCreeps >= targetNumber) {
            return;
        }

        if (1 < 2) {
            return;
        }

        this.spawn(flag, spawner);
        spawnerStatus.hasSpawned = true;
    }

    private spawn(flag: Flag, spawner: StructureSpawn) {
        // 25 Move, 25 RangeAttack
        let components = [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK];

        SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            flagName: flag.name,
            working: false,
        });
    }

}
Profiler.registerClass(RoleFlagAttacker, RoleFlagAttacker.name);
