import {BaseRole, SpawnParameter} from "../common/role-base";
import {SpawnerStatus} from "../../spawning/spawner-status";
import {Profiler} from "../../profiler";
import {MyCreep} from "../../types/my-creep";
import {SpawnerWrapper} from "../../statistics/spawner-wrapper";

export class RoleAreaDefender extends BaseRole {

    public icon(): string {
        return "⛨❌";
    }

    public roleName(): string {
        return "areaDefender";
    }

    public roleShortName(): string {
        return "AD";
    }

    public callReplacementRole() {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        let flag = Game.flags[creep.memory.flagName];
        // console.log(JSON.stringify(flag));
        if (!flag) {
            console.log(creep.name + " is missing his flag!");
            return;
        }

        if (this.needsBoost(creep)) {
            return;
        }

        if (!flag.room) {
            creep.moveTo(flag);
        } else if (this.helperNavigation.moveToRoom(creep, flag.pos.roomName)) {
            if (this.enemiesInRange(creep) || (creep.memory.target)) {
                this.huntTarget(creep);
            } else {
                if (this.helperNavigation.moveCloseTo(creep, flag.pos)) {
                    if (creep.hits < creep.hitsMax) {
                        creep.rangedHeal(creep.getOriginalCreep());
                    }
                }
                creep.heal(creep);
            }
            this.healOther(creep, false);
        }
    }

    private enemiesInRange(creep: MyCreep): boolean {
        //TODO heal other creeps if damaged and close
        // console.log("enemiesInRange");
        let targetCreeps = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 7);
        // let targetCreep = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 7);
        if (!targetCreeps || targetCreeps.length <= 0) {
            return false;
        }
        creep.memory.target = targetCreeps[0].id;
        return true;
    }

    private huntTarget(creep: MyCreep) {
        // console.log("huntTarget");
        let targetCreep = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS,
            {
                filter: (enemy: Creep) => {
                    // console.log(enemy.id + "=== " + creep.memory.target);
                    return enemy.id === creep.memory.target;
                }
            });
        // console.log(JSON.stringify(targetCreep));
        if (!targetCreep) {
            delete creep.memory.target;
            return;
        }

        let distance = creep.pos.getRangeTo(targetCreep);
        // let distance = targetCreep.pos.x - creep.pos.x + targetCreep.pos.y - creep.pos.y;

        if (creep.hits >= creep.hitsMax) {
            delete creep.memory.heal;
        }
        if (creep.hits <= creep.hitsMax * 0.1 || creep.memory.heal === true) {
            // console.log("kiteAndHeal");
            creep.memory.heal = true;
            this.kiteAndHeal(distance, creep, targetCreep);
        } else {
            // console.log("fullAttack");
            this.fullAttack(distance, creep, targetCreep);
        }

    }

    private kiteAndHeal(distance: number, creep: MyCreep, targetCreep: Creep) {
        if (distance <= 5) {
            let targetDirection = creep.pos.getDirectionTo(targetCreep);

            //TODO improve this with some random
            let kiteDirection;
            switch (targetDirection) {
                case TOP:
                    kiteDirection = BOTTOM;
                    break;
                case TOP_RIGHT:
                    kiteDirection = BOTTOM_LEFT;
                    break;
                case RIGHT:
                    kiteDirection = LEFT;
                    break;
                case BOTTOM_RIGHT:
                    kiteDirection = TOP_LEFT;
                    break;
                case BOTTOM:
                    kiteDirection = TOP;
                    break;
                case BOTTOM_LEFT:
                    kiteDirection = TOP_RIGHT;
                    break;
                case LEFT:
                    kiteDirection = RIGHT;
                    break;
                case TOP_LEFT:
                    kiteDirection = BOTTOM_RIGHT;
                    break;
                default:
                    kiteDirection = BOTTOM;
            }
            if (creep.pos.x <= 2) {
                kiteDirection = TOP;
            } else if (creep.pos.x >= 48) {
                kiteDirection = BOTTOM;
            }
            creep.move(kiteDirection);
            creep.rangedAttack(targetCreep);
            creep.rangedHeal(creep.getOriginalCreep());
        } else {
            creep.rangedHeal(creep.getOriginalCreep());
        }
    }

    private fullAttack(distance: number, creep: MyCreep, targetCreep: Creep) {
        if (distance <= 1) {
            creep.attack(targetCreep);
        } else {
            creep.moveTo(targetCreep);
            creep.heal(creep);
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        // let hasSpawned = false;

        for (let flagName in Game.flags) {
            let flag = <Flag> Game.flags[flagName];
            if (flagName.substr(0, 8) === "Defence ") {
                // console.log(JSON.stringify(flag));
                this.checkSpawnForFlag(flag, spawnParameter.spawner, spawnParameter.spawnerStatus, spawnParameter.filteredCreeps);
                // } else {
                // console.log("NOK " + flag.name);
            }
        }
    }

    private checkSpawnForFlag(flag: Flag, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, filteredCreeps: Creep[]) {
        let currentCreeps = 0;
        _.forEach(filteredCreeps, (creep: Creep) => {
            if (creep.memory.role === this.roleName() && creep.ticksToLive > 250) {
                currentCreeps += creep.memory.flagName === flag.name ? 1 : 0;
            }
        });
        let targetNumber = 1;

        // console.log("cnt=" + cnt);

        //TODO spawnerStatus.addToReport(this.roleShortName(), cnt, targetNumber, flag.name);

        if (spawnerStatus.hasSpawned) {
            return;
        }

        if (currentCreeps >= targetNumber) {
            return;
        }

        console.log("currentCreeps=" + currentCreeps);
        console.log("targetNumber=" + targetNumber);

        if (1 < 2) {
            return;
        }

        this.spawn(flag, spawner);
        spawnerStatus.hasSpawned = true;
    }

    private spawn(flag: Flag, spawner: StructureSpawn) {
        // 25 Move, 20 Attack, 5 Heal
        let components = [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, ATTACK, MOVE, HEAL, HEAL, HEAL, HEAL];

        SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            flagName: flag.name,
            working: false,
            lab: "58754a80c7af030903e61091",
        });
        //TODO remove hardcoded lab-id
    }

    private healOther(creep: MyCreep, withMove: boolean) {
        // console.log("healOther");
        let damagedCreeps = creep.pos.findInRange(FIND_MY_CREEPS, 3, {
                filter: (filterCreep: Creep) => {
                    return filterCreep.hitsMax > filterCreep.hits
                        && filterCreep.id !== creep.getOriginalCreep().id
                        ;
                }
            }
        );
        if (!damagedCreeps || damagedCreeps.length <= 0) {
            return;
        }
        // console.log(damagedCreeps.length);
        let rc = creep.rangedHeal(damagedCreeps[0]);
        if (rc === ERR_NOT_IN_RANGE && withMove) {
            creep.moveTo(damagedCreeps[0]);
        }
    }

}
Profiler.registerClass(RoleAreaDefender, RoleAreaDefender.name);
