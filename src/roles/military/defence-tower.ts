import {Profiler} from "../../profiler";
export class DefenceTower {

    public repairIfExists(tower: StructureTower, onlyMy: boolean, filter: any): boolean {

        let structure: any;
        if (onlyMy) {
            structure = tower.pos.findInRange(FIND_MY_STRUCTURES, 50, filter);
        } else {
            structure = tower.pos.findInRange(FIND_STRUCTURES, 50, filter);
        }
        if (structure && structure.length > 0) {
            // console.log(structure.hits + "/" + structure.hitsMax)
            tower.repair(structure[Math.floor(Math.random() * structure.length)]);
            return true;
        }
        return false;
    }

    public repairBuildings(tower: StructureTower): boolean {
        // console.log(Game.time + " - " + tower.pos.roomName + " -> " + tower.id);
        if (tower.energy < tower.energyCapacity * 0.75) {
            // save energy for defending
            return false;
        }
        let tick = Game.time % 10;
        // tick is used to save CPU
        if (tick === 0 && this.repairIfExists(tower, true, {
                filter: (s: any) => (
                    s.hits / s.hitsMax <= 0.9
                    && s.structureType !== STRUCTURE_WALL
                    && s.structureType !== STRUCTURE_RAMPART
                    && s.pos.roomName === tower.pos.roomName
                ),
            })) {
            return true;
        } else if (tick >= 2 && this.repairIfExists(tower, false, {
                filter: (s: any) => (
                    (
                        s.structureType === STRUCTURE_ROAD
                        // || s.structureType === STRUCTURE_CONTAINER
                    )
                    && s.hits / s.hitsMax <= 0.75
                    && s.pos.roomName === tower.pos.roomName
                ),
            })) {
            return true;
        } else if (this.repairIfExists(tower, false, {
                filter: (s: any) => (
                    s.structureType === STRUCTURE_RAMPART
                    && s.hits / s.hitsMax <= 0.002
                    && s.pos.roomName === tower.pos.roomName
                ),
            })) {
            return true;
        } else if (this.repairIfExists(tower, false, {
                filter: (s: any) => (
                    s.structureType === STRUCTURE_WALL
                    && s.hits / s.hitsMax <= 0.0001
                    && s.pos.roomName === tower.pos.roomName
                ),
            })) {
            return true;
        }
        return false;
    }

    public defendRoom(room: Room): boolean {

        let hostiles: any = room.find(FIND_HOSTILE_CREEPS);

        if (hostiles.length > 0) {
            let username = hostiles[0].owner.username;
            Game.notify(`User ${username} spotted in room ${room.name}`);
            let towers: any[] = room.find(
                FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
            towers.forEach(tower => tower.attack(hostiles[Math.floor(Math.random()*_.size(hostiles))]));
            return true;
        }
        return false;
    }

    public healCreeps(tower: StructureTower) {
        let room = Game.rooms[tower.pos.roomName];
        let damagedCreeps = room.find(FIND_MY_CREEPS, {
            filter: (cr: Creep) => (cr.hits < cr.hitsMax),
        });
        if (damagedCreeps && damagedCreeps.length > 0) {
            tower.heal(<Creep>damagedCreeps[0]);
            return true;
        }
        return false;
    }
}
Profiler.registerClass(DefenceTower, DefenceTower.name);
