import {BaseRole, SpawnParameter} from "../common/role-base";
import {SpawnerStatus} from "../../spawning/spawner-status";
import {Profiler} from "../../profiler";
import {MyCreep} from "../../types/my-creep";
import {SpawnerWrapper} from "../../statistics/spawner-wrapper";

export class RoleFlagHealer extends BaseRole {

    public icon(): string {
        return "⛽";
    }

    public roleName(): string {
        return "flagHealer";
    }

    public roleShortName(): string {
        return "FHL";
    }

    public callReplacementRole() {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        let flag = Game.flags[creep.memory.flagName];
        // console.log(JSON.stringify(flag));
        if (!flag) {
            console.log(creep.name + " is missing his flag!");
            return;
        }

        if (this.needsBoost(creep)) {
            return;
        }

        if (creep.hitsMax > creep.hits) {
            console.log(creep.name + " heals self");
            creep.heal(creep);
        }
        if (!flag.room || flag.room.name !== creep.room.name) {
            let moveToOpts = <MoveToOpts> {reusePath: 25, maxOps: 2000, maxRooms: 10};
            creep.moveTo(flag, moveToOpts);
        } else {
            if (this.helperNavigation.moveCloseTo(creep, flag.pos, 1)) {
                this.healOther(creep);
            }
        }
    }

    private healOther(creep: MyCreep): boolean {
        // console.log("enemiesInRange");
        let targetCreeps = creep.pos.findInRange(FIND_MY_CREEPS, 3, {
            filter: (filterCreep: Creep) => {
                return filterCreep.hitsMax > filterCreep.hits
                    && filterCreep.id !== creep.getOriginalCreep().id
                    ;
            }
        });
        // console.log(JSON.stringify(targetCreeps));
        // let targetCreep = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 7);
        if (!targetCreeps || targetCreeps.length <= 0) {
            return false;
        }
        console.log(creep.name + " range healing " + targetCreeps[0].name);
        creep.rangedHeal(targetCreeps[0]);
        return true;
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        // let hasSpawned = false;

        for (let flagName in Game.flags) {
            let flag = <Flag> Game.flags[flagName];
            if (flagName.substr(0, 5) === "Heal ") {
                // console.log(JSON.stringify(flag));
                this.checkSpawnForFlag(flag, spawnParameter.spawner, spawnParameter.spawnerStatus, spawnParameter.filteredCreeps);
                // } else {
                // console.log("NOK " + flag.name);
            }
        }
    }

    private checkSpawnForFlag(flag: Flag, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, filteredCreeps: Creep[]) {
        let currentCreeps = 0;
        _.forEach(filteredCreeps, (creep: Creep) => {
            if (creep.ticksToLive > 250) {
                currentCreeps += creep.memory.flagName === flag.name ? 1 : 0;
            }
        });
        let targetNumber = 1;

        //TODO spawnerStatus.addToReport(this.roleShortName(), cnt, targetNumber, flag.name);

        if (spawnerStatus.hasSpawned) {
            return;
        }

        if (currentCreeps >= targetNumber) {
            return;
        }

        if (1 < 2) {
            return;
        }

        this.spawn(flag, spawner);
        spawnerStatus.hasSpawned = true;
    }

    private spawn(flag: Flag, spawner: StructureSpawn) {
        // 18 Move, 18 Heal
        let components = [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, HEAL, MOVE];

        SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            flagName: flag.name,
            working: false,
            lab: "58754a80c7af030903e61091",
        });
        //TODO remove hardcoded lab-id
    }

}
Profiler.registerClass(RoleFlagHealer, RoleFlagHealer.name);
