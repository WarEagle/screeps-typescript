import {BaseRole} from "../common/role-base";
import {Profiler} from "../../profiler";
import {MyCreep} from "../../types/my-creep";

// Game.creeps.Adrian.memory = {role:'healer'}
export class RoleHealer extends BaseRole {

    // private static cntDefenderPerRoom: number = 1;

    public icon(): string {
        return "TODO";
    }

    public roleName(): string {
        return "healer";
    }

    public roleShortName(): string {
        return "HE";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 8);

        // correct room?
        if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {

            let flagDrain = <Flag>Game.flags[creep.memory.attackFlag];
            let flagRegenerate = <Flag>Game.flags[creep.memory.attackFlag];
            if (creep.hits < creep.hitsMax * 0.5) {
                creep.moveTo(flagRegenerate);
                creep.heal(creep);
            } else {
                creep.moveTo(flagDrain);
            }

            // // first try ranged
            // let targets: any = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 3);
            // if (targets.length > 0) {
            //     creep.rangedAttack(targets[0]);
            // }
            //
            // // then attack melee
            // targets = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 1);
            // if (targets.length > 0) {
            //     if (creep.attack(targets[0]) === ERR_NOT_IN_RANGE) {
            //         creep.moveTo(targets[0]);
            //     }
            // }
            //
            // // and now move closer
            // let closedTarget: any = creep.pos.findClosestByPath(FIND_HOSTILE_CREEPS);
            // if (closedTarget) {
            //     creep.moveTo(closedTarget);
            // } else {
            //     // move to center of room
            //     creep.moveTo(25, 25);
            // }
        }
    }

    //     internalSpawnIfNeeded(spawner: StructureSpawn, spawnerStatus: SpawnerStatus, roomController: RoomControllerCommon, maxEnergyAllowed: number, filteredCreeps: Creep[]): void {
    // let hasSpawned = false;
    //
    // for (let sourceRoomName in (<RoomMemory> spawner.room.memory).miningOperations) {
    //     if (sourceRoomName !== roomName) {
    //         continue;
    //     }
    //     for (let targetRoomName in (<RoomMemory> spawner.room.memory).miningOperations[sourceRoomName]) {
    //
    //         let currentCreeps = ScreepUtils.count(Game.creeps, (c) => (
    //             c.memory.role === this.roleName()
    //             && c.memory.targetRoomName === targetRoomName
    //         ));
    //
    //         spawnerStatus.addToReport(this.roleShortName(), currentCreeps, RoleDefender.cntDefenderPerRoom, targetRoomName);
    //         if (spawnerStatus.hasSpawned) {
    //             continue;
    //         }
    //
    //         if (currentCreeps < RoleDefender.cntDefenderPerRoom) {
    //             //spawn new one
    //             hasSpawned = this.spawnDefender(targetRoomName, roomName, spawner, maxEnergyAllowed);
    //             spawnerStatus.hasSpawned = true;
    //         }
    //     }
    // }
    // }

    // private spawnDefender(targetRoomName: string, baseRoomName: string, spawner: StructureSpawn, maxEnergyAllowed: number): boolean {
    //     let componentsNormal = [TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, MOVE, MOVE, MOVE, ATTACK, ATTACK, MOVE, MOVE, RANGED_ATTACK, RANGED_ATTACK];
    //     let componentsSmall = [TOUGH, TOUGH, MOVE, MOVE, ATTACK, ATTACK];
    //
    //     let rc;
    //     if (HelperCalculation.costsForBody(componentsNormal) <= maxEnergyAllowed) {
    //         rc = SpawnerWrapper.createCreep(spawner, componentsNormal, this, {
    //             baseRoomName: baseRoomName,
    //             role: this.roleName(),
    //             targetRoomName: targetRoomName,
    //             working: false,
    //         });
    //     } else {
    //         rc = SpawnerWrapper.createCreep(spawner, componentsSmall, this, {
    //             baseRoomName: baseRoomName,
    //             role: this.roleName(),
    //             targetRoomName: targetRoomName,
    //             working: false,
    //         });
    //     }
    //
    //     return _.isString(rc);
    // }

    internalSpawnIfNeeded(): void {
        throw new SyntaxError("This method isn't implemented");
    }

}
Profiler.registerClass(RoleHealer, RoleHealer.name);
