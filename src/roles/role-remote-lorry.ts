import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {BaseRole, SpawnParameter} from "./common/role-base";
import {SpawnerStatus} from "../spawning/spawner-status";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";
import {RoomMemory, MiningContainer} from "../room/room-memory";

export class RoleRemoteLorry extends BaseRole {

    public icon(): string {
        return "⛐⛟";
    }

    public roleName(): string {
        return "remoteLorry";
    }

    public roleShortName(): string {
        return "RLO";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 3);

        if (creep.memory.working === true && creep.carry.energy === 0) {
            creep.memory.working = false;
            creep.memory.desiredTarget = creep.memory.container;
        }

        if (creep.memory.working === false && creep.carry.energy >= creep.carryCapacity * 0.75) {
            creep.memory.working = true;
        }

        if (creep.ticksToLive < 70) {
            if (Game.time % 4 === 0) {
                creep.say("dying", false);
            }
            creep.memory.working = true;
            creep.suicide();
        }

        if (creep.fatigue > 0) {
            return;
        }

        if (creep.memory.working === true) {
            if (this.helperNavigation.moveToRoom(creep, creep.memory.sourceRoomName)) {
                this.unload(creep);
            }
        } else {
            if (this.helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
                this.load(creep);
            }
        }
    }

    private load(creep: MyCreep) {
        //TODO switch between  containers from memory
        if (this.helperNavigation.collectFromDesiredTarget(creep)) {
        } else if (this.helperNavigation.transferFromContainer(creep)) {
        } else if (this.helperNavigation.collectEnergyFromGround(creep)) {
        } else {
        }
    }

    //distribute energy
    private unload(creep: MyCreep) {
        if (this.helperNavigation.transferToUnloadingLink(creep, 10)) {
        } else if (this.helperNavigation.unloadingToSpawnOrExtension(creep)) {
        } else if (this.helperNavigation.unloadingEnergyToTower(creep, 0.8)) {
        } else if (this.helperNavigation.unloadingEnergyToTerminalWithLimit(creep, 100000)) {
        } else if (this.helperNavigation.unloadingEnergyToStorage(creep)) {
        } else {
            // all storage is full
            // console.log("lorry has problem unloading " + creep.name);
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {

        let room = spawnParameter.spawner.room;
        let roomMemory = <RoomMemory> room.memory;

        // let targetTotal = 2;
        let targetTotal = 1;
        let possibleRelevantCreeps = this.getCreepsWithOwnRole();

        for (let src in roomMemory.miningOperations) {
            for (let trg in roomMemory.miningOperations[src]) {
                let operation = roomMemory.miningOperations[src][trg].containers;
                for (let container in operation) {
                    // console.log(JSON.stringify(operation[container]));
                    // console.log(!operation[container].containerId)
                    // console.log(!operation[container].isBuilt)
                    // console.log(operation[container].isBuilt !== true)
                    if (!operation[container].containerId || operation[container].isBuilt !== true) {
                        continue;
                    }

                    if (roomMemory.miningOperations[src][trg].isSourceKeeper === true) {
                        // no auto-defender in SK-rooms
                        targetTotal++;
                        targetTotal++;
                    }
                    // if (operation[container].distance > 80) {
                    //     targetTotal++;
                    // }
                    if (operation[container].distance > 60) {
                        targetTotal++;
                    }

                    // console.log(1)
                    // use one lorry for each source, maybe change this later to one lorry for all sources?
                    let currCountTotal = this.getExistingCreepsForMiningOperation(operation[container], possibleRelevantCreeps, src, trg);
                    spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currCountTotal, targetTotal, trg);
                    this.spawn(src, trg, spawnParameter.spawner, spawnParameter.spawnerStatus, currCountTotal, targetTotal, operation[container], spawnParameter.maxEnergyAllowed);
                }
            }
        }
    }

    private spawn(sourceRoomName: string, targetRoomName: string, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, currCountTotal: number, targetTotal: number, container: MiningContainer, maxEnergyCapacityAvailable: number) {
        if (spawnerStatus.hasSpawned) {
            return;
        }
        if (currCountTotal >= targetTotal) {
            return;
        }
        //TODO also repair roads?
        // let comp1onentsBase = [CARRY, CARRY, MOVE];

        let nrComponents = Math.floor(maxEnergyCapacityAvailable / 100);

        let components = [];
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            working: false,
            sourceRoomName: sourceRoomName,
            targetRoomName: targetRoomName,
            container: container.containerId,
            x: container.x,
            y: container.y
        });

        spawnerStatus.hasSpawned = true;

    }

    private getExistingCreepsForMiningOperation(container: MiningContainer, possibleRelevantCreeps: any, sourceRoomName: string, targetRoomName: string): number {
        // console.log("container=" + JSON.stringify(container));
        // console.log("possibleRelevantCreeps.length=" + possibleRelevantCreeps.length);
        // console.log("possibleRelevantCreeps=" + JSON.stringify(possibleRelevantCreeps));
        let cnt = _.filter(possibleRelevantCreeps, function (creep: MyCreep) {
                // console.log("sourceRoomName="+sourceRoomName)
                // console.log("sourceRoomName="+sourceRoomName)
                // console.log("targetRoomName="+targetRoomName)
                // console.log("targetRoomName="+targetRoomName)
                // console.log("creep.memory.container="+creep.memory.container)
                // console.log("container.containerId="+container.containerId)
                return (
                    creep.memory.sourceRoomName === sourceRoomName
                    && creep.memory.targetRoomName === targetRoomName
                    && creep.memory.container === container.containerId
                    // && creep.memory.x === container.x
                    // && creep.memory.y === container.y
                );
            }
        ).length;

        // console.log("cnt=" + cnt);
        return cnt;
    }

}

Profiler.registerClass(RoleRemoteLorry, RoleRemoteLorry.name);
