import {BaseRole} from "./common/role-base";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";
export class RoleTransfer extends BaseRole {

    public icon(): string {
        return "TODO";
    }

    public roleName(): string {
        return "transfer";
    }

    public roleShortName(): string {
        return "TR";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {

        let way = creep.memory.way.split(",");
        // correct room?
        // console.log(creep.room.name);
        // console.log(way[way.length - 1]);
        if (creep.room.name === way[way.length - 1]) {
            console.log(creep.name + " reached goal!!");
            creep.memory.role = creep.memory.newrole;
            creep.moveToXY(15, 15);
        } else {
            for (let i = 0; i < way.length; i++) {
                if (creep.room.name === way[i]) {
                    let exitDir: any = Game.map.findExit(creep.room, way[i + 1]);
                    let exit: any = creep.pos.findClosestByRange(exitDir);
                    creep.moveTo(exit);
                }
            }
        }
    }

    internalSpawnIfNeeded(): void {
        throw new SyntaxError("This method isn't implemented");
    }

}
Profiler.registerClass(RoleTransfer, RoleTransfer.name);
