import {BaseRole, SpawnParameter} from "./common/role-base";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {ScreepUtils} from "../helper/screep-utils";
import {Profiler} from "../profiler";
import {MyCreep} from "../types/my-creep";

export class RoleFixedHarvester extends BaseRole {

    public icon(): string {
        return "⚿⚒";
    }

    public roleName(): string {
        return "fixedHarvester";
    }

    public roleShortName(): string {
        return "FH";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 1);

        let x = creep.memory.x;
        let y = creep.memory.y;
        let sourceId = creep.memory.sourceId;

        if (creep.pos.x !== x || creep.pos.y !== y) {
            // let path = creep.pos.findPathTo(x, y);
            // creep.moveByPath(path);
            creep.moveToXY(x, y);
        } else {
            //collect energy
            let source: any = Game.getObjectById(sourceId);
            creep.harvest(source);
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        let room = spawnParameter.spawner.room;
        let roomMemory = room.memory;

        if (!roomMemory.fixedHarvesters || roomMemory.fixedHarvesters.length === 0) {
            return;
        }

        let hasSpawned = false;
        let targetTotal = 0;

        let currCountTotal = ScreepUtils.countArray(spawnParameter.filteredCreeps, (c) => (
            c.room.name === spawnParameter.spawner.room.name
            && c.ticksToLive > 50
        ));
        // console.log("currCount "+currCount);
        for (let x of roomMemory.fixedHarvesters) {
            if (x.active) {
                targetTotal++;
            }
        }
        spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currCountTotal, targetTotal);
        if (spawnParameter.spawnerStatus.hasSpawned) {
            return;
        }

        // console.log("   FH " + roomName + ": " + currCountTotal + "/" + targetTotal);
        if (currCountTotal >= targetTotal) {
            // nothing to do
            return;
        }

        //spawn new one
        for (let harvester of roomMemory.fixedHarvesters) {
            let currCount = ScreepUtils.countArray(spawnParameter.filteredCreeps, (c: Creep) => (
                c.room.name === spawnParameter.spawner.room.name
                && c.memory.x === harvester.x
                && c.memory.y === harvester.y
                && c.ticksToLive >= 50
            ));
            if (currCount < 1) {
                // this specific does not exist, so create it
                hasSpawned = this.spawnFixedHarvester(spawnParameter.spawner, harvester.x, harvester.y);
                spawnParameter.spawnerStatus.hasSpawned = true;
            }
        }
    }

    private spawnFixedHarvester(spawner: StructureSpawn, x: number, y: number): boolean {
        let components = [];

        components.push(WORK);
        components.push(WORK);
        components.push(WORK);
        components.push(WORK);
        components.push(WORK);
        // components.push(WORK);

        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);

        let pos = new RoomPosition(x, y, spawner.room.name);
        // console.log(JSON.stringify(pos));
        let source: any = pos.findClosestByRange(FIND_SOURCES);
        // console.log(JSON.stringify(source));

        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            sourceId: source.id,
            working: false,
            x: x,
            y: y,
        });
        return _.isString(rc);
    }

}
Profiler.registerClass(RoleFixedHarvester, RoleFixedHarvester.name);
