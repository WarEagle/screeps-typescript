import {RoleHarvester} from "../role-harvester";
import {RoleUpgrader} from "../role-upgrader";
import {RoleBuilder} from "../role-builder";
import {RoleRepairer} from "../role-repairer";
import {RoleRecharger} from "../role-recharger";
import {RoleLdHarvester} from "../role-remote-harvester";
import {RoleClaimer} from "../role-claimer";
import {RoleTransfer} from "../role-transfer";
import {RoleRemoteBuilder} from "../role-remote-builder";
import {RoleDefender} from "../military/role-defender";
import {RoleAttacker} from "../military/role-attacker";
import {RoleMiner} from "../role-miner";
import {RoleFixedHarvester} from "../role-fixed-harvester";
import {RoleLorry} from "../role-lorry";
import {RoleRoomTransfer} from "../role-lorry-roomtransfer";
import {RoleSigner} from "../role-signer";
import {RoleScout} from "../role-scout";
import {HelperNavigation} from "../../helper/helper-navigation";
import {RoleSniper} from "../role-sniper";
import {RolePowerHarvester} from "../role-power-harvester";
import {RoleLabRat} from "../role-labrat";
import {RoleBoostedUpgrader} from "../role-boosted-upgrader";
import {RoleHealer} from "../military/role-healer";
import {RoleRemoteFixedHarvester} from "../role-remote-fixed-harvester";
import {RoleRemoteLorry} from "../role-remote-lorry";
import {RoleAreaDefender} from "../military/role-area-defender";
import {RoleFlagAttacker} from "../military/role-flag-attacker";
import {RoleFlagHealer} from "../military/role-flag-healer";
import {RoleFlagAttackerMelee} from "../military/role-flag-attacker-melee";

export class RoleFactory {
    private roleHarvester: RoleHarvester;
    private roleUpgrader: RoleUpgrader;
    private roleBuilder: RoleBuilder;
    private roleRepairer: RoleRepairer;
    private roleRecharger: RoleRecharger;
    private roleLdHarvester: RoleLdHarvester;
    private roleClaimer: RoleClaimer;
    private roleTransfer: RoleTransfer;
    private roleRemoteBuilder: RoleRemoteBuilder;
    private roleDefender: RoleDefender;
    private roleAttacker: RoleAttacker;
    private roleMiner: RoleMiner;
    private roleFixedHarvester: RoleFixedHarvester;
    private roleLorry: RoleLorry;
    private roleRoomTransfer: RoleRoomTransfer;
    private roleSigner: RoleSigner;
    private roleScout: RoleScout;
    private roleSniper: RoleSniper;
    private rolePowerHarvester: RolePowerHarvester;
    private roleLabRat: RoleLabRat;
    private roleBoostedUpgrader: RoleBoostedUpgrader;
    private roleHealer: RoleHealer;
    private roleRemoteFixedHarvester: RoleRemoteFixedHarvester;
    private roleRemoteLorry: RoleRemoteLorry;
    private roleAreaDefender: RoleAreaDefender;
    private roleFlagAttacker: RoleFlagAttacker;
    private roleFlagAttackerMelee: RoleFlagAttackerMelee;
    private roleFlagHealer: RoleFlagHealer;

    private helperNavigation: HelperNavigation;

    constructor() {
        this.helperNavigation = new HelperNavigation();

        // this.roleHarvester = new RoleHarvester(this, this.helperNavigation);
        // this.roleUpgrader = new RoleUpgrader(this, this.helperNavigation);
        // this.roleBuilder = new RoleBuilder(this, this.helperNavigation);
        // this.roleRepairer = new RoleRepairer(this, this.helperNavigation);
        // this.roleRecharger = new RoleRecharger(this, this.helperNavigation);
        // this.roleLdHarvester = new RoleLdHarvester(this, this.helperNavigation);
        // this.roleClaimer = new RoleClaimer(this, this.helperNavigation);
        // this.roleTransfer = new RoleTransfer(this, this.helperNavigation);
        // this.roleRemoteBuilder = new RoleRemoteBuilder(this, this.helperNavigation);
        // this.roleDefender = new RoleDefender(this, this.helperNavigation);
        // this.roleAttacker = new RoleAttacker(this, this.helperNavigation);
        // this.roleMiner = new RoleMiner(this, this.helperNavigation);
        // this.roleFixedHarvester = new RoleFixedHarvester(this, this.helperNavigation);
        // this.roleLorry = new RoleLorry(this, this.helperNavigation);
        // this.roleRoomTransfer = new RoleRoomTransfer(this, this.helperNavigation);
        // this.roleSigner = new RoleSigner(this, this.helperNavigation);
        // this.roleScout = new RoleScout(this, this.helperNavigation);
        // this.roleSniper = new RoleSniper(this, this.helperNavigation);
        // this.rolePowerHarvester = new RolePowerHarvester(this, this.helperNavigation);
        // this.roleLabRat = new RoleLabRat(this, this.helperNavigation);
        // this.roleBoostedUpgrader = new RoleBoostedUpgrader(this, this.helperNavigation);
        // this.roleHealer = new RoleHealer(this, this.helperNavigation);
        // this.roleRemoteFixedHarvester = new RoleRemoteFixedHarvester(this, this.helperNavigation);
        // this.roleRemoteLorry = new RoleRemoteLorry(this, this.helperNavigation);
    }

    getRoleHarvester(): RoleHarvester {
        if (!this.roleHarvester) {
            this.roleHarvester = new RoleHarvester(this, this.helperNavigation);
        }
        return this.roleHarvester;
    }

    getRoleUpgrader() {
        if (!this.roleUpgrader) {
            this.roleUpgrader = new RoleUpgrader(this, this.helperNavigation);
        }
        return this.roleUpgrader;
    }

    getRoleBuilder() {
        if (!this.roleBuilder) {
            this.roleBuilder = new RoleBuilder(this, this.helperNavigation);
        }
        return this.roleBuilder;
    }

    getRoleRepairer() {
        if (!this.roleRepairer) {
            this.roleRepairer = new RoleRepairer(this, this.helperNavigation);
        }
        return this.roleRepairer;
    }

    getRoleRecharger() {
        if (!this.roleRecharger) {
            this.roleRecharger = new RoleRecharger(this, this.helperNavigation);
        }
        return this.roleRecharger;
    }

    getRoleRemoteHarvester() {
        if (!this.roleLdHarvester) {
            this.roleLdHarvester = new RoleLdHarvester(this, this.helperNavigation);
        }
        return this.roleLdHarvester;
    }

    getRoleClaimer() {
        if (!this.roleClaimer) {
            this.roleClaimer = new RoleClaimer(this, this.helperNavigation);
        }
        return this.roleClaimer;
    }

    getRoleTransfer() {
        if (!this.roleTransfer) {
            this.roleTransfer = new RoleTransfer(this, this.helperNavigation);
        }
        return this.roleTransfer;
    }

    getRoleRemoteBuilder() {
        if (!this.roleRemoteBuilder) {
            this.roleRemoteBuilder = new RoleRemoteBuilder(this, this.helperNavigation);
        }
        return this.roleRemoteBuilder;
    }

    getRoleDefender() {
        if (!this.roleDefender) {
            this.roleDefender = new RoleDefender(this, this.helperNavigation);
        }
        return this.roleDefender;
    }

    getRoleAttacker() {
        if (!this.roleAttacker) {
            this.roleAttacker = new RoleAttacker(this, this.helperNavigation);
        }
        return this.roleAttacker;
    }

    getRoleMiner() {
        if (!this.roleMiner) {
            this.roleMiner = new RoleMiner(this, this.helperNavigation);
        }
        return this.roleMiner;
    }

    getRoleFixedHarvester() {
        if (!this.roleFixedHarvester) {
            this.roleFixedHarvester = new RoleFixedHarvester(this, this.helperNavigation);
        }
        return this.roleFixedHarvester;
    }

    getRoleLorry() {
        if (!this.roleLorry) {
            this.roleLorry = new RoleLorry(this, this.helperNavigation);
        }
        return this.roleLorry;
    }

    getRoleRoomTransfer() {
        if (!this.roleRoomTransfer) {
            this.roleRoomTransfer = new RoleRoomTransfer(this, this.helperNavigation);
        }
        return this.roleRoomTransfer;
    }

    getRoleSigner() {
        if (!this.roleSigner) {
            this.roleSigner = new RoleSigner(this, this.helperNavigation);
        }
        return this.roleSigner;
    }

    getRoleScout() {
        if (!this.roleScout) {
            this.roleScout = new RoleScout(this, this.helperNavigation);
        }
        return this.roleScout;
    }

    getRoleSniper() {
        if (!this.roleSniper) {
            this.roleSniper = new RoleSniper(this, this.helperNavigation);
        }
        return this.roleSniper;
    }

    getRolePowerHarvester() {
        if (!this.rolePowerHarvester) {
            this.rolePowerHarvester = new RolePowerHarvester(this, this.helperNavigation);
        }
        return this.rolePowerHarvester;
    }

    getRoleLabRat() {
        if (!this.roleLabRat) {
            this.roleLabRat = new RoleLabRat(this, this.helperNavigation);
        }
        return this.roleLabRat;
    }

    getRoleBoostedUpgrader() {
        if (!this.roleBoostedUpgrader) {
            this.roleBoostedUpgrader = new RoleBoostedUpgrader(this, this.helperNavigation);
        }
        return this.roleBoostedUpgrader;
    }

    getRoleHealer() {
        if (!this.roleHealer) {
            this.roleHealer = new RoleHealer(this, this.helperNavigation);
        }
        return this.roleHealer;
    }

    getRoleRemoteLorry() {
        if (!this.roleRemoteLorry) {
            this.roleRemoteLorry = new RoleRemoteLorry(this, this.helperNavigation);
        }
        return this.roleRemoteLorry;
    }

    getRoleRemoteFixedHarvester() {
        if (!this.roleRemoteFixedHarvester) {
            this.roleRemoteFixedHarvester = new RoleRemoteFixedHarvester(this, this.helperNavigation);
        }
        return this.roleRemoteFixedHarvester;
    }

    getRoleRoleAreaDefender() {
        if (!this.roleAreaDefender) {
            this.roleAreaDefender = new RoleAreaDefender(this, this.helperNavigation);
        }
        return this.roleAreaDefender;
    }

    getRoleFlagAttacker() {
        if (!this.roleFlagAttacker) {
            this.roleFlagAttacker = new RoleFlagAttacker(this, this.helperNavigation);
        }
        return this.roleFlagAttacker;
    }

    getRoleFlagAttackerMelee() {
        if (!this.roleFlagAttackerMelee) {
            this.roleFlagAttackerMelee = new RoleFlagAttackerMelee(this, this.helperNavigation);
        }
        return this.roleFlagAttackerMelee;
    }

    getRoleFlagHealer() {
        if (!this.roleFlagHealer) {
            this.roleFlagHealer = new RoleFlagHealer(this, this.helperNavigation);
        }
        return this.roleFlagHealer;
    }
}
