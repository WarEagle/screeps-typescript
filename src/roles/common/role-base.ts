import {RoleFactory} from "./role-factory";
import {HelperNavigation} from "../../helper/helper-navigation";
import {Profiler} from "../../profiler";
import {MyCreep} from "../../types/my-creep";
import {SpawnerStatus} from "../../spawning/spawner-status";
import {RoomControllerCommon} from "../../room/room-controller-common";

export interface SpawnParameter {
    spawner: StructureSpawn;
    spawnerStatus: SpawnerStatus;
    roomController: RoomControllerCommon;
    maxEnergyAllowed: number;
    filteredCreeps: Creep[];
}

export abstract class BaseRole {

    private whiteListMilitary = ["WarEagle", "ganger-m"];
    protected filterWhitelistMilitary = {
        filter: (target: Creep) => this.whiteListMilitary.indexOf(target.owner.username) === -1
    };

    protected isSubRole: boolean = false;

    constructor(protected roleFactory: RoleFactory, protected helperNavigation: HelperNavigation) {
    }

    protected sayRoleIcon(creep: MyCreep, time: number): void {
        if (!this.isSubRole && Game.time % 10 === time) {
            creep.say(this.icon(), false);
        }
    }

    abstract run(creep: MyCreep): void;

    abstract icon(): string;

    abstract roleName(): string;

    abstract roleShortName(): string;

    abstract callReplacementRole(creep?: MyCreep): void;

    abstract internalSpawnIfNeeded(spawnParameter: SpawnParameter): void;

    public spawnIfNeeded(givenSpawnParameter: SpawnParameter, groupedCreeps: any): void {
        let spawnParameter: SpawnParameter = {
            spawner: givenSpawnParameter.spawner,
            spawnerStatus: givenSpawnParameter.spawnerStatus,
            roomController: givenSpawnParameter.roomController,
            maxEnergyAllowed: givenSpawnParameter.maxEnergyAllowed,
            filteredCreeps: groupedCreeps[this.roleName()],
        };
        this.internalSpawnIfNeeded(spawnParameter);
    }

    public runAsSubRole(creep: MyCreep) {
        this.isSubRole = true;
        this.run(creep);
        this.isSubRole = false;
    }

    checkForMissingRoad(creep: MyCreep): void {
        if (!(creep.fatigue > 0 && (creep.memory.role !== "ldHarvester" && creep.memory.role === "harvester"))) {
            return;
        }

        if (!creep.room.controller) {
            return;
        }

        if (!creep.room.controller.my) {
            return;
        }

        if (creep.room.controller.level < 4) {
            return;
        }
        creep.room.createConstructionSite(creep.pos.x, creep.pos.y, STRUCTURE_ROAD);
    }

    protected harvestFromClosestResource(creep: any): boolean {
        // collect energy
        // let harvesterMeasurer = new CpuMeasurer("\t" + creep.memory.role + " loading " + creep.name);
        // harvesterMeasurer.start();
        let globalRc: boolean = false;
        let source: any = undefined;

        if (creep.memory.desiredSource) {
            source = Game.getObjectById(creep.memory.desiredSource);
            if (!source) {
                delete creep.memory.desiredSource;
            }
        } else {
            // must be findClosestByPath otherwise the creep doesn't move to the next source if the current one is blocked
            source = creep.pos.findClosestByRange(FIND_SOURCES_ACTIVE, {
                filter: (x: any) =>
                    (
                        x.energy > 0
                        && x.room === creep.room
                    ),
            });
            if (source) {
                creep.memory.desiredSource = source.id;
            } else {
                delete creep.memory.desiredSource;
            }
        }
        if (source) {
            let rc = creep.harvest(source);
            if (rc === ERR_NOT_IN_RANGE) {
                creep.moveTo(source);
            }
            if (rc === ERR_NO_PATH) {
                delete creep.memory.desiredSource;
            }

            if (rc === ERR_NOT_ENOUGH_RESOURCES) {
                delete creep.memory.desiredSource;
            }

            globalRc = rc === OK;
        }
        // harvesterMeasurer.stop();
        return globalRc;
    }

    protected needsBoost(creep: MyCreep): boolean {
        if (!creep.memory.isBoosted === true && creep.memory.lab) {
            // console.log(creep.name);
            let lab = <Lab>Game.getObjectById(creep.memory.lab);
            if (creep.pos.inRangeTo(lab, 1)) {
                let rc = lab.boostCreep(creep.getOriginalCreep());
                if (rc === ERR_NOT_IN_RANGE) {
                    creep.moveTo(lab);
                } else if (rc === OK) {
                    creep.memory.isBoosted = true;
                    return false;
                } else {
                    console.log("got strange rc for creep " + creep.name + " while boosting rc=" + rc);
                }
            } else {
                let moveToOpts = <MoveToOpts> {reusePath: 25, maxOps: 200000, maxRooms: 20};
                creep.moveTo(lab, moveToOpts);
                // console.log("rc=" + rc);
            }
            return true;
        }
        return false;
    }

    protected getCreepsWithOwnRole() {
        //0.650sec
        // filter before, so the next queries are faster
        // let possibleRelevantCreeps = _.filter(Game.creeps, (creep: MyCreep) => {
        //         return (creep.memory.role === this.roleName());
        //     }
        // );
        //

        //0.430sec
        // let possibleRelevantCreeps: Creep[] = [];
        // for (let name in Game.creeps) {
        //     if (Game.creeps[name].memory.role === this.roleName()) {
        //         possibleRelevantCreeps.push(Game.creeps[name]);
        //     }
        // }
        //1.4sec
        // let possibleRelevantCreeps: Creep[] = [];
        // _.forEach(Game.creeps, (crp) => {
        //     if (crp.memory.role === this.roleName()) {
        //         possibleRelevantCreeps.push(crp);
        //     }
        // });
        //
        let possibleRelevantCreeps: Creep[] = [];
        for (let name in Game.creeps) {
            if (Game.creeps[name].memory.role === this.roleName()) {
                possibleRelevantCreeps.push(Game.creeps[name]);
            }
        }

        return possibleRelevantCreeps;
    }


}
Profiler.registerClass(BaseRole, BaseRole.name);
