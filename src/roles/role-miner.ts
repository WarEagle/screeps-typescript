import {BaseRole, SpawnParameter} from "./common/role-base";
import {Profiler} from "../profiler";
import {ScreepUtils} from "../helper/screep-utils";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {RoomMemory} from "../room/room-memory";
import {MyCreep} from "../types/my-creep";

export class RoleMiner extends BaseRole {

    public icon(): string {
        return "⛏";
    }

    public roleName(): string {
        return "miner";
    }

    public roleShortName(): string {
        return "MI";
    }

    public callReplacementRole(): void {
    }

    public run(creep: MyCreep) {
        this.sayRoleIcon(creep, 4);

        if (creep.memory.working === true && (_.sum(creep.carry) === 0)) {
            creep.memory.working = false;
        }

        //loop on resource_all?
        if (creep.memory.working === false && (_.sum(creep.carry) >= creep.carryCapacity)) {
            creep.memory.working = true;
        }

        //come back when end is near
        if (creep.ticksToLive < 75) {
            creep.memory.working = true;
        }

        if (creep.memory.working === true) {
            if (this.helperNavigation.unloadingMineralsToTerminal(creep)) {
            } else if (this.helperNavigation.unloadingMineralsToStorage(creep)) {
            }
        } else {
            //collect minerals
            let source: any = creep.pos.findClosestByPath(FIND_MINERALS, {
                    filter: (x: any) => (
                    x.room === creep.room)
                })
                ;
            if (source) {
                //console.log(creep.harvest(source));
                if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(source);
                }
            } else {
                console.log(creep.name + " no extractor found.");
            }
        }
    }

    internalSpawnIfNeeded(spawnParameter: SpawnParameter): void {
        let room = spawnParameter.spawner.room;
        let roomMemory = <RoomMemory> room.memory;

        let currCountTotal = ScreepUtils.countArray(spawnParameter.filteredCreeps, (c) => (
            c.room.name === spawnParameter.spawner.room.name
        ));

        let extractorLocation;
        if (roomMemory.extractorId) {
            extractorLocation = roomMemory.extractorId;
        } else {
            extractorLocation = spawnParameter.spawner.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                    filter: (x: Structure) => (
                        x.pos.roomName === spawnParameter.spawner.pos.roomName
                        && x.structureType === STRUCTURE_EXTRACTOR
                    )
                }
            );
            if (extractorLocation) {
                roomMemory.extractorId = (<StructureExtractor> extractorLocation).id;
            }

        }

        if (!extractorLocation) {
            return;
        }

        //TODO store this in roomMemory once found
        let mineralLocation;
        if (roomMemory.mineralId) {
            mineralLocation = Game.getObjectById(roomMemory.mineralId);
        } else {
            mineralLocation = spawnParameter.spawner.pos.findClosestByRange(FIND_MINERALS, {
                    filter: (x: Mineral) => (
                        x.pos.roomName === spawnParameter.spawner.pos.roomName
                    )
                }
            );
            if (mineralLocation) {
                roomMemory.mineralId = (<Mineral> mineralLocation).id;
            }
        }

        let targetCnt = extractorLocation ? 1 : 0;

        if (!mineralLocation || (<Mineral> mineralLocation).mineralAmount <= 0) {
            targetCnt = 0;
        }

        if (targetCnt === 0) {
            return;
        }
        spawnParameter.spawnerStatus.addToReport(this.roleShortName(), currCountTotal, targetCnt);
        if (spawnParameter.spawnerStatus.hasSpawned) {
            return;
        }

        // console.log("currCountTotal=" + currCountTotal);
        // console.log("extractorLocation=" + (extractorLocation ? 1 : 0));

        if (currCountTotal >= (extractorLocation ? 1 : 0)) {
            // nothing to do
            return;
        }

        this.spawn(spawnParameter.spawner, <Mineral> mineralLocation, spawnParameter.maxEnergyAllowed);
        spawnParameter.spawnerStatus.hasSpawned = true;
    }

    private spawn(spawner: StructureSpawn, mineralLocation: Mineral, maxEnergyCapacityAvailable: number): boolean {
        let components = [];

        //TODO scale depending on the mineral
        let nrComponents = Math.floor(maxEnergyCapacityAvailable / 450);

        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
            components.push(WORK);
            components.push(WORK);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
            components.push(MOVE);
        }

        // let pos = new RoomPosition(x, y, spawner.room.name);
        // // console.log(JSON.stringify(pos));
        // let source: any = pos.findClosestByRange(FIND_MINERALS);
        // // console.log(JSON.stringify(source));

        let rc = SpawnerWrapper.createCreep(spawner, components, this, {
            role: this.roleName(),
            sourceId: mineralLocation.id,
            working: false,
            x: mineralLocation.pos.x,
            y: mineralLocation.pos.y,
        });
        return _.isString(rc);
    }

}
Profiler.registerClass(RoleMiner, RoleMiner.name);
