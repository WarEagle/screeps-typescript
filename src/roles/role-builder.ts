import {BaseRole} from "./common/role-base";
import {SpawnerStatus} from "../spawning/spawner-status";
import {SpawnerController} from "../spawning/spawner-controller";
import {Profiler} from "../profiler";
import {ScreepCache} from "../helper/screep-cache";
import {MyCreep} from "../types/my-creep";

export class RoleBuilder extends BaseRole {

    public icon(): string {
        return "⛫";
    }

    public roleName(): string {
        return "builder";
    }

    public roleShortName(): string {
        return "BU";
    }

    public callReplacementRole(creep: MyCreep): void {
        //perform upgrade
        // console.log("callReplacementRole " + creep.name + " / " + creep.pos.roomName + " builder");
        ScreepCache.setRoomDone(creep.pos.roomName, this.roleName());
        if (_.any(creep.room.find(FIND_CREEPS), function (entry: Creep) {
                return entry.room.name === creep.room.name
                    && entry.memory
                    && entry.memory.role
                    && entry.memory.role === "repairer";
            })
        ) {
            this.roleFactory.getRoleRepairer().callReplacementRole(creep);
        } else {
            this.roleFactory.getRoleRepairer().runAsSubRole(creep);
        }
        // this.roleFactory.getRoleUpgrader().runAsSubRole(creep);
    }

    public run(creep: MyCreep) {
        // if (creep.name.length > 0){
        //     return;
        // }

        this.sayRoleIcon(creep, 5);

        if (creep.memory.working === true && creep.carry.energy === 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working === false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.memory.working !== false && creep.memory.working !== true) {
            creep.memory.working = false;
        }

        if (ScreepCache.isRoomDone(creep.pos.roomName, this.roleName())) {
            // console.log(creep.name + " skipping role builder!");
            this.callReplacementRole(creep);
        }

        if (creep.memory.working === true) {
            if (!this.isSubRole) {
                //safety because builders sometimes leave their rooms
                if (this.helperNavigation.moveToRoom(creep, creep.memory.spawnedIn)) {
                    this.performWorking(creep);
                }
            } else {
                this.performWorking(creep);
            }
        } else {
            this.performEnergyCollection(creep);
        }
    }

    private performEnergyCollection(creep: MyCreep) {
        // get energy
        if (this.helperNavigation.takeEnergyFromMyStorage(creep)) {
        } else if (this.helperNavigation.takeEnergyFromMyTerminal(creep)) {
        } else if (this.helperNavigation.takeEnergyFromMyContainer(creep)) {
        } else {
            //harvest
            const source: any = creep.pos.findClosestByRange(FIND_SOURCES_ACTIVE, {
                filter: (x: any) => (
                    x.energy > 0
                    && x.room.name === creep.room.name
                )

            });
            if (source) {
                if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(source);
                }
            }
        }
    }

    private performWorking(creep: MyCreep) {
        const constructionSite: any = creep.pos.findClosestByPath(FIND_MY_CONSTRUCTION_SITES, {
            filter: (x: any) => (
                x.pos.roomName === creep.pos.roomName
            ), maxRooms: 0
        });
        // console.log(" 2-> " + creep.name +  " " + JSON.stringify(constructionSite));
        if (constructionSite) {
            // console.log(" 3-> " + creep.name);
            // console.log(" 3-> " + creep.name+ " "+creep.build(constructionSite) );
            let rc = creep.build(constructionSite);
            if (rc === ERR_RCL_NOT_ENOUGH) {
                // console.log("ERROR RCL too low for builder " + creep.name);
            } else if (rc === ERR_NOT_IN_RANGE) {
                creep.moveTo(constructionSite);
            }
        } else {
            // console.log("run repairer by " + creep.name + " for " + creep.pos.roomName + " in " + this.roleName());
            // console.log("pos= " + creep.pos);
            // console.log("constructionSite= " + JSON.stringify(constructionSite));
            // console.log("constructionSite= " + (constructionSite));
            this.callReplacementRole(creep);
        }
    }

    public spawnIfNeededOldWay(spawner: StructureSpawn, spawnerStatus: SpawnerStatus, currBuilder: number, targetBuilder: number, maxEnergyCapacityAvailable: number) {
        // let rc: number;
        // let tmp: number = 100;
        //
        // let workableFields = WorldInfoCollector.getCntWorkableFieldsSource(spawner.room.name);
        // if (workableFields !== -1) {
        //     tmp = workableFields - currFixedHarvester;
        // }
        // // tmp = Math.min(currFixedHarvester, tmp);
        // rc = Math.max(targetBuilder, tmp);
        // //rc = Math.min(currFixedHarvester, rc);
        // //rc = Math.max(rc, 0);
        // rc = Math.min(rc, targetBuilder);
        //
        // SpawnerController.spawnMax(this.roleName(), spawner, spawnerStatus, currBuilder, rc);
        SpawnerController.spawnMax(this.roleName(), spawner, spawnerStatus, currBuilder, targetBuilder, this, maxEnergyCapacityAvailable);
    }

    internalSpawnIfNeeded(): void {
        //TODO
        throw new SyntaxError("This method isn't implemented");
    }

}
Profiler.registerClass(RoleBuilder, RoleBuilder.name);
