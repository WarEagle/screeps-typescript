import {BaseRole} from "./roles/common/role-base";
import {WorldInfoCollector} from "./statistics/world-info";
import {SpawnerStatus} from "./spawning/spawner-status";
import {RoleFactory} from "./roles/common/role-factory";
import {RoomRunner} from "./room-runner";
import {SpawnerController} from "./spawning/spawner-controller";
import {GlobalRoomsController} from "./room/global-rooms-controller";
import {MaintenanceLink} from "./maintenance.links";
import {Profiler} from "./profiler";
import {RemoteMining} from "./helper/remote-mining";
import {RoleHealer} from "./roles/military/role-healer";
import {HelperNavigation} from "./helper/helper-navigation";
import {MyCreep} from "./types/my-creep";
import {GrafanaCollector} from "./statistics/grafana-collector";
import {ScreepUtils} from "./helper/screep-utils";
import {ResourceDistributor} from "./room/resource-distributor";

export class MainLogic {

    public run() {
        // if (Game.time % 3 === 0){
        //     Game.profiler.reset();
        // }
        // if (Game.time % 3 === 1){
        //     Game.profiler.profile(1, "RoleRemoteBuilder.run");
        // }
        //

        let worldInfoCollector = new WorldInfoCollector();
        worldInfoCollector.collectInfos();
        this.cleanMemory();

        let statusCollection: SpawnerStatus[] = [];
        let roleFactory = this.initRoleFactory();
        let spawnerController = this.initSpawnerController();

        new RemoteMining().fillMissingVariables();

        this.runRooms(statusCollection, spawnerController, roleFactory);
        SpawnerStatus.reportOverview(statusCollection);

        GrafanaCollector.addDetailedCpu("MainLogic runCreeps", () => {
            this.runCreeps(roleFactory);
        });
    }

    private initSpawnerController() {
        return new SpawnerController();
    }

    private initRoomRunner() {
        return new RoomRunner();
    }

    private initLinkRunner() {
        return new MaintenanceLink();
    }

    private initRoleFactory() {
        return new RoleFactory();
    }

    private cleanMemory() {
        // Check memory for null or out of bounds custom objects
        if (!Memory.uuid || Memory.uuid > 100) {
            Memory.uuid = 0;
        }
        this.cleanCreeps();
    }

    private cleanCreeps() {
        for (let name in Memory.creeps) {
            if (!Game.creeps[name]) {
                delete Memory.creeps[name];
                // console.log("Clearing non-existing creep memory: ", name);
            }
        }
    }

    private runRooms(statusCollection: SpawnerStatus[], spawnerController: SpawnerController, roleFactory: RoleFactory) {

        new ResourceDistributor().distribute();

        let roomRunner = this.initRoomRunner();
        let linkRunner = this.initLinkRunner();

        let roomsWithController = ScreepUtils.roomsWithController();
        for (let roomCounter in roomsWithController) {
            let room = roomsWithController[roomCounter];
            // console.log(JSON.stringify(roomCounter));
            // console.log(JSON.stringify(room));
            // console.log(JSON.stringify(room));
            let roomController = new GlobalRoomsController().getController(room);
            roomRunner.manage(room, roomController, statusCollection, spawnerController, linkRunner, roleFactory);

        }
    }

    private runCreeps(roleFactory: RoleFactory) {
        // Letting the creeps do their jobs
        for (let name in Game.creeps) {
            this.runSingleCreep(name, roleFactory);
        }
    }

    private runSingleCreep(name: string, roleFactory: RoleFactory): void {
        let creep = Game.creeps[name];
        if (creep.spawning === true) {
            return;
        }
        let myCreep = new MyCreep(creep);
        this.performCommonTasks(roleFactory, myCreep);

        GrafanaCollector.addDetailedCpu("Creep Role " + myCreep.memory.role, () => {
            let role = this.findMatchingRole(myCreep, roleFactory);
            this.runCreepRole(role, myCreep);
        });

    }

    private findMatchingRole(creep: MyCreep, roleFactory: RoleFactory) {
        let role: BaseRole;

        switch (creep.memory.role) {
            case roleFactory.getRoleRemoteFixedHarvester().roleName():
                role = roleFactory.getRoleRemoteFixedHarvester();
                break;
            case roleFactory.getRoleRemoteLorry().roleName():
                role = roleFactory.getRoleRemoteLorry();
                break;
            case roleFactory.getRoleHarvester().roleName():
                role = roleFactory.getRoleHarvester();
                break;
            case roleFactory.getRoleBuilder().roleName():
                role = roleFactory.getRoleBuilder();
                break;
            case roleFactory.getRoleFixedHarvester().roleName():
                role = roleFactory.getRoleFixedHarvester();
                break;
            case roleFactory.getRoleUpgrader().roleName():
                role = roleFactory.getRoleUpgrader();
                break;
            case roleFactory.getRoleLorry().roleName():
                role = roleFactory.getRoleLorry();
                break;
            case roleFactory.getRoleRemoteBuilder().roleName():
                role = roleFactory.getRoleRemoteBuilder();
                break;
            case roleFactory.getRoleClaimer().roleName():
                role = roleFactory.getRoleClaimer();
                break;
            case roleFactory.getRoleDefender().roleName():
                role = roleFactory.getRoleDefender();
                break;
            case roleFactory.getRoleMiner().roleName():
                role = roleFactory.getRoleMiner();
                break;
            case roleFactory.getRoleRoomTransfer().roleName():
                role = roleFactory.getRoleRoomTransfer();
                break;
            case roleFactory.getRoleRepairer().roleName():
                role = roleFactory.getRoleRepairer();
                break;
            case roleFactory.getRoleRecharger().roleName():
                role = roleFactory.getRoleRecharger();
                break;
            case roleFactory.getRoleLabRat().roleName():
                role = roleFactory.getRoleLabRat();
                break;
            case roleFactory.getRoleSniper().roleName():
                role = roleFactory.getRoleSniper();
                break;
            case roleFactory.getRoleRoleAreaDefender().roleName():
                role = roleFactory.getRoleRoleAreaDefender();
                break;
            case roleFactory.getRolePowerHarvester().roleName():
                role = roleFactory.getRolePowerHarvester();
                break;
            case roleFactory.getRoleBoostedUpgrader().roleName():
                role = roleFactory.getRoleBoostedUpgrader();
                break;
            case roleFactory.getRoleHealer().roleName():
                role = roleFactory.getRoleHealer();
                break;
            case roleFactory.getRoleRemoteHarvester().roleName():
                role = roleFactory.getRoleRemoteHarvester();
                break;
            case roleFactory.getRoleTransfer().roleName():
                role = roleFactory.getRoleTransfer();
                break;
            case roleFactory.getRoleAttacker().roleName():
                role = roleFactory.getRoleAttacker();
                break;
            case roleFactory.getRoleSigner().roleName():
                role = roleFactory.getRoleSigner();
                break;
            case roleFactory.getRoleScout().roleName():
                role = roleFactory.getRoleScout();
                break;
            case roleFactory.getRoleFlagAttacker().roleName():
                role = roleFactory.getRoleFlagAttacker();
                break;
            case roleFactory.getRoleFlagHealer().roleName():
                role = roleFactory.getRoleFlagHealer();
                break;
            case roleFactory.getRoleFlagAttackerMelee().roleName():
                role = roleFactory.getRoleFlagAttackerMelee();
                break;
            default:
                console.log("UNKNOWN ROLE! " + creep.name + " (" + creep.memory.role + ")");
                // role = roleFactory.roleHealer;
                role = roleFactory.getRoleHarvester();
                role = new RoleHealer(roleFactory, new HelperNavigation());
        }
        return role;
    }

    private runCreepRole(role: BaseRole, creep: MyCreep) {
        role.run(creep);
    }

    private performCommonTasks(roleFactory: RoleFactory, creep: MyCreep) {
        if (Game.time % 3 === 0) {
            // save some cpu
            //FIXME annoying! it works too good
            if (1 < 2) {
                roleFactory.getRoleHarvester().checkForMissingRoad(creep);
            }
        }
    }
}

Profiler.registerClass(MainLogic, MainLogic.name);
