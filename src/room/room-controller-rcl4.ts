import {RoomControllerCommon} from "./room-controller-common";
import {BaseDesigner} from "./base-designer";
import {WorldInfoCollector} from "../statistics/world-info";
import {Profiler} from "../profiler";

export class RoomControllerRcl4 extends RoomControllerCommon {
    constructor(protected room: Room, forStatistics?: boolean) {
        super(room, forStatistics);
    }

    public getMaxEnergyAllowed(): number {
        return Math.min(1600, this.room.energyCapacityAvailable);
    }

    public getTargetCntRepairer(): number {
        return 1;
    }

    public getTargetHarvester(): number {
        // TODO depend on current avail energy capacity
        // if (this.room.memory.supportMode){
        //     return 1;
        // }
        let tmp;
        tmp = Math.min(3, this.roomMemory.cntSources);
        if (this.room.energyCapacityAvailable >= 1000) {
            tmp = this.roomMemory.cntSources;
        } else if (this.room.energyCapacityAvailable >= 700) {
            tmp = this.roomMemory.cntSources + 1;
        }
        tmp -= this.roomMemory.cntActiveFixedHarvesters;
        return Math.max(0, tmp);
    }

    public getTargetUpgrader(): number {
        return 1;
    }

    public getTargetBuilder(): number {
        if (this.room.memory.attackMode) {
            return 1;
        }
        if (this.room.memory.supportMode) {
            return 1;
        }
        let tmp;
        // TODO make this depending on number of construction sites
        if (this.room.energyCapacityAvailable >= 1000) {
            tmp = Math.min(8, this.roomMemory.cntSources);
        } else if (this.room.energyCapacityAvailable >= 700) {
            tmp = Math.min(8, this.roomMemory.cntSources * 3);
        } else {
            tmp = Math.min(8, this.roomMemory.cntSources * 3);
        }

        let workableFields = WorldInfoCollector.getCntWorkableFieldsSource(this.room.name);
        tmp = Math.min(workableFields, tmp);

        return tmp;
    }

    public getTargetRecharger(): number {
        return this.notAtThisLevel;
    }

    public getTargetCntLorries(): number {
        return this.roomMemory.cntActiveFixedHarvesters;
    }

    public tasksForUpgrade() {
        this.roomMemory.requiredRclSubtask = 1;
    }

    tasksForNextUpgradeSubtask() {
        let designer = new BaseDesigner();
        designer.buildBaseRcl(this.room);
        delete this.roomMemory.requiredRclSubtask;
    }

}
Profiler.registerClass(RoomControllerRcl4, RoomControllerRcl4.name);
