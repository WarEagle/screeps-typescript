import {RoomControllerCommon} from "./room-controller-common";
import {BaseDesigner} from "./base-designer";
import {ScreepUtils} from "../helper/screep-utils";
import {WorldInfoCollector} from "../statistics/world-info";
import {Profiler} from "../profiler";

export class RoomControllerRcl3 extends RoomControllerCommon {
    constructor(protected room: Room, forStatistics?: boolean) {
        super(room, forStatistics);
    }

    public getMaxEnergyAllowed(): number {
        return Math.min(1600, this.room.energyCapacityAvailable);
    }

    public getTargetCntRepairer(): number {
        return this.notAtThisLevel;
    }

    public getTargetHarvester(): number {
        // TODO depend on current avail energy capacity
        // if (this.room.memory.supportMode){
        //     return 1;
        // }
        if (this.room.energyCapacityAvailable >= 700) {
            return this.roomMemory.cntSources + 1;
        }
        return Math.min(3, this.roomMemory.cntSources);
    }

    public getTargetUpgrader(): number {
        return 1;
    }

    public getTargetBuilder(): number {
        if (this.room.memory.attackMode) {
            return 1;
        }
        if (this.room.memory.supportMode) {
            return 1;
        }
        let tmp;
        // TODO make this depending on number of construction sites
        // TODO depend on current avail energy capacity
        if (this.room.energyCapacityAvailable >= 700) {
            tmp = Math.min(8, this.roomMemory.cntSources * 2);
        } else {
            tmp = Math.min(8, this.roomMemory.cntSources * 3);
        }
        let supportingRemoteBuilder = ScreepUtils.count(Game.creeps, (c) => (
            c.memory.role === "remoteBuilder"
            && c.memory.targetRoomName === this.room.name
        ));
        tmp -= supportingRemoteBuilder * 2;
        // console.log("tmp=" + tmp);

        tmp = Math.max(1, tmp);

        let workableFields = WorldInfoCollector.getCntWorkableFieldsSource(this.room.name);
        tmp = Math.min(workableFields, tmp);
        // console.log("workableFields=" + workableFields);
        // console.log("tmp=" + tmp);
        return tmp;
    }

    public getTargetRecharger(): number {
        return this.notAtThisLevel;
    }

    public getTargetCntLorries(): number {
        return this.roomMemory.cntActiveFixedHarvesters;
    }

    public tasksForUpgrade() {
        new BaseDesigner().buildBaseRcl(this.room);
        this.roomMemory.requiredRclSubtask = 1;
    }

    tasksForNextUpgradeSubtask() {
        let designer = new BaseDesigner();
        designer.buildAllFixedHarvesterContainers(this.room.name);

        // let sources = this.room.find(FIND_SOURCES, {
        //     filter: (entry: Source) => {
        //         return entry.room.name === this.room.name;
        //     }
        // });
        // let spawners = this.room.find(FIND_MY_SPAWNS, {
        //     filter: (entry: StructureSpawn) => {
        //         return entry.room.name === this.room.name;
        //     }
        // });
        // new BaseDesigner().buildRoad(this.room, (<Spawn> spawners[0]).pos, (<any> this.room.controller).pos);
        // for (let source of sources) {
        //     new BaseDesigner().buildRoad(this.room, (<Spawn> spawners[0]).pos, (<Source> source).pos);
        // }

        delete this.roomMemory.requiredRclSubtask;
    }

}
Profiler.registerClass(RoomControllerRcl3, RoomControllerRcl3.name);
