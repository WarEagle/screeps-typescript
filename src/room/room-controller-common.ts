import {RoomMemory} from "./room-memory";
import {Profiler} from "../profiler";

export abstract class RoomControllerCommon {
    protected notAtThisLevel: number = 0;
    protected room: Room;
    protected roomMemory: RoomMemory;
    // For SIM
    // private refreshTime: number = 10;
    private refreshTime: number = 100;

    constructor(room: Room, forStatistics?: boolean) {
        this.room = room;
        this.roomMemory = room.memory;
        if (!forStatistics && Game.time % 10 === 0) {
            // safe CPU
            this.updateRoomVariables();
        }
    }

    abstract tasksForUpgrade(): void;

    abstract getMaxEnergyAllowed(): number;

    abstract tasksForNextUpgradeSubtask(): void;

    private updateRoomVariables(): void {
        // if (this.room.name) {
        //     return;
        // }

        // console.log("updateRoomVariables " + this.room.name);
        let tmpTick = 0;
        // console.log("this.roomMemory.lastRcl=" + this.roomMemory.lastRcl);
        // console.log("this.room.controller=" + this.room.controller);
        // console.log("this.room.controller.level=" + (this.room.controller ? this.room.controller.level : "---"));

        if (!this.roomMemory.lastRcl || (this.room.controller && this.roomMemory.lastRcl < this.room.controller.level)) {
            // console.log("performUpgrade" + this.room.name);
            this.performUpgrade();
            tmpTick = Game.time;
        }

        if (!this.roomMemory.lastRefresh || ((Game.time - this.roomMemory.lastRefresh) >= this.refreshTime)) {
            // console.log("performRefresh" + this.room.name);
            this.performRefresh();
        }

        if (
            this.roomMemory.requiredRclSubtask
            && this.roomMemory.requiredRclSubtask > 0
            && this.room.controller
            && this.roomMemory.cntConstructionSites === 0
            && this.roomMemory.lastRcl === this.room.controller.level
            && Game.time !== tmpTick
        ) {
            //this must not called the same tick an upgrade was done, because then the numbers aren't correct
            console.log("tasksForNextUpgradeSubtask " + this.room.name);
            this.tasksForNextUpgradeSubtask();
        }

        // console.log("this.roomMemory.lastRefresh=" + this.roomMemory.lastRefresh);
        // console.log("this.refreshTime=" + this.refreshTime);
        // console.log("Game.time - this.roomMemory.lastRefresh=" + (Game.time - this.roomMemory.lastRefresh));

        // console.log("updateRoomVariables");
    }

    private performUpgrade() {
        // console.log("perform room upgrade");
        // TODO
        if (!this.room.controller) {
            return;
        }
        this.updateMilestones();
        this.tasksForUpgrade();
        this.roomMemory.lastRcl = this.room.controller.level;
        // set this, so the next refresh is performed one tick after creating new construction sites
        this.roomMemory.lastRefresh = Game.time - this.refreshTime + 1;
    }

    private performRefresh() {
        // console.log("performRefresh");
        this.refreshMemory();
        this.refreshFixedHarvesters();
        this.refreshLinks();
        this.roomMemory.lastRefresh = Game.time;
    }

    public abstract getTargetHarvester(): number;

    public abstract getTargetUpgrader(): number;

    public abstract getTargetBuilder(): number;

    public abstract getTargetRecharger(): number;

    public abstract getTargetCntLorries(): number;

    public abstract getTargetCntRepairer(): number;

    private refreshMemory() {
        this.roomMemory.cntSources = this.room.find(FIND_SOURCES).length;
        this.roomMemory.cntConstructionSites = this.room.find(FIND_MY_CONSTRUCTION_SITES).length;
        // console.log("refreshMemory");
        // console.log("cntConstructionSites=" + this.roomMemory.cntConstructionSites);
    }

    private refreshLinks() {
        // console.log("refreshLinks");
        if (this.roomMemory.receivingLink) {
            return;
        }

        let spawn: any = this.room.find(FIND_MY_SPAWNS)[0];
        if (!spawn) {
            console.log("refreshLinks problem in " + this.room.name);
            return;
        }
        let found = spawn.pos.findInRange(FIND_STRUCTURES, 3, {
            filter: (a: any) => {
                return a.structureType === STRUCTURE_LINK;
            },
        });
        if (found && found.length > 0) {
            this.roomMemory.receivingLink = found[0].id;
            // console.log(this.room.name + " -> " + JSON.stringify(found[0]));
        }
        // console.log(this.room.name + " this.roomMemory.receivingLink=" + this.roomMemory.receivingLink);

    }

    private refreshFixedHarvesters() {
        if (!this.roomMemory.fixedHarvesters) {
            this.roomMemory.cntActiveFixedHarvesters = 0;
            return;
        }
        let total = 0;
        for (let position of this.roomMemory.fixedHarvesters) {
            if (position.active) {
                total++;
                continue;
            }
            let objects = this.room.lookAt(position.x, position.y);
            for (let obj of objects) {
                if (obj.structure && obj.structure.structureType === STRUCTURE_CONTAINER) {
                    // console.log(JSON.stringify(obj));
                    position.active = true;
                    total++;
                }
            }
        }
        this.roomMemory.cntActiveFixedHarvesters = total;
    }

    updateMilestones() {
        if (!((<any> this.room.memory).milestones)) {
            (<any> this.room.memory).milestones = {};
        }
        let mem: any = (<any> this.room.memory).milestones;
        let tmp: string = "reachedRcl" + (<any> this.room.controller).level;

        mem[tmp] = Game.time;
    }

}
Profiler.registerClass(RoomControllerCommon, RoomControllerCommon.name);
