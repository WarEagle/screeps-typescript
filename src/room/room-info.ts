export class RoomInfo {
    private timeToLife: number = 100000; // 100 000

    public cntSources: number;
    public isKeeper: boolean;
    public isEnemy: boolean;
    public isEmpty: boolean;
    public isRoomAvailable: boolean;
    public cntWorkableFieldsController: number = 0;
    public cntWorkableFieldsSource: number = 0;
    public timeScanned: number;

    //noinspection JSUnusedGlobalSymbols
    public isOutdated(): boolean {
        return (Game.time - this.timeToLife) > this.timeScanned;
    }
}
