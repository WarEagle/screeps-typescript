import {RoomControllerCommon} from "./room-controller-common";
import {RoomControllerRcl2} from "./room-controller-rcl2";
import {RoomControllerRcl1} from "./room-controller-rcl1";
import {RoomControllerRcl3} from "./room-controller-rcl3";
import {RoomControllerRcl4} from "./room-controller-rcl4";
import {RoomControllerRcl5} from "./room-controller-rcl5";
import {Profiler} from "../profiler";
import {RoomControllerRcl6} from "./room-controller-rcl6";
import {RoomControllerRcl7} from "./room-controller-rcl7";

export class GlobalRoomsController {

    public getController(room: Room, forStatistics?: boolean): RoomControllerCommon {
        let rc: RoomControllerCommon;

        // console.log("Room " + room.name + "");
        if (!room.controller) {
            console.log("Room " + room.name + " seems to not have a controller!");
            rc = new RoomControllerRcl1(room, forStatistics);
            return rc;
        }
        if (room.controller.level === 1) {
            rc = new RoomControllerRcl1(room, forStatistics);
        } else if (room.controller.level === 2) {
            rc = new RoomControllerRcl2(room, forStatistics);
        } else if (room.controller.level === 3) {
            rc = new RoomControllerRcl3(room, forStatistics);
        } else if (room.controller.level === 4) {
            rc = new RoomControllerRcl4(room, forStatistics);
        } else if (room.controller.level === 5) {
            rc = new RoomControllerRcl5(room, forStatistics);
        } else if (room.controller.level === 6) {
            rc = new RoomControllerRcl6(room, forStatistics);
        } else if (room.controller.level === 7) {
            rc = new RoomControllerRcl7(room, forStatistics);
        } else if (room.controller.level === 8) {
            // FIXME
            rc = new RoomControllerRcl6(room);
        } else {
            console.log("No Controller for room " + room.name);
            console.log("room.controller " + room.controller);
            console.log("room.controller.level " + room.controller.level);
            // use highest one
            rc = new RoomControllerRcl2(room);
        }
        return rc;
    }
}
Profiler.registerClass(GlobalRoomsController, GlobalRoomsController.name);
