import {RoomControllerCommon} from "./room-controller-common";
import {ScreepUtils} from "../helper/screep-utils";
import {Profiler} from "../profiler";

export class RoomControllerRcl1 extends RoomControllerCommon {

    constructor(protected room: Room, forStatistics?: boolean) {
        super(room, forStatistics);
    }

    public getMaxEnergyAllowed(): number {
        return Math.min(1600, this.room.energyCapacityAvailable);
    }

    public getTargetCntRepairer(): number {
        return this.notAtThisLevel;
    }

    public getTargetHarvester(): number {
        return Math.min(3, this.roomMemory.cntSources * 2);
    }

    public getTargetUpgrader(): number {
        return 1;
    }

    public getTargetBuilder(): number {
        if (this.room.memory.attackMode) {
            return 1;
        }
        if (this.room.memory.supportMode) {
            return 1;
        }
        let supportingRemoteBuilder = ScreepUtils.count(Game.creeps, (c) => (
            c.memory.role === "remoteBuilder"
            && c.memory.targetRoomName === this.room.name
        ));
        let tmp = Math.min(8, this.roomMemory.cntSources * 3);
        tmp -= supportingRemoteBuilder * 2;
        tmp = Math.max(1, tmp);
        return tmp;
    }

    public getTargetRecharger(): number {
        return this.notAtThisLevel;
    }

    public getTargetCntLorries(): number {
        return this.notAtThisLevel;
    }

    public tasksForUpgrade() {
        console.log("Nothing to do for level 1");
    }

    tasksForNextUpgradeSubtask() {
    }

}
Profiler.registerClass(RoomControllerRcl1, RoomControllerRcl1.name);
