import {Profiler} from "../profiler";
import {ScreepUtils} from "../helper/screep-utils";
import {RoomMemory} from "./room-memory";

export class ResourceDistributor {

    public distribute() {
        if (Game.time % 5 !== 0) {
            return;
        }

        this.distributeEnergy();
        this.distributeMinerals();
    }

    private distributeMinerals() {

        let mineralsNeeded: {trgRoom: Room, srcRoom?: Room, mineral: string}[] = [];
        for (let room of ScreepUtils.roomsWithController()) {
            let mem = <RoomMemory> room.memory;
            for (let labOpId in mem.labOperations) {
                let labOp = mem.labOperations[labOpId];
                if (!labOp.producer) {
                    let currMineral = labOp.resource;
                    if ((room.terminal && room.terminal.store[currMineral] && room.terminal.store[currMineral] > 10000)
                        || (room.storage && room.storage.store[currMineral] && room.storage.store[currMineral] > 10000)
                    ) {
                        //enough stored
                        continue;
                    }
                    mineralsNeeded.push({trgRoom: room, mineral: currMineral});
                }
            }
        }

        //console.log(JSON.stringify(mineralsNeeded));
        if (mineralsNeeded.length === 0) {
            //nothing needed
            return;
        }

        for (let currMineral of mineralsNeeded) {
            let roomsWithMinerals = _.filter(Game.rooms, (room: Room) => {
                return room.controller
                    && room.controller.my
                    && room.terminal
                    && room.terminal.store
                    && room.terminal.store[currMineral.mineral]
                    && room.terminal.store[currMineral.mineral] > 20000
                    ;
            });
            if (!roomsWithMinerals || roomsWithMinerals.length === 0) {
                continue;
            }

            // let srcTerminal = _.first(roomsWithMinerals).terminal;
            // console.log("1 "+srcTerminal!.room.name + " " + currMineral.mineral + " " + " -> " + currMineral.trgRoom.name);
            currMineral.srcRoom = _.first(roomsWithMinerals);
        }

        for (let currMineral of mineralsNeeded) {
            // console.log("2 "+currMineral.srcRoom + " " + currMineral.mineral + " " + " -> " + currMineral.trgRoom.name);
            if (currMineral.srcRoom) {
                // console.log("3 "+currMineral.srcRoom.name + " " + currMineral.mineral + " " + " -> " + currMineral.trgRoom.name);
                if (_.sum(currMineral.trgRoom!.terminal!.store) < 280000) {
                    console.log("transfer " + currMineral.srcRoom.name + " (" + currMineral.mineral + ") " + " -> " + currMineral.trgRoom.name);
                    currMineral.srcRoom!.terminal!.send(currMineral.mineral, 10000, currMineral.trgRoom.name);
                }
            }
        }

        // let roomsWithMuchOfThisMineral = _.filter(Game.rooms, (room: Room) => {
        //     return room.controller
        //         && room.controller.my
        //         && room.terminal
        //         && room.terminal.store
        //         && room.terminal.store[RESOURCE_ENERGY]
        //         && room.terminal.store[RESOURCE_ENERGY] > 120000
        //         ;
        // });
    }

    private distributeEnergy() {
        let roomsWithEnergy = _.filter(Game.rooms, (room: Room) => {
            return room.controller
                && room.controller.my
                && room.terminal
                && room.terminal.store
                && room.terminal.store[RESOURCE_ENERGY]
                && room.terminal.store[RESOURCE_ENERGY] > 120000
                ;
        });

        // console.log("roomsWithEnergy="+JSON.stringify(roomsWithEnergy));
        if (!roomsWithEnergy || roomsWithEnergy.length <= 0) {
            return;
        }

        let roomsWithNeed = _.filter(Game.rooms, (room: Room) => {
            return room.controller
                && room.controller.my
                && room.terminal
                && room.terminal.store
                && (_.sum(room.terminal.store) < 250000)
                && (!room.terminal.store[RESOURCE_ENERGY] || room.terminal.store[RESOURCE_ENERGY] < 50000)
                ;
        });
        // console.log("roomsWithNeed="+JSON.stringify(roomsWithNeed));

        if (!roomsWithNeed || roomsWithNeed.length <= 0) {
            return;
        }

        (<StructureTerminal>_.first(roomsWithEnergy).terminal).send(RESOURCE_ENERGY, 50000, _.first(roomsWithNeed).name, "internal transfer");

    }
}

Profiler.registerClass(ResourceDistributor, ResourceDistributor.name);
