import {Profiler} from "../profiler";
export class BaseDesigner {

    public buildRoad(from: RoomPosition, to: RoomPosition): void {
        // console.log("Building Road");
        // console.log("\t from: " + JSON.stringify(from));
        // console.log("\t to: " + JSON.stringify(to));
        this.buildRoomChangerRoad(from, to);
        this.buildRoomChangerRoad(to, from);
    }

    private buildRoomChangerRoad(from: RoomPosition, to: RoomPosition) {
        let route = from.findPathTo(to, {
            ignoreCreeps: true,
            ignoreRoads: false,
            maxRooms: 10,
            maxOps: 10000,
        });
        // console.log("route=" + JSON.stringify(route));
        for (let step of route) {
            let pos = new RoomPosition(step.x, step.y, from.roomName);
            Game.rooms[from.roomName].createConstructionSite(pos, STRUCTURE_ROAD);
            // console.log("\t\t" + step.x + "/" + step.y);
        }
    }

    public buildAllFixedHarvesterContainers = function (roomName: string): void {
        console.log("Building All Fixed Harvester Container");
        let room = Game.rooms[roomName];
        let designer = new BaseDesigner();

        let spawn: any = room.find(FIND_MY_SPAWNS)[0];
        let sources: any = room.find(FIND_SOURCES);
        for (let source of sources) {
            designer.createFixedHarvesterContainer(spawn, source.pos);
        }
    };

    public createFixedHarvesterContainer = function (spawner: StructureSpawn, posTo: RoomPosition): void {
        console.log("Building Fixed Harvester Container");
        let roomName = spawner.pos.roomName;
        let room = Game.rooms[roomName];

        let posFrom = spawner.pos;
        let route = room.findPath(posFrom, posTo, {
            ignoreCreeps: true,
            ignoreRoads: true,

        });
        let lastPos = route[route.length - 2];
        // console.log(JSON.stringify(lastPos));
        // room.createFlag(lastPos.x, lastPos.y, "Here!");
        room.createConstructionSite(lastPos.x, lastPos.y, STRUCTURE_CONTAINER);

        // remember in memory
        if (!room.memory.fixedHarvesters) {
            room.memory.fixedHarvesters = [];
        }
        room.memory.fixedHarvesters.push({x: lastPos.x, y: lastPos.y, active: false});

    };

    private getLayout(): string[] {

        let rc: string[] = [];

        // 2 -> Extension RCL 2
        // 3 -> Extension RCL 3
        // 4 -> Extension RCL 4
        // 5 -> Extension RCL 5
        // + -> Spawn
        // # -> Road
        // T -> Tower
        // S -> Storage
        // L -> Link - not used any more
        // Y -> Laboratory - not used any more
        rc.push("               "); //  1
        rc.push("               "); //  2
        rc.push("   55 5 5 55   "); //  3
        rc.push("  5  5   5  5  "); //  4
        rc.push("  5  44 44  5  "); //  5
        rc.push("   33 2 2 33   "); //  6
        rc.push("  3 22    S 3  "); //  7
        rc.push("               "); //  8
        rc.push("  3 22   22 4  "); //  9
        rc.push("   33 2 2 34   "); // 10
        rc.push("  5  44 44  4  "); // 11
        rc.push("  5  4   4  5  "); // 12
        rc.push("   55 5 5 55   "); // 13
        rc.push("               "); // 14
        rc.push("               "); // 15

        return rc;
    }

    private convertStringFromLayout(layout: string, rcl: number): string|undefined {
        let rc: string|undefined = undefined;

        switch (layout) {
            case "2":
                if (rcl >= 2) {
                    rc = STRUCTURE_EXTENSION;
                }
                break;
            case "3":
                if (rcl >= 3) {
                    rc = STRUCTURE_EXTENSION;
                }
                break;
            case "4":
                if (rcl >= 4) {
                    rc = STRUCTURE_EXTENSION;
                }
                break;
            case "5":
                if (rcl >= 5) {
                    rc = STRUCTURE_EXTENSION;
                }
                break;
            case "E":
                rc = STRUCTURE_EXTENSION;
                break;
            case "S":
                rc = STRUCTURE_STORAGE;
                break;
            case "T":
                rc = STRUCTURE_TOWER;
                break;
            case "L":
                rc = STRUCTURE_LINK;
                break;
            case "Y":
                rc = STRUCTURE_LAB;
                break;
            case "#":
                if (rcl >= 4) {
                    rc = STRUCTURE_ROAD;
                }
                break;
            case " ":
                break;
            default:
                rc = undefined;
        }
        // console.log("layout=" + layout + ", rc=" + rc);
        return rc;
    }

    private getCharFromLayout(x: number, y: number, layout: string[]): string {
        let rc = " ";
        // console.log("x=" + x + ", y=" + y);
        // console.log("layout[y]="+layout[y]);
        if (!layout[y]) {
            return rc;
        }
        if (layout[y].length < x) {
            return rc;
        }
        // console.log("\t=>" + layout[y].charAt(x));
        return layout[y].charAt(layout[y].length - x);
    }

    public buildBaseRcl(room: Room): void {
        if (!room.controller) {
            console.log("Error, no controller!");
            return;
        }
        console.log("Setting up constructions for Level " + room.controller.level);

        let spawners: any = room.find(FIND_MY_SPAWNS);
        // console.log(JSON.stringify(spawners));
        let spawner = spawners[0];
        if (!spawner) {
            console.log("Needs spawner!");
            return;
        }

        let spawnerX = spawner.pos.x;
        let spawnerY = spawner.pos.y;
        let rc: number;
        let layout = this.getLayout();

        let width = Math.floor(layout[0].length / 2);
        let height = Math.floor(layout.length / 2);

        for (let x = -width; x < width + 1; x++) {
            for (let y = -height; y < height + 1; y++) {
                let tmp = this.getCharFromLayout(x + width, y + height, layout);
                let tmp2 = this.convertStringFromLayout(tmp, +room.controller.level);
                if (tmp2) {
                    // console.log("tmp2=" + tmp2);
                    rc = room.createConstructionSite(spawnerX - x + 1, spawnerY - y, tmp2);
                    // console.log(rc);
                }
            }
        }
    }
}
Profiler.registerClass(BaseDesigner, BaseDesigner.name);
