import {RoomControllerCommon} from "./room-controller-common";
import {BaseDesigner} from "./base-designer";
import {ScreepUtils} from "../helper/screep-utils";
import {Profiler} from "../profiler";

export class RoomControllerRcl2 extends RoomControllerCommon {
    constructor(protected room: Room, forStatistics?: boolean) {
        super(room, forStatistics);
    }

    public getMaxEnergyAllowed(): number {
        return Math.min(1600, this.room.energyCapacityAvailable);
    }

    public getTargetCntRepairer(): number {
        return this.notAtThisLevel;
    }

    public getTargetHarvester(): number {
        // TODO depend on current avail energy capacity
        // if (this.room.memory.supportMode){
        //     return 1;
        // }
        if (this.room.energyCapacityAvailable >= 700) {
            return this.roomMemory.cntSources + 1;
        }
        return Math.max(3, this.roomMemory.cntSources * 2.5);
    }

    public getTargetUpgrader(): number {
        // return Math.min(8, this.roomMemory.cntSources * 4);
        // TODO depend on current avail energy capacity
        return 1;
    }

    public getTargetBuilder(): number {
        if (this.room.memory.attackMode) {
            return 1;
        }
        if (this.room.memory.supportMode) {
            return 1;
        }
        let tmp = 0;
        // TODO make this depending on number of construction sites
        // TODO depend on current avail energy capacity
        if (this.room.energyCapacityAvailable >= 700) {
            tmp = Math.min(8, this.roomMemory.cntSources * 3);
        } else {
            tmp = Math.min(8, this.roomMemory.cntSources * 4);
        }

        let supportingRemoteBuilder = ScreepUtils.count(Game.creeps, (c) => (
            c.memory.role === "remoteBuilder"
            && c.memory.targetRoomName === this.room.name
        ));
        tmp -= supportingRemoteBuilder * 3;
        tmp = Math.max(1, tmp);
        return tmp;
    }

    public getTargetRecharger(): number {
        return this.notAtThisLevel;
    }

    public getTargetCntLorries(): number {
        return this.notAtThisLevel;
    }

    public tasksForUpgrade() {
        new BaseDesigner().buildBaseRcl(this.room);
    }

    tasksForNextUpgradeSubtask() {
    }

}
Profiler.registerClass(RoomControllerRcl2, RoomControllerRcl2.name);
