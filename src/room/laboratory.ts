import {RoomMemory} from "./room-memory";
import {Profiler} from "../profiler";
export class Laboratory {

    public runLabs(room: Room) {
        if (Game.time % 5 !== 0) {
            //TODO check if the timeout of labs is constant
            return;
        }

        let mem = <RoomMemory> room.memory;
        for (let op in mem.labOperations) {
            let operation = mem.labOperations[op];
            if (operation.producer) {
                this.runReaction(op, operation.lab1, operation.lab2);
            }
        }
    }

    public runReaction(mainLabId: string, lab1Id: string, lab2Id: string): void {
        let mainLab = <StructureLab> Game.getObjectById(mainLabId);
        if (mainLab.cooldown > 0) {
            return;
        }
        let lab1 = <StructureLab> Game.getObjectById(lab1Id);
        let lab2 = <StructureLab> Game.getObjectById(lab2Id);

        mainLab.runReaction(lab1, lab2);
    }
}

Profiler.registerClass(Laboratory, Laboratory.name);
