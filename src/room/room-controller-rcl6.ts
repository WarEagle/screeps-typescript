import {RoomControllerCommon} from "./room-controller-common";
import {BaseDesigner} from "./base-designer";
import {WorldInfoCollector} from "../statistics/world-info";
import {Profiler} from "../profiler";

export class RoomControllerRcl6 extends RoomControllerCommon {
    constructor(protected room: Room, forStatistics?: boolean) {
        super(room, forStatistics);
    }

    public getMaxEnergyAllowed(): number {
        return Math.min(1500, this.room.energyCapacityAvailable);
    }

    public getTargetCntRepairer(): number {
        return 1;
    }

    public getTargetHarvester(): number {
        // at this point everything should be on fixed harvesters
        let tmp;
        tmp = Math.min(3, this.roomMemory.cntSources);
        if (this.room.energyCapacityAvailable >= 1400) {
            tmp = 1;
        } else if (this.room.energyCapacityAvailable >= 1000) {
            tmp = this.roomMemory.cntSources;
        } else if (this.room.energyCapacityAvailable >= 700) {
            tmp = this.roomMemory.cntSources + 1;
        }
        tmp -= this.roomMemory.cntActiveFixedHarvesters;
        return Math.max(0, tmp);
    }

    public getTargetUpgrader(): number {
        return 1;
    }

    public getTargetBuilder(): number {
        if (this.room.memory.attackMode) {
            return 1;
        }
        if (this.room.memory.supportMode) {
            return 1;
        }
        let tmp = 0;
        // TODO make this depending on number of construction sites
        if (this.room.energyCapacityAvailable >= 1500) {
            tmp = 1;
        } else if (this.room.energyCapacityAvailable >= 1000) {
            tmp = Math.min(8, this.roomMemory.cntSources);
        } else if (this.room.energyCapacityAvailable >= 700) {
            tmp = Math.min(8, this.roomMemory.cntSources * 3);
        } else {
            tmp = Math.min(8, this.roomMemory.cntSources * 3);
        }

        let workableFields = WorldInfoCollector.getCntWorkableFieldsSource(this.room.name);

        if ((this.room.energyAvailable < this.room.energyCapacityAvailable * 0.75) && (this.room.storage && this.room.storage.store.energy < 500)) {
            tmp = 1;
        }

        tmp = Math.min(workableFields, tmp);

        if (this.room.storage && this.room.storage.store) {
            tmp += Math.floor(this.room.storage.store.energy / 60000);
        }
        if (this.room.terminal && this.room.terminal.store) {
            tmp += Math.floor(this.room.terminal.store.energy / 60000);
        }

        return tmp;
    }

    public getTargetRecharger(): number {
        return 1;
    }

    public getTargetCntLorries(): number {
        let tmp = 0;
        tmp += Math.ceil(this.roomMemory.cntActiveFixedHarvesters);
        if (this.roomMemory.receivingLink) {
            tmp++;
        }
        return tmp;
    }

    public tasksForUpgrade() {
        let designer = new BaseDesigner();
        designer.buildBaseRcl(this.room);
        this.roomMemory.requiredRclSubtask = 1;
    }

    tasksForNextUpgradeSubtask() {
        this.roomMemory.requiredRclSubtask--;

        this.placeExtractor();
        // some cleanup
        delete this.roomMemory.requiredRclSubtask;
    }

    private placeExtractor() {
        let mineral: Mineral = <Mineral> this.room.find(FIND_MINERALS)[0];
        let pos = new RoomPosition(mineral.pos.x, mineral.pos.y, this.room.name);
        this.room.createConstructionSite(pos, STRUCTURE_EXTRACTOR);

    }
}
Profiler.registerClass(RoomControllerRcl6, RoomControllerRcl6.name);
