export class RoomMemory {

    public lastRcl: number;
    public requiredRclSubtask: number;
    public lastRefresh: number;
    public cntSources: number;
    public cntActiveFixedHarvesters: number;
    public fixedHarvesters: {x: number, y: number, active: boolean}[];
    public unloadingLinks: {targetRoomName: string, id: string}[];
    public cntConstructionSites: number;
    public receivingLink: string;
    public displayName: string;
    public extractorId: string;
    public mineralId: string;
    public miningOperations: SourceMap;
    public labOperations: LabMap;

}

interface SourceMap {
    [key: string]: TargetMap;
}

interface LabMap {
    [labId: string]: LabOperation;
}

interface TargetMap {
    [key: string]: MiningOperation;
}

export interface MiningOperation {
    hasControllerRoads: boolean;
    hasSourceRoads: boolean;
    hasExternalSourceRoads: boolean;
    hasContainer: boolean;
    hasCntSource: boolean;
    isSourceKeeper: boolean;
    cntSources?: number;
    containers: Array<MiningContainer>;
}

export interface LabOperation {
    producer: boolean;
    amount: number;
    resource: string;
    lab1: string;
    lab2: string;
}

export interface MiningContainer {
    sourceId: string;
    containerId?: string;
    x: number;
    y: number;
    isBuilt: boolean;
    distance: number;
}
