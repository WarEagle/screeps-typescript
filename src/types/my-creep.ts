import {Profiler} from "../profiler";

export class MyCreep {
    constructor(private creep: Creep) {
    }

    getOriginalCreep(): Creep {
        return this.creep;
    }

    get memory(): any {
        return this.creep.memory;
    }

    get ticksToLive(): any {
        return this.creep.ticksToLive;
    }

    get hits(): number {
        return this.creep.hits;
    }

    get hitsMax(): number {
        return this.creep.hitsMax;
    }

    get body(): any {
        return this.creep.body;
    }

    get carry(): any {
        return this.creep.carry;
    }

    get carryCapacity(): any {
        return this.creep.carryCapacity;
    }

    get room(): any {
        return this.creep.room;
    }

    get owner(): any {
        return this.creep.owner;
    }

    get pos(): any {
        return this.creep.pos;
    }

    get name(): any {
        return this.creep.name;
    }

    get fatigue(): number {
        return this.creep.fatigue;
    }

    /**
     * Attack another creep or structure in a short-ranged attack. Needs the ATTACK body part. If the target is inside a rampart, then the rampart is attacked instead. The target has to be at adjacent square to the creep. If the target is a creep with ATTACK body parts and is not inside a rampart, it will automatically hit back at the attacker.
     * @returns Result Code: OK, ERR_NOT_OWNER, ERR_BUSY, ERR_INVALID_TARGET, ERR_NOT_IN_RANGE, ERR_NO_BODYPART
     */
    attack(target: Creep | Spawn | Structure): number {
        return this.creep.attack(target);
    }

    /**
     * Drop this resource on the ground.
     * @param resourceType One of the RESOURCE_* constants.
     * @param amount The amount of resource units to be dropped. If omitted, all the available carried amount is used.
     */
    drop(resourceType: string, amount?: number): number {
        return this.creep.drop(resourceType, amount);
    }

    /**
     * Repair a damaged structure using carried energy. Needs the WORK and CARRY body parts. The target has to be within 3 squares range of the creep.
     * @param target he target structure to be repaired.
     */
    repair(target: Spawn | Structure): number {
        if ((Math.abs(this.pos.x - target.pos.x) + Math.abs(this.pos.y - target.pos.y)) > 5) {
            return ERR_NOT_IN_RANGE;
        }
        return this.creep.repair(target);
    }

    /**
     * A ranged attack against another creep or structure. Needs the RANGED_ATTACK body part. If the target is inside a rampart, the rampart is attacked instead. The target has to be within 3 squares range of the creep.
     * @param target The target object to be attacked.
     */
    rangedAttack(target: Creep | Spawn | Structure): number {
        return this.creep.rangedAttack(target);
    }

    /**
     * Build a structure at the target construction site using carried energy. Needs WORK and CARRY body parts. The target has to be within 3 squares range of the creep.
     * @param target The target object to be attacked.
     * @returns Result Code: OK, ERR_NOT_OWNER, ERR_BUSY, ERR_NOT_ENOUGH_RESOURCES, ERR_INVALID_TARGET, ERR_NOT_IN_RANGE, ERR_NO_BODYPART, ERR_RCL_NOT_ENOUGH
     */
    build(target: ConstructionSite): number {
        if ((Math.abs(this.pos.x - target.pos.x) + Math.abs(this.pos.y - target.pos.y)) > 5) {
            return ERR_NOT_IN_RANGE;
        }
        return this.creep.build(target);
    }

    /**
     * Requires the CLAIM body part. If applied to a neutral controller, claims it under your control. If applied to a hostile controller, decreases its downgrade or reservation timer depending on the CLAIM body parts count. The target has to be at adjacent square to the creep.
     * @param target The target controller object.
     * @returns Result Code: OK, ERR_NOT_OWNER, ERR_BUSY, ERR_INVALID_TARGET, ERR_FULL, ERR_NOT_IN_RANGE, ERR_NO_BODYPART, ERR_GCL_NOT_ENOUGH
     */
    claimController(target: Controller): number {
        if ((Math.abs(this.pos.x - target.pos.x) + Math.abs(this.pos.y - target.pos.y)) > 2) {
            return ERR_NOT_IN_RANGE;
        }
        return this.creep.claimController(target);
    }

    /**
     * Decreases the controller's downgrade or reservation timer for 1 tick per every 5 CLAIM body parts (so the creep must have at least 5xCLAIM). The controller under attack cannot be upgraded for the next 1,000 ticks. The target has to be at adjacent square to the creep.
     * @returns Result Code: OK, ERR_NOT_OWNER, ERR_BUSY, ERR_INVALID_TARGET, ERR_NOT_IN_RANGE, ERR_NO_BODYPART
     */
    attackController(target: Structure): number {
        return this.creep.attackController(target);
    }

    /**
     * Temporarily block a neutral controller from claiming by other players. Each tick, this command increases the counter of the period during which the controller is unavailable by 1 tick per each CLAIM body part. The maximum reservation period to maintain is 5,000 ticks. The target has to be at adjacent square to the creep....
     * @param target The target controller object to be reserved.
     * @return Result code: OK, ERR_NOT_OWNER, ERR_BUSY, ERR_INVALID_TARGET, ERR_NOT_IN_RANGE, ERR_NO_BODYPART
     */
    reserveController(target: Controller): number {
        if ((Math.abs(this.pos.x - target.pos.x) + Math.abs(this.pos.y - target.pos.y)) > 2) {
            return ERR_NOT_IN_RANGE;
        }
        return this.creep.reserveController(target);
    }

    /**
     * Harvest energy from the source. Needs the WORK body part. If the creep has an empty CARRY body part, the harvested energy is put into it; otherwise it is dropped on the ground. The target has to be at an adjacent square to the creep.
     * @param target The source object to be harvested.
     */
    harvest(target: Source | Mineral): number {
        return this.creep.harvest(target);
    }

    /**
     * Heal self or another creep. It will restore the target creep’s damaged body parts function and increase the hits counter. Needs the HEAL body part. The target has to be at adjacent square to the creep.
     * @param target The target creep object.
     */
    heal(target: MyCreep): number {
        return this.creep.heal(target.getOriginalCreep());
    }

    /**
     * Move the creep one square in the specified direction. Needs the MOVE body part.
     * @param direction
     */
    move(direction: number): number {
        return this.creep.move(direction);
    }

    /**
     * Find the optimal path to the target within the same room and move to it. A shorthand to consequent calls of pos.findPathTo() and move() methods. If the target is in another room, then the corresponding exit will be used as a target. Needs the MOVE body part.
     * @param x X position of the target in the room.
     * @param y Y position of the target in the room.
     * @param opts An object containing pathfinding options flags (see Room.findPath for more info) or one of the following: reusePath, serializeMemory, noPathFinding
     */
    moveToXY(x: number, y: number, opts?: MoveToOpts & FindPathOpts): number {
        if (this.pos.x === x && this.pos.y === y) {
            return OK;
        }
        let newOpts = opts;
        if (!opts) {
            // console.log("opts="+JSON.stringify(opts));
            newOpts = {};
        }
        if (newOpts && !newOpts.reusePath) {
            newOpts.reusePath = 10;
        }
        if (newOpts && !newOpts.maxRooms) {
            newOpts.maxRooms = 1;
        }
        return this.creep.moveTo(x, y, newOpts);
    }

    /**
     * Find the optimal path to the target within the same room and move to it. A shorthand to consequent calls of pos.findPathTo() and move() methods. If the target is in another room, then the corresponding exit will be used as a target. Needs the MOVE body part.
     * @param target Can be a RoomPosition object or any object containing RoomPosition.
     * @param opts An object containing pathfinding options flags (see Room.findPath for more info) or one of the following: reusePath, serializeMemory, noPathFinding
     */
    moveTo(target: RoomPosition | {
        pos: RoomPosition;
    }, opts?: MoveToOpts & FindPathOpts): number {
        let newOpts = opts;
        if (!opts) {
            newOpts = {};
        }
        if (newOpts && !newOpts.reusePath) {
            newOpts.reusePath = 10;
        }
        if (newOpts && !newOpts.maxRooms) {
            newOpts.maxRooms = 1;
        }
        return this.creep.moveTo(target, newOpts);
    }

    /**
     * Transfer resource from the creep to another object. The target has to be at adjacent square to the creep.
     * @param target The target object.
     * @param resourceType One of the RESOURCE_* constants
     * @param amount The amount of resources to be transferred. If omitted, all the available carried amount is used.
     */
    transfer(target: Creep | Spawn | Structure, resourceType: string, amount?: number): number {
        if (!target) {
            return ERR_INVALID_TARGET;
        }
        if ((Math.abs(this.pos.x - target.pos.x) + Math.abs(this.pos.y - target.pos.y)) > 2) {
            return ERR_NOT_IN_RANGE;
        }
        return this.creep.transfer(target, resourceType, amount);
    }

    /**
     * Upgrade your controller to the next level using carried energy. Upgrading controllers raises your Global Control Level in parallel. Needs WORK and CARRY body parts. The target has to be at adjacent square to the creep. A fully upgraded level 8 controller can't be upgraded with the power over 15 energy units per tick regardless of creeps power. The cumulative effect of all the creeps performing upgradeController in the current tick is taken into account.
     * @param target The target controller object to be upgraded.
     */
    upgradeController(target: Controller): number {
        return this.creep.upgradeController(target);
    }

    /**
     * Pick up an item (a dropped piece of energy). Needs the CARRY body part. The target has to be at adjacent square to the creep or at the same square.
     * @param target The target object to be picked up.
     */
    pickup(target: Resource): number {
        if ((Math.abs(this.pos.x - target.pos.x) + Math.abs(this.pos.y - target.pos.y)) > 2) {
            return ERR_NOT_IN_RANGE;
        }
        return this.creep.pickup(target);
    }

    /**
     * Withdraw resources from a structure. The target has to be at adjacent square to the creep. Multiple creeps can withdraw from the same structure in the same tick. Your creeps can withdraw resources from hostile structures as well, in case if there is no hostile rampart on top of it.
     * @param target The target object.
     * @param resourceType The target One of the RESOURCE_* constants..
     * @param amount The amount of resources to be transferred. If omitted, all the available amount is used.
     */
    withdraw(target: Structure, resourceType: string, amount?: number): number {
        if ((Math.abs(this.pos.x - target.pos.x) + Math.abs(this.pos.y - target.pos.y)) > 2) {
            return ERR_NOT_IN_RANGE;
        }
        return this.creep.withdraw(target, resourceType, amount);
    }

    signController(target: StructureController, text: string): number {
        if ((Math.abs(this.pos.x - target.pos.x) + Math.abs(this.pos.y - target.pos.y)) > 2) {
            return ERR_NOT_IN_RANGE;
        }
        return this.creep.signController(target, text);
    }

    /**
     * Display a visual speech balloon above the creep with the specified message. The message will disappear after a few seconds. Useful for debugging purposes. Only the creep's owner can see the speech message.
     * @param message The message to be displayed. Maximum length is 10 characters.
     * @param toPublic set to 'true' to allow other players to see this message. Default is 'false'.
     */
    say(message: string, toPublic?: boolean): number {
        return this.creep.say(message, toPublic);
    }

    /**
     * Kill the creep immediately.
     */
    suicide(): number {
        return this.creep.suicide();
    }

    /**
     * Heal another creep at a distance. It will restore the target creep’s damaged body parts function and increase the hits counter. Needs the HEAL body part. The target has to be within 3 squares range of the creep.
     * @param target The target creep object.
     */
    rangedHeal(target: Creep): number {
        return this.creep.rangedHeal(target);
    }

    /**
     * A ranged attack against all hostile creeps or structures within 3 squares range. Needs the RANGED_ATTACK body part. The attack power depends on the range to each target. Friendly units are not affected.
     */
    rangedMassAttack(): number {
        return this.creep.rangedMassAttack();
    }

    /**
     * Get the quantity of live body parts of the given type. Fully damaged parts do not count.
     * @param type A body part type, one of the following body part constants: MOVE, WORK, CARRY, ATTACK, RANGED_ATTACK, HEAL, TOUGH, CLAIM
     */
    getActiveBodyparts(type: string): number {
        return this.creep.getActiveBodyparts(type);
    }

    /**1
     * Dismantles any (even hostile) structure returning 50% of the energy spent on its repairBuildings. Requires the WORK body part. If the creep has an empty CARRY body part, the energy is put into it; otherwise it is dropped on the ground. The target has to be at adjacent square to the creep.
     * @param target The target structure.
     */
    dismantle(target: Spawn | Structure): number {
        return this.creep.dismantle(target);
    }

    /**
     * Move the creep using the specified predefined path. Needs the MOVE body part.
     * @param path A path value as returned from Room.findPath or RoomPosition.findPathTo methods. Both array form and serialized string form are accepted.
     */
    moveByPath(path: PathStep[] | RoomPosition[] | string): number {
        return this.creep.moveByPath(path);
    }


}
Profiler.registerClass(MyCreep, MyCreep.name);
