import {Profiler} from "../profiler";
import {RoomMemory} from "../room/room-memory";
import {ScreepCache} from "../helper/screep-cache";

class StatusEntry {
    type: {
        [type: string]: {
            current: number,
            target: number
        }
    };
}

class StatusCollection {
    myRoomName: string;
    origRoomName: string;
    controllerLevel: number;
    controllerProgress: number;
    rooms: {[room: string]: StatusEntry};
}

export class SpawnerStatus {

    private data: StatusCollection;

    // private report: string = "";
    private totalCntCreeps: number = 0;
    private totalTargetCreeps: number = 0;
    public hasSpawned: boolean = false;

    private requiredEnergyForController: any = [];

    constructor(private room: Room) {
        let controller = room.controller;
        if (!controller) {
            return;
        }

        this.data = new StatusCollection();
        this.requiredEnergyForController["1"] = 200;
        this.requiredEnergyForController["2"] = 45000;
        this.requiredEnergyForController["3"] = 135000;
        this.requiredEnergyForController["4"] = 405000;
        this.requiredEnergyForController["5"] = 1215000;
        this.requiredEnergyForController["6"] = 3645000;
        this.requiredEnergyForController["7"] = 10935000;
        this.requiredEnergyForController["8"] = 0;

        this.data.controllerLevel = controller.level;
        this.data.controllerProgress = Math.floor(controller.progress / this.requiredEnergyForController[controller.level] * 100);

        let name = "Game.rooms." + this.room.name + ".memory.displayName missing";
        if ((<RoomMemory> room.memory).displayName) {
            name = (<RoomMemory> room.memory).displayName;
        }
        this.data.myRoomName = name;
        this.data.origRoomName = this.room.name;
    };

    private addHeader(data: StatusCollection): string {
        let report: string = "";

        report += "<div style='display: block;float: left'>";
        report += "<a href = \"#!/room/" + data.origRoomName + "\">" + data.origRoomName + "</a> - " + data.myRoomName;
        report += "</div>";
        report += "<div style='display: block;float: right'>";
        report += data.controllerLevel + " " + data.controllerProgress + "%";
        report += "</div>";
        report += "<div style='font-size: 11px;margin: 2px'>";
        report += "<table border='1'>";
        report += "<tr><th>Role</th><th>Current</th><th>Target</th></tr>";

        return report;
    }

    public addToData(type: string, current: number, target: number, roomName: string) {
        // console.log("=> "+type);
        if (!this.data.rooms) {
            this.data.rooms = {};
        }
        if (!this.data.rooms[roomName]) {
            this.data.rooms[roomName] = new StatusEntry();
        }
        if (!this.data.rooms[roomName].type) {
            this.data.rooms[roomName].type = {};
        }
        if (!this.data.rooms[roomName].type[type]) {
            this.data.rooms[roomName].type[type] = {current: current, target: target};
        } else {
            this.data.rooms[roomName].type[type].current += current;
            this.data.rooms[roomName].type[type].target += target;
        }
    }

    public addToReport(type: string, current: number, target: number, roomName?: string) {
        //console.log(this.room.name + " -> " + type + " " + current + "/" + target);
        //this.report += this.generateReportLine(type, current, target, roomName);

        if (roomName) {
            this.addToData(type, current, target, roomName);
        } else {
            this.addToData(type, current, target, "");
        }

        // console.log(JSON.stringify(this.data));

        if (type !== "Energy") {
            //Important for Grafana!
            this.totalCntCreeps += current;
            this.totalTargetCreeps += target;
        }
    }

    private generateReportLineAscii(type: string, current: number, target: number): string {
        let rc = "";
        rc += type;
        rc += ":";
        rc += current;
        rc += "/";
        rc += target;
        rc += ",   ";
        // rc += "<br/>";
        // rc += "\n";

        return rc;
    }

    private generateReportLine(type: string, current: number, target: number, roomName?: string, tooltip?: string): string {
        if (type !== "Energy") {
            ScreepCache.addCreepSpawningTotal(this.room.name, type, current, target);
        }

        let reportLine = "";
        let fontColor = "white";

        if (current < target && type !== "Energy") {
            fontColor = "red";
        }
        reportLine += "<tr style='color: " + fontColor + "'>";
        if (roomName) {
            reportLine += "<td style='padding-right: 5px'>";
            reportLine += type;
            reportLine += " -> ";
            reportLine += "<a href = \"#!/room/" + roomName + "\" ";
            reportLine += "data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"top\" ";
            reportLine += "title=\"" + tooltip + "\" ";
            reportLine += ">";
            reportLine += roomName;
            reportLine += "</a>";
            reportLine += "</td>";

        } else {
            reportLine += "<td style='padding-right: 5px'>" + type + "</td>";
        }
        reportLine += "<td style='text-align: center;'>" + current + "</td>";
        reportLine += "<td style='text-align: center;'>" + target + "</td>";
        reportLine += "</tr>";

        // <a href="#" data-toggle="tooltip" data-placement="top" title="Hooray!">Hover</a>

        return reportLine;
    }

    public generateSingleRoomReport_NEW(): string {
        let tmp = "";
        {
            let room = this.data.rooms[""];

            for (let type in room.type) {
                tmp += this.generateReportLine(type, room.type[type].current, room.type[type].target);
            }
        }

        {
            //TODO add tooltip with details
            for (let roomName in this.data.rooms) {
                if (roomName === "") {
                    continue;
                }
                let miningRoom = this.data.rooms[roomName];
                let cntCurrent = 0;
                let cntTarget = 0;
                let tooltip = "";
                // tooltip += "<html>";
                // tooltip += "<table>";
                for (let type in miningRoom.type) {
                    cntCurrent += miningRoom.type[type].current;
                    cntTarget += miningRoom.type[type].target;
                    tooltip += this.generateReportLineAscii(type, miningRoom.type[type].current, miningRoom.type[type].target);
                    // tooltip+="<br>";
                }
                // tooltip += "</html>";
                // tooltip += "</table>";
                tmp += this.generateReportLine("", cntCurrent, cntTarget, roomName, tooltip);
            }
        }

        tmp += this.generateReportLine("Total", this.totalCntCreeps, this.totalTargetCreeps);
        tmp += "</table>";
        tmp += "</div>";
        return tmp;
    }

    // public generateReport(): string {
    //     let tmp = this.report;
    //     tmp += this.generateReportLine("Total", this.totalCntCreeps, this.totalTargetCreeps);
    //     tmp += "</table>";
    //     tmp += "</div>";
    //     return tmp;
    // }

    static reportOverview(statusCollection: SpawnerStatus[]): void {
        if ((<any> Memory).fast && Game.time % 100 !== 0) {
            return;
        }
        if (!statusCollection || statusCollection.length === 0) {
            // don't report empty
            return;
        }
        let tmp: string = "";
        tmp += "<div>";
        for (let status of statusCollection) {
            // console.log(JSON.stringify(status));
            tmp += "<div style='display: block;float: left;margin-right: 10px'>";
            tmp += status.addHeader(status.data);
            tmp += status.generateSingleRoomReport_NEW();
            tmp += "</div>";
        }
        tmp += "</div>";
        console.log(tmp);

    }

}
Profiler.registerClass(SpawnerStatus, SpawnerStatus.name);
