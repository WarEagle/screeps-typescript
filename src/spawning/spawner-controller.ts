import {RoomConstants} from "../helper/helper-constants";
import {SpawnerWrapper} from "../statistics/spawner-wrapper";
import {RoomControllerCommon} from "../room/room-controller-common";
import {ScreepUtils} from "../helper/screep-utils";
import {SpawnerStatus} from "./spawner-status";
import {Profiler} from "../profiler";
import {RoleFactory} from "../roles/common/role-factory";
import {BaseRole, SpawnParameter} from "../roles/common/role-base";

export class SpawnerController {

    public spawn(room: Room, spawner: StructureSpawn, roomController: RoomControllerCommon, statusCollection: SpawnerStatus[], roleFactory: RoleFactory) {

        // console.log("spawn - " + room.name);
        let roomConstants: any = new RoomConstants().roomConstants;
        let constants = roomConstants[room.name];
        let spawnerStatus = new SpawnerStatus(spawner.room);

        if (spawner.spawning !== null) {
            //don't try to spawn if spawner is already spawning
            spawnerStatus.hasSpawned = true;
        }

        // TODO create different section in spawner-status
        // spawnerStatus.addToReport("Energy", room.energyAvailable, room.energyCapacityAvailable);

        let targetHarvester = roomController.getTargetHarvester();
        let targetBuilder = roomController.getTargetBuilder();
        let targetRecharger = roomController.getTargetRecharger();
        let targetLorry = roomController.getTargetCntLorries();

        let maxEnergyAllowed = roomController.getMaxEnergyAllowed();

        let groupedCreeps = _.groupBy(Game.creeps, "memory.role");
        let currHarvester = ScreepUtils.countArray(groupedCreeps["harvester"], (c: Creep) => c.room.name === spawner.room.name);
        let currBuilder = ScreepUtils.countArray(groupedCreeps["builder"], (c: Creep) => c.room.name === spawner.room.name);
        let currRecharger = ScreepUtils.countArray(groupedCreeps["recharger"], (c: Creep) => c.room.name === spawner.room.name);
        let currFixedHarvester = ScreepUtils.countArray(groupedCreeps["fixedHarvester"], (c: Creep) => c.room.name === spawner.room.name);
        let currLorry = ScreepUtils.countArray(groupedCreeps["lorry"], (c: Creep) => c.room.name === spawner.room.name);

        if (((currHarvester === 0 && currFixedHarvester === 0 && currLorry === 0))) {

            SpawnerController.spawnMaxAvail("harvester", spawner, spawnerStatus, roleFactory.getRoleHarvester());
            //set to true, even if not spawned, just to prevent others from using up the energy
            spawnerStatus.hasSpawned = true;
        }

        let spawnParameter: SpawnParameter = {
            spawner: spawner,
            spawnerStatus: spawnerStatus,
            roomController: roomController,
            maxEnergyAllowed: maxEnergyAllowed,
            filteredCreeps: [],
        };

        SpawnerController.spawnMax("harvester", spawner, spawnerStatus, currHarvester, targetHarvester, roleFactory.getRoleHarvester(), maxEnergyAllowed);
        roleFactory.getRoleFixedHarvester().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleLorry().spawnIfNeededOldWay(spawner, spawnerStatus, currLorry, targetLorry, currHarvester, maxEnergyAllowed);
        roleFactory.getRoleUpgrader().spawnIfNeeded(spawnParameter, groupedCreeps);
        if (targetRecharger !== 0) {
            SpawnerController.spawnMaxCarry(roleFactory.getRoleRecharger().roleName(), spawner, spawnerStatus, currRecharger, targetRecharger, roleFactory.getRoleRecharger(), maxEnergyAllowed);
        }
        roleFactory.getRoleRemoteFixedHarvester().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleRemoteHarvester().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleDefender().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleDefender().spawnIfNeededOldWay(constants, spawner, spawnerStatus, maxEnergyAllowed);//TODO remove me
        roleFactory.getRoleRepairer().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleAttacker().spawnIfNeeded(spawnParameter, groupedCreeps);
        // roleFactory.getRoleClaimer().spawnIfNeededOldWay(roomName, constants, spawner, spawnerStatus);//TODO remove me
        roleFactory.getRoleClaimer().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleBuilder().spawnIfNeededOldWay(spawner, spawnerStatus, currBuilder, targetBuilder, maxEnergyAllowed);// TODO remove me
        roleFactory.getRoleRemoteBuilder().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleRemoteBuilder().spawnIfNeededOldWay(constants, spawner, spawnerStatus, maxEnergyAllowed);//TODO remove me
        roleFactory.getRoleRoomTransfer().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleMiner().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleScout().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleLabRat().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleBoostedUpgrader().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleRemoteLorry().spawnIfNeeded(spawnParameter, groupedCreeps);
        roleFactory.getRoleRoleAreaDefender().spawnIfNeeded(spawnParameter, groupedCreeps);

        statusCollection.push(spawnerStatus);
    }

    public static spawnMax(givenRole: string, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, curr: number, target: number, creepRole: BaseRole, maxEnergyCapacityAvailable: number): void {
        let nrComponents = Math.floor(maxEnergyCapacityAvailable / 200);
        // console.log("spawner:"+spawner.pos.roomName);
        // console.log("energyCapacityAvailable:"+Game.spawns.Spawn1.room.energyCapacityAvailable);
        // console.log("nrComponents:"+nrComponents);
        if (target === 0) {
            return;
        }
        spawnerStatus.addToReport(givenRole, curr, target);

        if (curr >= target || spawnerStatus.hasSpawned) {
            return;
        }

        let components = [];
        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        let rc = SpawnerWrapper.createCreep(spawner, components, creepRole, {
                role: givenRole,
                working: false,
            },
        );
        // console.log(rc);
        if (_.isString(rc)) {
            spawnerStatus.hasSpawned = true;
        }
    }

    public static spawnMaxCarry(givenRole: string, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, curr: number, target: number, creepRole: BaseRole, maxEnergyCapacityAvailable: number): void {
        if (target === 0) {
            return;
        }
        spawnerStatus.addToReport(givenRole, curr, target);

        if (curr >= target || spawnerStatus.hasSpawned) {
            return;
        }
        let nrComponents = Math.floor(maxEnergyCapacityAvailable / 150);
        let components = [];
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        SpawnerWrapper.createCreep(spawner, components, creepRole, {
            role: givenRole,
            working: false,
        });
        spawnerStatus.hasSpawned = true;

    }

    public static spawnMaxAvail(givenRole: string, spawner: StructureSpawn, spawnerStatus: SpawnerStatus, creepRole: BaseRole): void {
        let nrComponents = Math.floor(spawner.room.energyAvailable / 200);
        // console.log("energyAvailable: "+spawner.room.energyAvailable);
        // console.log("energyCapacityAvailable: "+spawner.room.energyCapacityAvailable);
        // console.log("nrComponents: "+nrComponents);
        let components = [];
        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        SpawnerWrapper.createCreep(spawner, components, creepRole, {
            role: givenRole,
            working: false,
        });
        spawnerStatus.hasSpawned = true;
    }

}
Profiler.registerClass(SpawnerController, SpawnerController.name);
